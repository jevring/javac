# Introduction

I want to write a compiler, to get a better understanding of how the the JVM and bytecodes etc work.
I decided to do it "back to front", in the sense that I start with the class file format, then add the byte codes themselves for method bodies, then the grammar, and finally the translation from grammar to AST to byte-code.
I did this because I wanted to have tangible results first, that were executable. 
Also, I felt like the most interesting part was in the class file generation.
With that done, I could, in theory, implement any language I want as a front-end.

This is all based on the [Java Virtual Machine Specification for Java 8](https://docs.oracle.com/javase/specs/jvms/se8/html/index.html).
 
* [Chapter 2](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-2.htm) 
* [Chapter 3](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-3.htm)
* [Chapter 4](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.htm) - The actual class file format
* [Chapter 5](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-5.htm)
* [Chapter 6](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-6.htm) - Instruction set, the byte codes and their semantics
	* [The wikipedia page on bytecodes](https://en.wikipedia.org/wiki/Java_bytecode_instruction_listings) is also a handy short reference
 
The front-end, where we'll parse Java according to the official grammar. 
* [The Java® Language Specification](https://docs.oracle.com/javase/specs/jls/se8/html/index.html)
* [Chapter 3. Lexical Structure](https://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html)
* [Chapter 19. the official grammar](https://docs.oracle.com/javase/specs/jls/se8/html/jls-19.html).
* [Chapter 15. Expressions ](https://docs.oracle.com/javase/specs/jls/se8/html/jls-15.html).

Interestingly, I seem to be choosing the same names of classes and functions as what's already part in the java compiler inside the jdk. And not just the names that correspond to things in the grammar or the class file =)

## Some kind of path forward:
* ✔ if-statements (Including StackMapTable etc)
* ✔ 'in-line boolean expressions: `boolean b = 1 == 2`
* ✔ LineNumbers in the class file
* ✔ String concatenation
* ✔ Static method invocation without static imports like `Math.random()`
* ✔ Reference methods by FQN, and not imports, like: 
	* ✔ `return net.jevring.javac.extras.Helpers.one();`
	* ✔ `return new net.jevring.javac.extras.IntContainer();` 
* ✔ Expression names with more than one dot. Like `System.out.println(A.b.c.d.e.f);`
* ✔ Variable assignment (not just as part of declarations)
	* ✔ non-static variable class initialization
	* ✔ Field access for assignment for other objects than 'this'
	* ✔ Have a Class representation for 'this' for the class we're compiling
	* ✔ Local variable slot sharing in different scopes (there might be N variables in slot 3 if there are N branches, despite differently named variables)
	* ✔ Deal with multiple variables with the same name in different scopes (for example `String s = "hello";` in both if and else branches)
	* ✔ Assigning variables outside their scope. For instance defining `int i;` outside an if-statement, then doing `i = 10;` inside the if-statement
* ✔ Superclasses other than java.lang.Object
	* ✔ super()
	* ✔ this()
* ✔ Interfaces
	* ✔ Implement an existing interface
	* ✔ Compile an interface
	* ✔ Call an interface method on another object
* ✔ String-concatenation where the return type isn't obvious, as from the method return type or assignment
	* `System.out.println(1 + " = " + 1);`
* ✔ if-statements with non-branching expressions, like `boolean b = ...; if (b) { ... }`
* ✔ Loops
	* ✔ for-statements
	* ✔ while-statements
	* ✔ do-while-statements
* ✔ `double`, `float`, and `long` literals and constant bytecode instructions
* ✔ Hex, octal, binary integer literals
* ✔ Reference fields in the same class (especially if they're been defined at the bottom of the class)
* ✔ Reference methods in the same class
* branch statements for doubles and non-integers/booleans
* ✔ else-if
* StringBuilder optimization to avoid creating a new one for each addition (i.e. one string builder to "a" + "b" + "c")
* ✔ null, non-null comparisons in branches
* Special short-cut rules for 0 or null (IFXX etc byte codes)
	* == null
	* != null
	* > 0, < 0, == 0, etc
* Circular references (might get that for free from above)
* General math, including shifting 
* ✔ Postfix incrementation for local variables and fields
* Prefix incrementation for local variables and fields
* ✔ Throwing exceptions
* ✔ Declaring exceptions
* ✔ try-catch (ExceptionTable)
* ✔ *basic* `finally` block handling
* *full* `finally` block - haha, not even fucking close!
* Basic polymorphism (`.equals(Object)`)
* Multiple variables with the same name in different scopes
	* Multiple catch clauses where the exception is called 'e'
	* Multiple normal blocks where the variable is of a different and incompatible type
	* Reuse slots (unexpectedly the normal javac actually does this!)
* Abstract classes
* ✔ Assigning a concrete implementation to an interface field/variable, like `Set set = new HashSet();`
* Enums
* Casting
* Anonymous inner classes
* Generics
* Generics in interfaces: `Set<String> set = new HashSet<String>()`
* Reference inner classes
	* Use the `InnerClasses` class file attribute (not sure what this is used *for*, but apparently it should (can?) be there when referencing an inner class)
* Compile entire directories
	* Reference other classes in the set of classes being compiled
	* Reference other static fields from static fields (declaration order)
	* Superclass or interface in compilation directory
* Polymorphism
	* method arguments (only Object)
	* method arguments
	* Generics
	* Exceptions caught
	* Exceptions declared 
	* Variable assignment
* Arrays (maybe split this in different sections)
* Figure out how the fuck `max_stack` is calculated, because my intuition is seemingly wrong
* ✔ Boolean operators (||, &&, ^)
	* ✔ Simple booleans
	* ✔ Multiple chained expressions
	* ✔ Combined branching expressions
* Assignment operators other than equals
* Type inference
	* `i2d` and similar conversions
* Diamond operator: `Set<String> set = new HashSet<>()`
* Annotations (compile-time)
* Annotations (run-time)
* Varargs
* All the other things I'm no-doubt forgetting will be slotted in here as I realize them...
* Inner classes
* Classes *inside* method bodies
* `default` interface methods
* `synchronized` keyword
* try-with-resources
* lambdas
* Method references
* Bitwise operators (|, &, ^)
* `ifne` and similar specialized jump instructions
* conflicting variable names in multiple static initializer blocks
* switch-case
* net.jevring.javac.output.structure.attributes.MethodParameters (javap doesn't seem to produce anything like this)
	* [https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.24](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.24)
* Syntactic sugar (things that get converted to simpler things by the compiler itself)
	* Enhanced for: `for (X x : xs)`
* non-static initializer blocks in classes
* double-brace initialization
* All the other weird shit I had no idea Java supported until I embarked on this journey
* Long tail of parser rules I've no-doubt missed
* All the remaining implementations of the `Writer` interface, as I'm sure there will be some. This includes figuring out why they are remaining, and why I haven't used them yet.

## Far into the future:
* [Validation outside of just "does this method exist"](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.10)
	* Disallow referencing variables that are unassigned
	* Ensure that the variable/field being assigned is polymorphically compatible (Set/HashSet, for example)
	* Validate that methods that throw checked exceptions declare them
	* Good error messages for things like missing semi-colon
	* Override annotation only on inherited methods
	* Don't catch or throw things that aren't exceptions
	* Caught exceptions separated with a | can't subclass each other.
	* Detect unreachable statements (more than just stuff after a return, too)
* Self-hosting
* Command-line interface
* End-user documentation
* Publication to the rest of the world (other than just through the repo)
* Separate back-end into byte-code generation library
* Java 9 support
* Java 9+ support
* `strictfp` keyword
* `native` keyword

# On StackMapTables
The stack map tables are an absolute pain in the ass, because, while they are described in the spec, I have no fucking clue as to how to actually generate them.
I don't know if I should generate them as I go, or after the fact. Or if I have to jump around in the byte code to know. Or if I can generate them from the local variable table, or vice versa.
As a result, I've decided to try to delay them by running with `-Xverify:none`.
Eventually they'll make it in here, because I get that they're good, but until I understand **how**, I'm trying to avoid them for now.

This has the unfortunate side-effect of disabling **all** byte code verification, when all I want is the stack map table verification disabled =(

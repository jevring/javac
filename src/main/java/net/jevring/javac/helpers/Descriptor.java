/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.helpers;

import net.jevring.javac.generators.methods.MethodSignature;
import net.jevring.javac.input.proxies.ClassProxy;

import java.util.Arrays;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Descriptor {
	public static String of(ClassProxy type) {
		if (type.equals(ClassProxy.VOID)) {
			return "V";
		} else if (type.equals(ClassProxy.BOOLEAN)) {
			return "Z";
		} else if (type.equals(ClassProxy.BYTE)) {
			return "B";
		} else if (type.equals(ClassProxy.SHORT)) {
			return "S";
		} else if (type.equals(ClassProxy.INT)) {
			return "I";
		} else if (type.equals(ClassProxy.LONG)) {
			return "J";
		} else if (type.equals(ClassProxy.CHAR)) {
			return "C";
		} else if (type.equals(ClassProxy.FLOAT)) {
			return "F";
		} else if (type.equals(ClassProxy.DOUBLE)) {
			return "D";
		} else {
			if (type.isArray()) {
				return type.getName().replace('.', '/');
			} else {
				return String.format("L%s;", type.getCanonicalName().replace('.', '/'));
			}
		}
	}

	public static String of(MethodSignature methodSignature) {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		Arrays.stream(methodSignature.getParameterTypes()).map(Descriptor::of).forEach(sb::append);
		sb.append(")");
		sb.append(Descriptor.of(methodSignature.getReturnType()));
		return sb.toString();
	}

	public static ClassProxy to(String descriptor) {
		switch (descriptor) {
			case "V":
				return ClassProxy.VOID;
			case "Z":
				return ClassProxy.BOOLEAN;
			case "B":
				return ClassProxy.BYTE;
			case "S":
				return ClassProxy.SHORT;
			case "I":
				return ClassProxy.INT;
			case "J":
				return ClassProxy.LONG;
			case "C":
				return ClassProxy.CHAR;
			case "F":
				return ClassProxy.FLOAT;
			case "D":
				return ClassProxy.DOUBLE;
			default:
				if (descriptor.startsWith("[")) {
					// todo: find out how many dimensions

					// todo: hack for HelloWorldTest. Will address later 
					return ClassProxy.forClass(String[].class);
				}
				if (descriptor.startsWith("L") && descriptor.endsWith(";")) {
					String classNameWithSlashes = descriptor.substring(1, descriptor.length() - 1);
					String classNameWithDots = classNameWithSlashes.replace('/', '.');

					// todo: we'd like this to be a CompilerException
					return ClassProxy.forName(classNameWithDots).orElseThrow(() -> new IllegalArgumentException("Unknown type: " + descriptor));
				} else {
					// todo: we'd like this to be a CompilerException
					throw new IllegalArgumentException("Unknown type: " + descriptor);
				}
		}
	}
}

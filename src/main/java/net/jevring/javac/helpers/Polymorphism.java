/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.helpers;

import net.jevring.javac.input.proxies.ClassProxy;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Polymorphism {
	public static boolean signatureIsPolymorphicallyCompatible(ClassProxy[] declarationParameterTypes, ClassProxy[] invocationParameterTypes) {
		// todo: varargs
		if (invocationParameterTypes.length != declarationParameterTypes.length) {
			return false;
		}
		for (int i = 0; i < invocationParameterTypes.length; i++) {
			ClassProxy declarationType = declarationParameterTypes[i];
			ClassProxy invocationType = invocationParameterTypes[i];
			if (!isSuperTypeOrInterface(declarationType, invocationType)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSuperTypeOrInterface(ClassProxy potentialSuperTypeOrInterface, ClassProxy type) {
		if (type.isPrimitive()) {
			// since we don't support auto-boxing yet, we're going to have to be stricted for primitives
			return potentialSuperTypeOrInterface.equals(type);
		}
		if (potentialSuperTypeOrInterface.equals(type) || ClassProxy.OBJECT.equals(potentialSuperTypeOrInterface)) {
			return true;
		} else {
			// todo: handle the case where we have something that extends a superclass, and where that super-class implements the interface we're after
			if (isSuperClass(potentialSuperTypeOrInterface, type)) {
				return true;
			} else {
				return isSuperInterface(potentialSuperTypeOrInterface, type);
			}
		}
	}

	private static boolean isSuperInterface(ClassProxy potentialSuperTypeOrInterface, ClassProxy type) {
		for (ClassProxy iface : type.getInterfaces()) {
			if (isSuperTypeOrInterface(potentialSuperTypeOrInterface, iface)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isSuperClass(ClassProxy potentialSuperTypeOrInterface, ClassProxy type) {
		return type.getSuperClass().filter(classProxy -> isSuperTypeOrInterface(potentialSuperTypeOrInterface, classProxy)).isPresent();
	}
}

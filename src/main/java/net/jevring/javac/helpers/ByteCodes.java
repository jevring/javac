/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.helpers;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.variables.LocalVariable;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.ByteCode;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ByteCodes {
	public static ByteCode returnTypeForDescriptor(ClassProxy clazz) {
		// todo: fill this up so that it works for every primitive
		switch (Descriptor.of(clazz)) {
			case "I":
			case "Z":
				return ByteCode.IRETURN;
			case "F":
				return ByteCode.FRETURN;
			case "D":
				return ByteCode.DRETURN;
			case "J":
				return ByteCode.LRETURN;
			case "V":
				return ByteCode.RETURN;
			default:
				return ByteCode.ARETURN;
		}
	}

	public static void store(LocalVariable localVariable, CodeGenerator codeGenerator) {
		// can we just dispatch this whole thing to a visitor and rely on the byte code that is returned simply leaves us with a correct stack?
		String descriptor = Descriptor.of(localVariable.getType());
		byte slot = localVariable.getSlot();
		switch (descriptor) {
			// todo: fill with the rest of the primitives
			case "I":
			case "Z":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.ISTORE_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.ISTORE_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.ISTORE_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.ISTORE_3);
						break;
					default:
						codeGenerator.add(ByteCode.ISTORE, slot);
						break;
				}
				break;
			case "D":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.DSTORE_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.DSTORE_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.DSTORE_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.DSTORE_3);
						break;
					default:
						codeGenerator.add(ByteCode.DSTORE, slot);
						break;
				}
				break;
			case "F":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.FSTORE_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.FSTORE_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.FSTORE_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.FSTORE_3);
						break;
					default:
						codeGenerator.add(ByteCode.FSTORE, slot);
						break;
				}
				break;
			case "L":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.LSTORE_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.LSTORE_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.LSTORE_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.LSTORE_3);
						break;
					default:
						codeGenerator.add(ByteCode.LSTORE, slot);
						break;
				}
				break;
			default:
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.ASTORE_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.ASTORE_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.ASTORE_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.ASTORE_3);
						break;
					default:
						codeGenerator.add(ByteCode.ASTORE, slot);
						break;
				}
				break;
		}
		codeGenerator.stack().pop();
	}

	public static void load(LocalVariable localVariable, CodeGenerator codeGenerator) {
		// can we just dispatch this whole thing to a visitor and rely on the byte code that is returned simply leaves us with a correct stack?
		String descriptor = Descriptor.of(localVariable.getType());
		byte slot = localVariable.getSlot();
		switch (descriptor) {
			// todo: fill with the rest of the primitives
			case "I":
			case "Z":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.ILOAD_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.ILOAD_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.ILOAD_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.ILOAD_3);
						break;
					default:
						codeGenerator.add(ByteCode.ILOAD, slot);
						break;
				}
				break;
			case "D":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.DLOAD_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.DLOAD_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.DLOAD_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.DLOAD_3);
						break;
					default:
						codeGenerator.add(ByteCode.DLOAD, slot);
						break;
				}
				break;
			case "F":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.FLOAD_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.FLOAD_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.FLOAD_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.FLOAD_3);
						break;
					default:
						codeGenerator.add(ByteCode.FLOAD, slot);
						break;
				}
				break;
			case "L":
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.LLOAD_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.LLOAD_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.LLOAD_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.LLOAD_3);
						break;
					default:
						codeGenerator.add(ByteCode.LLOAD, slot);
						break;
				}
				break;
			default:
				switch (slot) {
					case 0:
						codeGenerator.add(ByteCode.ALOAD_0);
						break;
					case 1:
						codeGenerator.add(ByteCode.ALOAD_1);
						break;
					case 2:
						codeGenerator.add(ByteCode.ALOAD_2);
						break;
					case 3:
						codeGenerator.add(ByteCode.ALOAD_3);
						break;
					default:
						codeGenerator.add(ByteCode.ALOAD, slot);
						break;
				}
				break;
		}
		codeGenerator.stack().push(Descriptor.to(descriptor));
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.helpers;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.input.proxies.MethodProxy;
import net.jevring.javac.output.structure.ByteCode;

import java.util.function.Supplier;

import static net.jevring.javac.input.proxies.ClassProxy.STRING_BUILDER;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class StringConcatenationHelper {
	private static final Supplier<AssertionError> BROKEN_STRING_BUILDER = () -> new AssertionError("StringBuilder class not what we expected");
	private static final MethodProxy STRING_BUILDER_CONSTRUCTOR = STRING_BUILDER.getConstructor().orElseThrow(BROKEN_STRING_BUILDER);
	private static final MethodProxy STRING_BUILDER_TO_STRING = STRING_BUILDER.getMethod("toString").orElseThrow(BROKEN_STRING_BUILDER);
	private static final MethodProxy STRING_BUILDER_APPEND_OBJECT =
			STRING_BUILDER.getMethod("append", ClassProxy.OBJECT).orElseThrow(BROKEN_STRING_BUILDER);

	private final ClassFileGenerator classFileGenerator;
	private final CodeGenerator codeGenerator;

	public StringConcatenationHelper(ClassFileGenerator classFileGenerator, CodeGenerator codeGenerator) {
		this.classFileGenerator = classFileGenerator;
		this.codeGenerator = codeGenerator;
	}

	public void newStringBuilder() {
		codeGenerator.add(ByteCode.NEW, classFileGenerator.constantPoolReference(STRING_BUILDER));
		codeGenerator.stack().push(STRING_BUILDER);
		codeGenerator.add(ByteCode.DUP);
		codeGenerator.stack().push(STRING_BUILDER);
		codeGenerator.add(ByteCode.INVOKESPECIAL, classFileGenerator.constantPoolReference(STRING_BUILDER_CONSTRUCTOR));
		codeGenerator.stack().pop();
	}

	public void callAppend() {
		MethodProxy appendForType = STRING_BUILDER.getMethod("append", codeGenerator.stack().peek()).orElse(STRING_BUILDER_APPEND_OBJECT);
		codeGenerator.add(ByteCode.INVOKEVIRTUAL, classFileGenerator.constantPoolReference(appendForType));
		codeGenerator.stack().pop(); // pop the argument, but leave the StringBuilder on the stack, since that's what .append() returns
	}

	public void callToString() {
		codeGenerator.add(ByteCode.INVOKEVIRTUAL, classFileGenerator.constantPoolReference(STRING_BUILDER_TO_STRING));
		codeGenerator.stack().pop();
		codeGenerator.stack().push(ClassProxy.STRING);
	}
}

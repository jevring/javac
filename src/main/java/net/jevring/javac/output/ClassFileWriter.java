/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import net.jevring.javac.output.structure.*;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Contains the bytes representing the class.
 *
 * @author markus@jevring.net
 * @see ClassFile
 */
@RequiredArgsConstructor(staticName = "createFor", access = AccessLevel.PUBLIC)
public class ClassFileWriter {
	private static final int MAGIC = 0xCAFEBABE;
	private final ClassFile classFile;

	public void write(OutputStream out) throws IOException {
		DataOutputStream dos = new DataOutputStream(out);

		writeMagic(dos);
		writeVersion(dos);
		writeConstantPool(dos);
		writeAccessFlags(dos);
		writeThisClassAndSuperClass(dos);
		writeInterfaces(dos);
		writeFields(dos);
		writeMethods(dos);
		writeAttributes(dos);

		dos.flush();
	}

	private void writeMagic(DataOutputStream dos) throws IOException {
		dos.writeInt(MAGIC);
	}

	private void writeVersion(DataOutputStream dos) throws IOException {
		classFile.getVersion().write(dos);
	}

	private void writeAccessFlags(DataOutputStream dos) throws IOException {
		classFile.getAccessFlags().write(dos);
	}

	private void writeThisClassAndSuperClass(DataOutputStream dos) throws IOException {
		dos.writeShort(classFile.getThisClass());
		dos.writeShort(classFile.getSuperClass());
	}

	private void writeAttributes(DataOutputStream dos) throws IOException {
		List<AttributeInfo> attributes = classFile.getAttributes();
		dos.writeShort(attributes.size());
		for (AttributeInfo attribute : attributes) {
			attribute.write(dos);
		}
	}

	private void writeMethods(DataOutputStream dos) throws IOException {
		List<MethodInfo> methods = classFile.getMethods();
		dos.writeShort(methods.size());
		for (MethodInfo method : methods) {
			method.write(dos);
		}
	}

	private void writeFields(DataOutputStream dos) throws IOException {
		List<FieldInfo> fields = classFile.getFields();
		dos.writeShort(fields.size());
		for (FieldInfo field : fields) {
			field.write(dos);
		}
	}

	private void writeInterfaces(DataOutputStream dos) throws IOException {
		List<Short> interfaces = classFile.getInterfaces();
		dos.writeShort(interfaces.size());
		for (Short iface : interfaces) {
			dos.writeShort(iface);
		}
	}

	private void writeConstantPool(DataOutputStream dos) throws IOException {
		List<ConstantPoolInfo> constantPool = classFile.getConstantPool();
		dos.writeShort(constantPool.size() + 1); // the constant pool is indexed from 1
		for (ConstantPoolInfo constantPoolInfo : constantPool) {
			if (constantPoolInfo == null) {
				// doubles and longs, take up two slots in the constant pool, which we handle by setting those "slots" to null.
				// thus, we need to skip then when writing.
				continue;
			}
			constantPoolInfo.write(dos);
		}
	}
}

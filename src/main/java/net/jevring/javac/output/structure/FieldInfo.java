/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure;

import lombok.Builder;
import lombok.Value;
import net.jevring.javac.output.Writer;
import net.jevring.javac.output.structure.accessflags.AccessFlags;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
@Value
@Builder
public class FieldInfo implements Writer {
	private final AccessFlags accessFlags;
	/**
	 * Points to a {@link net.jevring.javac.output.structure.constantpool.Utf8Info} in the constant pool.
	 */
	private final short nameIndex;
	/**
	 * Points to a {@link net.jevring.javac.output.structure.constantpool.Utf8Info} in the constant pool.
	 */
	private final short descriptorIndex;
	private final List<AttributeInfo> attributes;

	@Override
	public void write(DataOutputStream dos) throws IOException {
		accessFlags.write(dos);
		dos.writeShort(nameIndex);
		dos.writeShort(descriptorIndex);
		dos.writeShort(attributes.size());
		for (AttributeInfo attributeInfo : attributes) {
			attributeInfo.write(dos);
		}
	}

}

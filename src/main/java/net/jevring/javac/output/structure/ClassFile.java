/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure;

import lombok.Builder;
import lombok.Value;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.Writer;
import net.jevring.javac.output.structure.accessflags.AccessFlags;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Contains the data about a class, but not the actual byte-code.
 * <p>
 * https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html
 *
 * @author markus@jevring.net
 * @see ClassFileWriter
 */
@Value
@Builder
public class ClassFile {
	private final AccessFlags accessFlags;
	private final Version version;
	private final List<ConstantPoolInfo> constantPool;
	private final short thisClass;
	private final short superClass;
	private final List<Short> interfaces;
	private final List<FieldInfo> fields;
	private final List<MethodInfo> methods;
	private final List<AttributeInfo> attributes;

	@Value
	public static final class Version implements Writer {
		public static final Version JAVA_8 = new Version((short) 0, (short) 52);
		private final short minorVersion;
		private final short majorVersion;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeShort(minorVersion);
			dos.writeShort(majorVersion);
		}
	}
}

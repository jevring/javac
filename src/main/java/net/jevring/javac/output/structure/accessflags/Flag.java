/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure.accessflags;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public enum Flag {
	ACC_NATIVE(0x0100),
	ACC_INTERFACE(0x0200),
	ACC_STRICT(0x0800),
	ACC_ABSTRACT(0x0400),
	ACC_FINAL(0x0010),
	ACC_SYNCHRONIZED(0x0020),
	ACC_VOLATILE(0x0040),
	ACC_BRIDGE(0x0040),
	ACC_TRANSIENT(0x0080),
	ACC_VARARGS(0x0080),
	ACC_SYNTHETIC(0x1000),
	ACC_ANNOTATION(0x2000),
	ACC_ENUM(0x4000),
	ACC_MANDATED(0x8000),
	ACC_DEFAULT(0x0000),
	ACC_PUBLIC(0x0001),
	ACC_PRIVATE(0x0002),
	ACC_PROTECTED(0x0004),
	ACC_STATIC(0x0008);

	private final int value;

	Flag(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}

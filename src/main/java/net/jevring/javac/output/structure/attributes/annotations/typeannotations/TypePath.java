/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure.attributes.annotations.typeannotations;

import lombok.Value;
import net.jevring.javac.output.Writer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.20.2
 *
 * @author markus@jevring.net
 */
@Value
public class TypePath implements Writer {
	private final List<Path> path;

	@Override
	public void write(DataOutputStream dos) throws IOException {
		dos.writeByte(path.size());
		for (TypePath.Path part : path) {
			dos.writeByte(part.getTypePathKind().getKind());
			dos.writeByte(part.getTypeArgumentIndex());
		}
	}

	@Value
	public static final class Path {
		private final TypePathKind typePathKind;
		private final byte typeArgumentIndex;
	}

	/**
	 * https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.20.2-220-B-A.1
	 */
	public enum TypePathKind {
		Array(0),
		NestedType(1),
		BoundOfWildcard(2),
		ParameterizedType(3);

		private final byte kind;

		TypePathKind(int kind) {
			// use an int in the signature to not have to cast everywhere
			this.kind = (byte) kind;
		}

		public byte getKind() {
			return kind;
		}
	}
}

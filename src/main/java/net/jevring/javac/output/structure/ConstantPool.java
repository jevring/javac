/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure;

import net.jevring.javac.output.structure.constantpool.DoubleInfo;
import net.jevring.javac.output.structure.constantpool.LongInfo;
import net.jevring.javac.output.structure.constantpool.Utf8Info;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ConstantPool {
	private final List<ConstantPoolInfo> list = new ArrayList<>();

	/**
	 * Adds a {@link net.jevring.javac.output.structure.ConstantPoolInfo} to the constant pool for the provided value,
	 * unless such a value already exists, in which case it returns the index of the entry.
	 *
	 * @param info the item
	 * @return the index of the added or found entry
	 */
	public short add(ConstantPoolInfo info) {
		int i = list.indexOf(info);
		if (i >= 0) {
			return (short) (i + 1);
		} else {
			int index = list.size();
			list.add(info);
			if (info instanceof DoubleInfo || info instanceof LongInfo) {
				// doubles and longs take up two spaces, so we have to put something in this hole to prevent it from being taken by something else.
				// putting a null here, and skipping it when writing the class, is handy, because then we can use the size of the list to determine
				// how many slots there are, and we don't need to carry an extra field
				list.add(null);
			}
			return (short) (index + 1); // remember, it's 1-indexed!
		}
	}

	public <T extends ConstantPoolInfo> T get(short index) {
		return (T) list.get(index - 1); // remember, it's 1-indexed!
	}

	/**
	 * Adds a {@link net.jevring.javac.output.structure.constantpool.Utf8Info} to the constant pool for the provided value,
	 * unless such a value already exists, in which case it returns the index of the entry.
	 *
	 * @param value the string
	 * @return the index of the added or found entry
	 */
	public short utf8(String value) {
		byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
		Utf8Info utf8 = new Utf8Info((short) bytes.length, bytes);
		return add(utf8);
	}

	public List<ConstantPoolInfo> generate() {
		return list;
	}
}

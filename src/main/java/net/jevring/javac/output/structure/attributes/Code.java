/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.output.structure.attributes;

import lombok.Value;
import net.jevring.javac.generators.code.PendingByteArrayIndex;
import net.jevring.javac.output.Writer;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
@Value
public class Code implements Attribute {
	private final int attributeLength;
	private final short maxStack;
	private final short maxLocals;
	/**
	 * Max length: 65536
	 */
	private final byte[] code;
	private final byte[] exceptionTable;
	private final byte[] attributes;

	@Override
	public void write(DataOutputStream dos) throws IOException {
		dos.writeInt(attributeLength);
		dos.writeShort(maxStack);
		dos.writeShort(maxLocals);
		dos.writeInt(code.length);
		dos.write(code);
		dos.write(exceptionTable);
		dos.write(attributes);
	}

	@Value
	public static final class Exception implements Writer {
		private final short startPc;
		private final short endPc;
		private final PendingByteArrayIndex handlerPc;
		private final short catchType;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeShort(startPc);
			dos.writeShort(endPc);
			dos.writeShort(handlerPc.getValue());
			dos.writeShort(catchType);
		}
	}
}

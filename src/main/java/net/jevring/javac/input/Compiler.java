/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input;

import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.visitors.CompilationUnitVisitor;
import net.jevring.javac.output.ClassFileWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;

/**
 * Compiles .java files into .class files and writes them to disk in the specified target directory.
 *
 * @author markus@jevring.net
 */
public class Compiler {
	private final Path targetDirectory;
	private final ClassPath classPath;

	public Compiler(Path targetDirectory, ClassPath classPath) {
		this.targetDirectory = targetDirectory;
		this.classPath = classPath;
	}

	public <T> Class<T> compile(Path path) throws IOException {
		Java8Parser.CompilationUnitContext compilationUnit = Parser.parseFile(path);
		CompilationUnitVisitor visitor = new CompilationUnitVisitor(classPath);

		ClassFileGenerator classFileGenerator = compilationUnit.accept(visitor);
		String fullyQualifiedName = classFileGenerator.getFullyQualifiedName();
		File file = resolveFilePath(classFileGenerator.getFullyQualifiedNameWithSlashes());
		try (OutputStream out = new FileOutputStream(file)) {
			classFileGenerator.setSourceFile(path.getFileName().toString());
			ClassFileWriter.createFor(classFileGenerator.generate()).write(out);
		}
		System.out.printf("Wrote class to %s%n", file.getAbsolutePath());
		return loadClassFromDisk(fullyQualifiedName);
	}

	private <T> Class<T> loadClassFromDisk(String fullyQualifiedName) throws MalformedURLException {
		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{targetDirectory.toUri().toURL()});
		try {
			return (Class<T>) urlClassLoader.loadClass(fullyQualifiedName);
		} catch (ClassNotFoundException e) {
			// it must exist, because we just created it
			throw new AssertionError(e);
		}
	}

	private File resolveFilePath(String classFile) throws IOException {
		File file = targetDirectory.resolve(classFile + ".class").toFile();
		File parentDirectory = file.getParentFile();
		if (!parentDirectory.exists() && !parentDirectory.mkdirs()) {
			throw new IOException("Could not create directory " + parentDirectory);
		}
		return file;
	}
}

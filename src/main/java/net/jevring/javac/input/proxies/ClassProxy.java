/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.proxies;

import net.jevring.javac.input.intermediate.ClassFileGenerator;

import java.util.List;
import java.util.Optional;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public interface ClassProxy {
	ClassProxy VOID = forClass(void.class);
	ClassProxy BOOLEAN = forClass(boolean.class);
	ClassProxy INT = forClass(int.class);
	ClassProxy FLOAT = forClass(float.class);
	ClassProxy SHORT = forClass(short.class);
	ClassProxy DOUBLE = forClass(double.class);
	ClassProxy LONG = forClass(long.class);
	ClassProxy CHAR = forClass(char.class);
	ClassProxy BYTE = forClass(byte.class);
	ClassProxy STRING = forClass(String.class);
	ClassProxy OBJECT = forClass(Object.class);
	ClassProxy STRING_BUILDER = forClass(StringBuilder.class);
	ClassProxy STRING_ARRAY = forClass(String[].class);
	ClassProxy THROWABLE = forClass(Throwable.class);
	ClassProxy EXCEPTION = forClass(Exception.class);

	static ClassProxy forClass(Class<?> clazz) {
		return new RealClassProxy(clazz);
	}

	static ClassProxy forClass(ClassFileGenerator classFileGenerator) {
		return new ClassFileGeneratorClassProxy(classFileGenerator);
	}

	static Optional<ClassProxy> forName(String name) {
		try {
			return Optional.of(new RealClassProxy(Class.forName(name)));
		} catch (ClassNotFoundException e) {
			// todo: in the future, we should look up the currently compiled classes here
			return Optional.empty();
		}
	}

	public Optional<MethodProxy> getConstructor(ClassProxy... parameterTypes);

	public Optional<FieldProxy> getField(String name);

	public Optional<MethodProxy> getMethod(String name, ClassProxy... parameterTypes);

	public boolean isDoubleSize();

	public String getCanonicalName();

	public boolean isArray();

	public String getName();

	public boolean isInterface();

	public Optional<ClassProxy> getSuperClass();

	public List<ClassProxy> getInterfaces();

	public boolean isPrimitive();
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.proxies;

import java.lang.reflect.Field;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class RealFieldProxy implements FieldProxy {
	private final Field field;

	RealFieldProxy(Field field) {
		this.field = field;
	}

	@Override
	public ClassProxy getType() {
		return new RealClassProxy(field.getType());
	}

	@Override
	public int getModifiers() {
		return field.getModifiers();
	}

	@Override
	public String getName() {
		return field.getName();
	}

	@Override
	public ClassProxy getDeclaringClass() {
		return new RealClassProxy(field.getDeclaringClass());
	}

	@Override
	public String toString() {
		return "RealFieldProxy{" + "field=" + field + '}';
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.proxies;

import net.jevring.javac.generators.methods.Method;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class ClassFileGeneratorMethodProxy implements MethodProxy {
	private final Method method;

	ClassFileGeneratorMethodProxy(Method method) {
		this.method = method;
	}

	@Override
	public int getParameterCount() {
		return method.getMethodSignature().getParameterTypes().length;
	}

	@Override
	public ClassProxy getReturnType() {
		return method.getMethodSignature().getReturnType();
	}

	@Override
	public int getModifiers() {
		return method.getAccessFlags().getModifiers();
	}

	@Override
	public String getName() {
		return method.getName();
	}

	@Override
	public ClassProxy[] getParameterTypes() {
		return method.getMethodSignature().getParameterTypes();
	}

	@Override
	public ClassProxy getDeclaringClass() {
		return method.getDeclaringClass();
	}

	@Override
	public String toString() {
		return "ClassFileGeneratorMethodProxy{" + "method=" + method + '}';
	}
}

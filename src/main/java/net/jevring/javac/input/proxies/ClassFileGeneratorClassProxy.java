/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.proxies;

import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import net.jevring.javac.output.structure.constantpool.Utf8Info;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class ClassFileGeneratorClassProxy implements ClassProxy {
	private final ClassFileGenerator classFileGenerator;

	ClassFileGeneratorClassProxy(ClassFileGenerator classFileGenerator) {
		this.classFileGenerator = classFileGenerator;
	}

	@Override
	public Optional<FieldProxy> getField(String name) {
		return classFileGenerator.getFieldsGenerator().getField(name).map(fieldName -> new ClassFileGeneratorFieldProxy(this, fieldName));
	}

	@Override
	public Optional<MethodProxy> getMethod(String name, ClassProxy... parameterTypes) {
		return classFileGenerator.getMethodsGenerator().getMethod(name, parameterTypes).map(ClassFileGeneratorMethodProxy::new);
	}

	@Override
	public boolean isDoubleSize() {
		// since this proxy is *never* a double or a long, this will *never* be true
		return false;
	}

	@Override
	public Optional<MethodProxy> getConstructor(ClassProxy... parameterTypes) {
		return classFileGenerator.getMethodsGenerator().getMethod(MethodsGenerator.CONSTRUCTOR_NAME, parameterTypes).map(ClassFileGeneratorMethodProxy::new);
	}

	@Override
	public String getCanonicalName() {
		return classFileGenerator.getFullyQualifiedName();
	}

	@Override
	public boolean isArray() {
		// todo: can we ever *compile* an array?
		// we're more likely to compile the actual type, and then we get the array-type of that automatically
		return false;
	}

	@Override
	public boolean isInterface() {
		return Modifier.isInterface(classFileGenerator.getAccessFlags().getModifiers());
	}

	@Override
	public Optional<ClassProxy> getSuperClass() {
		return Optional.of(classFileGenerator.getSuperClass());
	}

	@Override
	public List<ClassProxy> getInterfaces() {
		// todo: this isn't very elegant.
		return classFileGenerator.getInterfaces()
		                         .stream()
		                         .map(classFileGenerator.getConstantPool()::get)
		                         .map(constantPoolInfo -> (ClassInfo) constantPoolInfo)
		                         .map(ClassInfo::getNameIndex)
		                         .map(classFileGenerator.getConstantPool()::get)
		                         .map(constantPoolInfo -> (Utf8Info) constantPoolInfo)
		                         .map(Utf8Info::toString)
		                         .map(ClassProxy::forName)
		                         .map(Optional::get) // todo: we should check this first
		                         .collect(Collectors.toList());

	}

	@Override
	public boolean isPrimitive() {
		// nothing we're compiling ourselves will ever be a primitive
		return false;
	}

	@Override
	public String getName() {
		return classFileGenerator.getName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ClassFileGeneratorClassProxy)) {
			return false;
		}
		ClassFileGeneratorClassProxy that = (ClassFileGeneratorClassProxy) o;
		return Objects.equals(classFileGenerator, that.classFileGenerator);
	}

	@Override
	public int hashCode() {
		return Objects.hash(classFileGenerator);
	}
}

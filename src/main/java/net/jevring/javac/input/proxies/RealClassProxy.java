/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.proxies;

import net.jevring.javac.helpers.Polymorphism;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class RealClassProxy implements ClassProxy {
	private final Class<?> clazz;

	RealClassProxy(Class<?> clazz) {
		this.clazz = clazz;
	}

	@Override
	public Optional<MethodProxy> getConstructor(ClassProxy... parameterTypes) {
		try {
			return Optional.of(new RealConstructorProxy(clazz.getConstructor(toClassArray(parameterTypes))));
		} catch (NoSuchMethodException e) {
			try {
				return Optional.of(new RealConstructorProxy(clazz.getDeclaredConstructor(toClassArray(parameterTypes))));
			} catch (NoSuchMethodException e2) {
				return Optional.empty();
			}
		}
	}

	@Override
	public Optional<FieldProxy> getField(String name) {
		try {
			return Optional.of(new RealFieldProxy(clazz.getField(name)));
		} catch (NoSuchFieldException e) {
			try {
				return Optional.of(new RealFieldProxy(clazz.getDeclaredField(name)));
			} catch (NoSuchFieldException e2) {
				return Optional.empty();
			}
		}
	}

	@Override
	public Optional<MethodProxy> getMethod(String name, ClassProxy... parameterTypes) {
		return matchMethod(name, clazz.getMethods(), parameterTypes).or(() -> matchMethod(name, clazz.getDeclaredMethods(), parameterTypes));
	}

	private Optional<MethodProxy> matchMethod(String name, Method[] methods, ClassProxy[] parameterTypes) {
		for (Method method : methods) {
			if (method.getName().equals(name)) {
				if (Polymorphism.signatureIsPolymorphicallyCompatible(toClassProxyArray(method.getParameterTypes()), parameterTypes)) {
					return Optional.of(new RealMethodProxy(method));
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public boolean isDoubleSize() {
		return clazz.equals(Double.class) || clazz.equals(Long.class) || clazz.equals(double.class) || clazz.equals(long.class);
	}

	@Override
	public String getCanonicalName() {
		return clazz.getCanonicalName();
	}

	@Override
	public boolean isArray() {
		return clazz.isArray();
	}

	@Override
	public String getName() {
		return clazz.getName();
	}

	@Override
	public boolean isInterface() {
		return clazz.isInterface();
	}

	@Override
	public Optional<ClassProxy> getSuperClass() {
		Class<?> superclass = clazz.getSuperclass();
		if (superclass == null) {
			return Optional.empty();
		} else {
			return Optional.of(ClassProxy.forClass(superclass));
		}
	}

	@Override
	public List<ClassProxy> getInterfaces() {
		return Arrays.stream(clazz.getInterfaces()).map(RealClassProxy::new).collect(Collectors.toList());
	}

	@Override
	public boolean isPrimitive() {
		return clazz.isPrimitive();
	}

	private static Class<?>[] toClassArray(ClassProxy[] parameterTypes) {
		return Arrays.stream(parameterTypes).map(parameterType -> ((RealClassProxy) parameterType).clazz).toArray(Class<?>[]::new);
	}

	private static ClassProxy[] toClassProxyArray(Class<?>[] parameterTypes) {
		return Arrays.stream(parameterTypes).map(RealClassProxy::new).toArray(ClassProxy[]::new);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof RealClassProxy)) {
			return false;
		}
		RealClassProxy that = (RealClassProxy) o;
		return Objects.equals(clazz, that.clazz);
	}

	@Override
	public int hashCode() {
		return Objects.hash(clazz);
	}

	@Override
	public String toString() {
		return "RealClassProxy{" + "clazz=" + clazz + '}';
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ClassPath {
	public String getClassName(String pkg, String identifier) {
		try {
			// todo: can we do better than just brute-forcing it like this?
			// worry about that later
			Class<?> aClass = Class.forName(pkg + "." + identifier);
			if (aClass != null) {
				return aClass.getName();
			} else {
				return null;
			}
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

	public String getFieldOrMethod(String type, String identifier) {
		try {
			Class<?> clazz = Class.forName(type);
			for (Method method : clazz.getMethods()) {
				if (method.getName().equals(identifier)) {
					// this class contains a matching method, return it
					// todo. return the method
					return clazz.getName();
				}
			}
			Field field = clazz.getField(identifier);
			return clazz.getName();
			// this class contains a matching field, return it
			// todo: return the field

			// todo: when we reference a field or method, what is it we *actually* want out of here?

		} catch (ClassNotFoundException | NoSuchFieldException e) {
			return null;
		}
	}
}

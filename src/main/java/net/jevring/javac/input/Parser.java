/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input;

import net.jevring.javac.grammar.Java8Lexer;
import net.jevring.javac.grammar.Java8Parser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Parser {
	public static Java8Parser.CompilationUnitContext parseFile(Path path) throws IOException {
		return parse(CharStreams.fromPath(path, StandardCharsets.UTF_8));
	}

	private static Java8Parser.CompilationUnitContext parse(CharStream input) throws IOException {
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		parser.setErrorHandler(new BailErrorStrategy());
		try {
			return parser.compilationUnit();
		} catch (ParseCancellationException e) {
			if (e.getCause() instanceof InputMismatchException) {
				InputMismatchException cause = (InputMismatchException) e.getCause();
				throw new CompilerException(cause.getOffendingToken(), "Could not parse. Maybe something is missing?");
			} else {
				throw e;
			}
		} catch (RecognitionException e) {
			throw new CompilerException(e.getOffendingToken(), "Could not parse. Maybe something is missing?");
		}
	}
}

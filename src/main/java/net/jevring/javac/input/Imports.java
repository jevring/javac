/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input;

import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.visitors.TypeNameVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Imports extends Java8BaseVisitor<Void> {
	private static final TypeNameVisitor typeNameVisitor = new TypeNameVisitor();
	private final List<ImportResolver> singleTypeImportResolvers = new ArrayList<>();
	private final List<ImportResolver> onDemandTypeImportResolvers = new ArrayList<>();
	private final List<ImportResolver> singleStaticImportResolvers = new ArrayList<>();
	private final List<ImportResolver> onDemandStaticImportResolvers = new ArrayList<>();
	private final ImportResolver samePackageImportResolver;
	private final ImportResolver javaLangImportResolver;
	private final ClassPath classPath;

	public Imports(List<String> currentPackage, ClassPath classPath) {
		this.samePackageImportResolver = new OnDemandTypeImportResolver(classPath, currentPackage);
		this.javaLangImportResolver = new OnDemandTypeImportResolver(classPath, List.of("java", "lang"));
		this.classPath = classPath;
	}

	public Optional<String> resolveType(String type) {
		return Stream.concat(Stream.of(samePackageImportResolver),
		                     Stream.concat(Stream.concat(singleTypeImportResolvers.stream(), onDemandTypeImportResolvers.stream()),
		                                   Stream.of(javaLangImportResolver))).map(resolver -> resolver.resolve(type)).filter(Objects::nonNull).findFirst();
	}

	public Optional<String> resolveStaticMethod(String method) {
		return Stream.concat(singleStaticImportResolvers.stream(), onDemandStaticImportResolvers.stream())
		             .map(resolver -> resolver.resolve(method))
		             .filter(Objects::nonNull)
		             .findFirst();
	}

	public Optional<String> resolveStaticField(String field) {
		// todo: this will need to change, so that we can differentiate between methods and fields
		return Stream.concat(singleStaticImportResolvers.stream(), onDemandStaticImportResolvers.stream())
		             .map(resolver -> resolver.resolve(field))
		             .filter(Objects::nonNull)
		             .findFirst();
	}


	@Override
	public Void visitSingleTypeImportDeclaration(Java8Parser.SingleTypeImportDeclarationContext ctx) {
		List<String> type = ctx.typeName().accept(typeNameVisitor);
		singleTypeImportResolvers.add(new SingleTypeImportResolver(type));
		return null;
	}

	@Override
	public Void visitTypeImportOnDemandDeclaration(Java8Parser.TypeImportOnDemandDeclarationContext ctx) {
		List<String> pkg = ctx.typeName().accept(typeNameVisitor);
		onDemandTypeImportResolvers.add(new OnDemandTypeImportResolver(classPath, pkg));
		return null;
	}

	@Override
	public Void visitSingleStaticImportDeclaration(Java8Parser.SingleStaticImportDeclarationContext ctx) {
		List<String> type = ctx.typeName().accept(typeNameVisitor);
		String member = ctx.Identifier().getText();
		singleStaticImportResolvers.add(new SingleStaticImportResolver(type, member));
		return null;
	}

	@Override
	public Void visitStaticImportOnDemandDeclaration(Java8Parser.StaticImportOnDemandDeclarationContext ctx) {
		List<String> type = ctx.typeName().accept(typeNameVisitor);
		onDemandStaticImportResolvers.add(new OnDemandStaticImportResolver(classPath, type));
		return null;
	}

	private interface ImportResolver {
		String resolve(String identifier);
	}

	private static final class OnDemandTypeImportResolver implements ImportResolver {
		private final ClassPath classPath;
		private final String pkg;

		private OnDemandTypeImportResolver(ClassPath classPath, List<String> pkg) {
			this.classPath = classPath;
			this.pkg = pkg.stream().collect(Collectors.joining("."));
		}

		@Override
		public String resolve(String identifier) {
			return classPath.getClassName(pkg, identifier);
		}
	}

	private static final class OnDemandStaticImportResolver implements ImportResolver {
		private final ClassPath classPath;
		private final String type;

		private OnDemandStaticImportResolver(ClassPath classPath, List<String> type) {
			this.classPath = classPath;
			this.type = type.stream().collect(Collectors.joining("."));
		}

		@Override
		public String resolve(String identifier) {
			return classPath.getFieldOrMethod(type, identifier);
		}
	}

	private static final class SingleTypeImportResolver implements ImportResolver {
		private final String lastToken;
		private final String type;

		private SingleTypeImportResolver(List<String> type) {
			this.lastToken = type.get(type.size() - 1);
			this.type = type.stream().collect(Collectors.joining("."));
		}

		@Override
		public String resolve(String identifier) {
			if (lastToken.equals(identifier)) {
				return type;
			} else {
				return null;
			}
		}
	}

	private static final class SingleStaticImportResolver implements ImportResolver {
		private final String member;
		private final String type;

		private SingleStaticImportResolver(List<String> type, String member) {
			this.member = member;
			this.type = type.stream().collect(Collectors.joining("."));
		}

		@Override
		public String resolve(String identifier) {
			if (member.equals(identifier)) {
				// todo: is this really what we want to return here?
				return type;
			} else {
				return null;
			}
		}
	}
}

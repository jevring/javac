/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ConstructorBodyVisitor extends Java8BaseVisitor<CodeGenerator> {
	private final ClassFileGenerator classFileGenerator;
	private final CodeGenerator codeGenerator;

	public ConstructorBodyVisitor(ClassFileGenerator classFileGenerator, CodeGenerator codeGenerator) {
		this.classFileGenerator = classFileGenerator;
		this.codeGenerator = codeGenerator;
	}

	@Override
	public CodeGenerator visitBlockStatements(Java8Parser.BlockStatementsContext ctx) {
		ctx.accept(new ExpressionVisitor(classFileGenerator, ClassProxy.VOID, codeGenerator, String.format("<constructor in class: %s>", classFileGenerator.getName())));
		return codeGenerator;
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ConstructorModifierVisitor extends AggregateJava8Visitor<AccessFlags.AccessFlagsBuilder> {
	public ConstructorModifierVisitor() {
		super(AccessFlags.builder());
	}

	@Override
	public AccessFlags.AccessFlagsBuilder visitConstructorModifier(Java8Parser.ConstructorModifierContext ctx) {
		// todo: handle annotations
		if (ctx.Public() != null) {
			aggregate.flag(Flag.ACC_PUBLIC);
		}
		if (ctx.Protected() != null) {
			aggregate.flag(Flag.ACC_PROTECTED);
		}
		if (ctx.Private() != null) {
			aggregate.flag(Flag.ACC_PRIVATE);
		}
		return aggregate;
	}
}

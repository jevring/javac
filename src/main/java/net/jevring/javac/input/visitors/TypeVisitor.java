/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.ClassInfo;

/**
 * Invoked once per type (class/interface/enum) defined in a given class.
 *
 * @author markus@jevring.net
 */
public class TypeVisitor extends Java8BaseVisitor<ClassFileGenerator> {
	private static final String JAVA_LANG_OBJECT = "java/lang/Object";

	private final ClassFileGenerator classFileGenerator;
	private final ConstantPool constantPool;
	private final String packageName;
	private final Imports imports;

	private final ClassModifierVisitor classModifierVisitor = new ClassModifierVisitor();
	private final InterfaceModifierVisitor interfaceModifierVisitor = new InterfaceModifierVisitor();

	public TypeVisitor(String packageName, ConstantPool constantPool, Imports imports) {
		this.classFileGenerator = new ClassFileGenerator(constantPool, imports);
		this.constantPool = constantPool;
		this.packageName = packageName;
		this.imports = imports;
	}

	@Override
	public ClassFileGenerator visitClassModifier(Java8Parser.ClassModifierContext ctx) {
		classFileGenerator.setAccessFlags(ctx.accept(classModifierVisitor).build());
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitInterfaceModifier(Java8Parser.InterfaceModifierContext ctx) {
		classFileGenerator.setAccessFlags(ctx.accept(interfaceModifierVisitor).build());
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitClassBody(Java8Parser.ClassBodyContext ctx) {
		// todo: what happens if there are nested classes? Will we end up in here again? deal with that later, as it happens. 
		// nesting things are going to be a bitch.
		// should we re-tool the grammar to make this easier?
		// IS there something we can do to make things easier?
		CodeGenerator clinit = new CodeGenerator(constantPool);
		ctx.accept(new ClassBodyVisitor(classFileGenerator, constantPool, clinit, imports));
		if (clinit.hasOps()) {
			clinit.add(ByteCode.RETURN);
			classFileGenerator.getMethodsGenerator()
			                  .addMethod(AccessFlags.builder().flag(Flag.ACC_STATIC).build(),
			                             MethodsGenerator.EMPTY_CONSTRUCTOR_SIGNATURE,
			                             MethodsGenerator.CLASS_INITIALIZER_NAME)
			                  .addAttribute(clinit.generate());
		}

		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
		String name = ctx.Identifier().getText();
		String classFqn = packageName + "." + name;
		classFileGenerator.setFullyQualifiedName(classFqn);
		classFileGenerator.setName(name);
		short thisClassIndex = constantPool.add(new ClassInfo(constantPool.utf8(dotsToSlashes(classFqn))));
		ClassProxy superClass;
		short superClassIndex;
		if (ctx.superclass() != null) {
			String resolvedSuperClass = ctx.superclass().classOrInterfaceType().accept(new ClassOrInterfaceTypeVisitor(imports));
			superClassIndex = constantPool.add(new ClassInfo(constantPool.utf8(dotsToSlashes(resolvedSuperClass))));
			superClass = ClassProxy.forName(resolvedSuperClass)
			                       .orElseThrow(() -> new CompilerException(ctx.superclass().start, String.format("Could not resolve type %s", name)));
		} else {
			superClassIndex = constantPool.add(new ClassInfo(constantPool.utf8(JAVA_LANG_OBJECT)));
			superClass = ClassProxy.OBJECT;
		}
		if (ctx.superinterfaces() != null) {
			for (Java8Parser.ClassOrInterfaceTypeContext implementedInterface : ctx.superinterfaces().interfaceTypeList().classOrInterfaceType()) {
				String resolvedInterface = implementedInterface.accept(new ClassOrInterfaceTypeVisitor(imports));
				short interfaceIndex = constantPool.add(new ClassInfo(constantPool.utf8(dotsToSlashes(resolvedInterface))));
				classFileGenerator.getInterfaces().add(interfaceIndex);
			}
		}
		if (ctx.typeParameters() != null) {
			// todo: attach the type parameters
		}

		classFileGenerator.setThisClassIndex(thisClassIndex);
		classFileGenerator.setSuperClassIndex(superClassIndex);
		classFileGenerator.setSuperClass(superClass);
		visitChildren(ctx);
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitEnumDeclaration(Java8Parser.EnumDeclarationContext ctx) {
		// todo: implement TypeVisitor.visitEnumDeclaration()
		throw new UnsupportedOperationException("TypeVisitor.visitEnumDeclaration() not yet implemented");
	}

	@Override
	public ClassFileGenerator visitNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) {
		// todo: implement TypeVisitor.visitNormalInterfaceDeclaration()
		throw new UnsupportedOperationException("TypeVisitor.visitNormalInterfaceDeclaration() not yet implemented");
	}

	@Override
	public ClassFileGenerator visitAnnotationTypeDeclaration(Java8Parser.AnnotationTypeDeclarationContext ctx) {
		// todo: implement TypeVisitor.visitAnnotationTypeDeclaration()
		throw new UnsupportedOperationException("TypeVisitor.visitAnnotationTypeDeclaration() not yet implemented");
	}

	private String dotsToSlashes(String somethingWithDots) {
		return somethingWithDots.replace('.', '/');
	}


}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.Imports;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.attributes.Exceptions;
import net.jevring.javac.output.structure.constantpool.ClassInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ThrowsVisitor extends Java8BaseVisitor<Optional<AttributeInfo>> {
	private final ConstantPool constantPool;
	private final Imports imports;

	public ThrowsVisitor(ConstantPool constantPool, Imports imports) {
		this.constantPool = constantPool;
		this.imports = imports;
	}

	@Override
	public Optional<AttributeInfo> visitXThrows(Java8Parser.XThrowsContext ctx) {
		List<Short> declaredExceptionConstantPoolIndexes = new ArrayList<>();
		for (Java8Parser.ExceptionTypeContext exceptionType : ctx.exceptionTypeList().exceptionType()) {
			// todo: support type variable & annotations
			String typeName = exceptionType.accept(new ClassOrInterfaceTypeVisitor(imports));
			declaredExceptionConstantPoolIndexes.add(constantPool.add(new ClassInfo(constantPool.utf8(typeName.replace('.', '/')))));
		}
		if (!declaredExceptionConstantPoolIndexes.isEmpty()) {
			return Optional.of(new AttributeInfo(constantPool.utf8("Exceptions"), new Exceptions(declaredExceptionConstantPoolIndexes)));
		} else {
			return Optional.empty();
		}
	}

	@Override
	protected Optional<AttributeInfo> defaultResult() {
		return Optional.empty();
	}
}

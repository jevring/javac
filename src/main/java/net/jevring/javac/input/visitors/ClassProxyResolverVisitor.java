/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.proxies.ClassProxy;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ClassProxyResolverVisitor extends Java8BaseVisitor<ClassProxy> {
	private final Imports imports;

	public ClassProxyResolverVisitor(Imports imports) {
		this.imports = imports;
	}

	// todo: handle everything under 'unannotatedReferenceType'

	@Override
	public ClassProxy visitClassOrInterfaceType(Java8Parser.ClassOrInterfaceTypeContext ctx) {
		String fqn;
		if (ctx.classOrInterfaceType() == null) {
			fqn = imports.resolveType(ctx.Identifier().getText()).orElseThrow(unknownClassAt(ctx));
		} else {
			// do the backwards walk, like for the package name
			fqn = reverseClassOrInterfaceType(ctx).stream().collect(Collectors.joining("."));
		}
		return ClassProxy.forName(fqn).orElseThrow(unknownClassAt(ctx));
	}

	private List<String> reverseClassOrInterfaceType(Java8Parser.ClassOrInterfaceTypeContext ctx) {
		Java8Parser.ClassOrInterfaceTypeContext part = ctx.classOrInterfaceType();
		LinkedList<String> parts = new LinkedList<>();
		while (part != null) {
			parts.addFirst(part.Identifier().getText());
			part = part.classOrInterfaceType();
		}
		parts.addLast(ctx.Identifier().getText());
		return parts;
	}


	@Override
	public ClassProxy visitClassOrInterfaceTypeToInstantiate(Java8Parser.ClassOrInterfaceTypeToInstantiateContext ctx) {
		String classToInstantiate;
		if (ctx.Identifier().size() == 1) {
			String type = ctx.Identifier(0).getText();
			classToInstantiate = imports.resolveType(type).orElseThrow(unknownClassAt(ctx));
		} else {
			classToInstantiate = ctx.Identifier().stream().map(TerminalNode::getText).collect(Collectors.joining("."));
		}
		return ClassProxy.forName(classToInstantiate).orElseThrow(unknownClassAt(ctx));
	}

	@Override
	public ClassProxy visitUnannotatedArrayType(Java8Parser.UnannotatedArrayTypeContext ctx) {
		// todo. deal with annotations
		// todo: how can we generate the dimensions here for the class constant?
		// can we maybe do the same as in UnannotatedTypeDescriptorVisitor and then simply pass that through Class.forName()?

		Class<Class[][]> aClass = Class[][].class;

		ClassProxy baseType = visit(ctx.unannotatedClassType());
		if (baseType.equals(ClassProxy.STRING)) {
			// special case to support HelloWorldTest for now
			return ClassProxy.forClass(String[].class);
		}
		throw new CompilerException(ctx.start, "Arrays not supported yet");

	}

	@Override
	public ClassProxy visitUnannotatedClassType(Java8Parser.UnannotatedClassTypeContext ctx) {
		// todo. deal with annotations
		// todo: deal with typeArguments
		String name = ctx.Identifier().getText();
		// todo: deal with the recursive role of 'unannotatedClassType'
		Optional<String> className = imports.resolveType(name);
		return className.flatMap(ClassProxy::forName).orElseThrow(unknownClassAt(ctx));
	}

	private Supplier<CompilerException> unknownClassAt(ParserRuleContext ctx) {
		return () -> new CompilerException(ctx.start, String.format("Could not resolve type %s", ctx.getText()));
	}

	@Override
	public ClassProxy visitPrimitiveBoolean(Java8Parser.PrimitiveBooleanContext ctx) {
		return ClassProxy.BOOLEAN;
	}

	@Override
	public ClassProxy visitPrimitiveByte(Java8Parser.PrimitiveByteContext ctx) {
		return ClassProxy.BYTE;
	}

	@Override
	public ClassProxy visitPrimitiveShort(Java8Parser.PrimitiveShortContext ctx) {
		return ClassProxy.SHORT;
	}

	@Override
	public ClassProxy visitPrimitiveInt(Java8Parser.PrimitiveIntContext ctx) {
		return ClassProxy.INT;
	}

	@Override
	public ClassProxy visitPrimitiveLong(Java8Parser.PrimitiveLongContext ctx) {
		return ClassProxy.LONG;
	}

	@Override
	public ClassProxy visitPrimitiveChar(Java8Parser.PrimitiveCharContext ctx) {
		return ClassProxy.CHAR;
	}

	@Override
	public ClassProxy visitPrimitiveFloat(Java8Parser.PrimitiveFloatContext ctx) {
		return ClassProxy.FLOAT;
	}

	@Override
	public ClassProxy visitPrimitiveDouble(Java8Parser.PrimitiveDoubleContext ctx) {
		return ClassProxy.DOUBLE;
	}
}

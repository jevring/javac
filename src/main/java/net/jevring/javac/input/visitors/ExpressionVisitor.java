/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.StackMapTableGenerator;
import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.code.PendingByteArrayIndex;
import net.jevring.javac.generators.code.PendingShortJumpOffset;
import net.jevring.javac.generators.code.PreparedShortJumpOffset;
import net.jevring.javac.generators.fields.Field;
import net.jevring.javac.generators.variables.LocalVariable;
import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.helpers.ByteCodes;
import net.jevring.javac.helpers.StringConcatenationHelper;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.TrackingStack;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.input.proxies.FieldProxy;
import net.jevring.javac.input.proxies.MethodProxy;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.ConstantPoolInfo;
import net.jevring.javac.output.structure.attributes.Code;
import net.jevring.javac.output.structure.attributes.stackmapframe.SameFrame;
import net.jevring.javac.output.structure.attributes.stackmapframe.SameFrameExtended;
import net.jevring.javac.output.structure.attributes.stackmapframe.SameLocals1StackItemFrame;
import net.jevring.javac.output.structure.attributes.stackmapframe.SameLocals1StackItemFrameExtended;
import net.jevring.javac.output.structure.attributes.stackmapframe.variableinfo.*;
import net.jevring.javac.output.structure.constantpool.*;
import org.antlr.v4.runtime.ParserRuleContext;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static net.jevring.javac.generators.variables.LocalVariableTableGenerator.HIDDEN;

/**
 * Returns the signature of the evaluated expression, and adds the corresponding byte codes into the code generator.
 * todo: we'll need to cut this class down a bit, but without making it too difficult to use
 *
 * @author markus@jevring.net
 */
public class ExpressionVisitor extends Java8BaseVisitor<CodeGenerator> {
	private final ClassFileGenerator classFileGenerator;
	private final ClassProxy expectedMethodReturnType;
	private final CodeGenerator codeGenerator;

	// todo: I think we can get rid of this if we move some things around
	private boolean shouldPutResultOfConditionalOnStack = false;
	private boolean shouldReverseBranchCondition = true;

	/**
	 * Jump target for when a boolean expression evaluates to false.
	 * If a boolean expression evaluates to true, the statements following the evaluation
	 * are just the next byte codes.
	 * <p>
	 * <p>It's a stack to support nested if-statements
	 * <p>
	 * todo: should we replace this stack with a new instance of ExpressionVisitor for each block/branch?
	 */
	private final Stack<PendingShortJumpOffset> branchNotTakenTargetOffset = new Stack<>();
	private final Stack<Java8Parser.XFinallyContext> finallyContext = new Stack<>();
	private ClassProxy expectedExpressionReturnType;
	/**
	 * This is used for example in static invocations, as we don't actually want to put the static type on to the normal stack.
	 */
	private final TrackingStack staticTypeStack = new TrackingStack();
	private final ClassProxy THIS;
	/**
	 * This is just used internally to know whether we should descent into another preview
	 */
	private final Set<ParserRuleContext> previewNodes = new HashSet<>();
	/**
	 * This is just for ease of debugging.
	 */
	private final String contextHint;


	public ExpressionVisitor(ClassFileGenerator classFileGenerator, ClassProxy expectedMethodReturnType, CodeGenerator codeGenerator, String contextHint) {
		this.classFileGenerator = classFileGenerator;
		this.expectedMethodReturnType = expectedMethodReturnType;
		this.codeGenerator = codeGenerator;
		this.expectedExpressionReturnType = expectedMethodReturnType;
		this.contextHint = contextHint;
		THIS = ClassProxy.forClass(this.classFileGenerator);
	}

	@Override
	public CodeGenerator visitBlock(Java8Parser.BlockContext ctx) {
		codeGenerator.variables().enterBlock();
		visitChildren(ctx);
		// todo: we should ideally not have to provide the code index here. that should be done transparently
		codeGenerator.variables().exitBlock(codeGenerator.getNextByteCodeArrayIndex());
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitBlockStatement(Java8Parser.BlockStatementContext ctx) {
		codeGenerator.lineNumbers().add(codeGenerator.getNextByteCodeArrayIndex(), ctx.start.getLine());
		return visitChildren(ctx);
	}

	@Override
	public CodeGenerator visitWhileStatement(Java8Parser.WhileStatementContext ctx) {
		return visitLoop(null, ctx.expression(), ctx.statement(), null);
	}

	@Override
	public CodeGenerator visitWhileStatementNoShortIf(Java8Parser.WhileStatementNoShortIfContext ctx) {
		return visitLoop(null, ctx.expression(), ctx.statementNoShortIf(), null);
	}

	@Override
	public CodeGenerator visitDoStatement(Java8Parser.DoStatementContext ctx) {
		// todo: stack map frames, eventually

		Java8Parser.ExpressionContext expression = ctx.expression();
		short loopStartIndex = codeGenerator.getNextByteCodeArrayIndex();

		visit(ctx.statement());
		shouldPutResultOfConditionalOnStack = false;
		// the jump direction here is reversed (compared to normal), so the conditionals need to be reversed too
		shouldReverseBranchCondition = false;
		visit(expression);
		setBranchNotTakenTargetOffset(codeGenerator.getLastBranchInstructionIndex(), loopStartIndex);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitBasicForStatement(Java8Parser.BasicForStatementContext ctx) {
		return visitLoop(ctx.forInit(), ctx.expression(), ctx.statement(), ctx.forUpdate());
	}

	@Override
	public CodeGenerator visitBasicForStatementNoShortIf(Java8Parser.BasicForStatementNoShortIfContext ctx) {
		return visitLoop(ctx.forInit(), ctx.expression(), ctx.statementNoShortIf(), ctx.forUpdate());
	}

	private CodeGenerator visitLoop(Java8Parser.ForInitContext forInit,
	                                Java8Parser.ExpressionContext expression,
	                                ParserRuleContext statement,
	                                Java8Parser.ForUpdateContext forUpdate) {
		// todo: stack map frames, eventually 
		if (forInit != null) {
			visit(forInit);
		}
		short loopStartIndex = codeGenerator.getNextByteCodeArrayIndex();

		shouldPutResultOfConditionalOnStack = false;
		shouldReverseBranchCondition = true;
		if (expression != null) {
			visit(expression);
		}
		short lastBranchInstructionIndex = codeGenerator.getLastBranchInstructionIndex();
		visit(statement);
		if (forUpdate != null) {
			visit(forUpdate);
		}
		PendingShortJumpOffset startOfLoopReverseOffsetReference = new PendingShortJumpOffset();
		short gotoIndex = codeGenerator.add(ByteCode.GOTO, startOfLoopReverseOffsetReference);
		startOfLoopReverseOffsetReference.setValue((short) (loopStartIndex - gotoIndex));

		setBranchNotTakenTargetOffset(lastBranchInstructionIndex, codeGenerator.getNextByteCodeArrayIndex());
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitIfThenElseStatementNoShortIf(Java8Parser.IfThenElseStatementNoShortIfContext ctx) {
		return visitIfStatement(ctx.expression(), ctx.ifPart, ctx.elsePart);
	}

	@Override
	public CodeGenerator visitIfThenElseStatement(Java8Parser.IfThenElseStatementContext ctx) {
		return visitIfStatement(ctx.expression(), ctx.statementNoShortIf(), ctx.statement());
	}

	@Override
	public CodeGenerator visitIfThenStatement(Java8Parser.IfThenStatementContext ctx) {
		return visitIfStatement(ctx.expression(), ctx.statement(), null);
	}

	private CodeGenerator visitIfStatement(ParserRuleContext conditionalPart, ParserRuleContext ifPart, ParserRuleContext elsePart) {
		shouldPutResultOfConditionalOnStack = false;
		shouldReverseBranchCondition = true;
		// reset the branch index, as the one we're looking for later is whatever is set by visiting the conditionalPart
		codeGenerator.resetLastBranchInstructionIndex();
		// put whatever we need on the stack
		visitChildren(conditionalPart);
		short jumpIndex = codeGenerator.getLastBranchInstructionIndex();
		if (jumpIndex == 0) {
			if (codeGenerator.stack().peek().equals(ClassProxy.BOOLEAN)) {
				// this happens for things like 'if (checkCondition()) { .. }', or 'if (b) { ... }' or 'if (true) { ... }'
				PendingShortJumpOffset branchNotTakenTargetOffset = new PendingShortJumpOffset();
				this.branchNotTakenTargetOffset.push(branchNotTakenTargetOffset);
				jumpIndex = codeGenerator.add(ByteCode.IFEQ, branchNotTakenTargetOffset);
				codeGenerator.stack().pop();
			} else {
				throw new CompilerException(conditionalPart.start, "if-statement parameters don't evaluate to a boolean");
			}
		}
		visitChildren(ifPart);

		if (elsePart != null) {
			if (codeGenerator.lastOpIsReturn() || codeGenerator.lastOpIsThrow()) {
				// todo: this is a bit of a hacky way to handle branching. See if we can't do better, once everything works
				// the idea here is that, if this branch returns anyway, there's no need to jump to the next bit
				setBranchNotTakenTargetOffset(jumpIndex, codeGenerator.getNextByteCodeArrayIndex());
				visitChildren(elsePart);
			} else {
				PendingShortJumpOffset branchTakenTargetOffset = new PendingShortJumpOffset();
				short gotoIndex = codeGenerator.add(ByteCode.GOTO, branchTakenTargetOffset);
				short elseIndex = codeGenerator.getNextByteCodeArrayIndex();
				visitChildren(elsePart);
				// calculate the jump offsets for both "if" and "else" branches
				branchTakenTargetOffset.setValue((short) (codeGenerator.getNextByteCodeArrayIndex() - gotoIndex));
				setBranchNotTakenTargetOffset(jumpIndex, elseIndex);
				addStackMapFrame();
			}
		} else {
			setBranchNotTakenTargetOffset(jumpIndex, codeGenerator.getNextByteCodeArrayIndex());
		}
		return codeGenerator;
	}

	private void setBranchNotTakenTargetOffset(short originalBranchInstructionIndex, short currentInstructionIndex) {
		PendingShortJumpOffset offset = branchNotTakenTargetOffset.pop();
		offset.setValue((short) (currentInstructionIndex - originalBranchInstructionIndex));

		// todo: can we expect the frame to *always* be the same when the branch is NOT taken?
		// if so, shouldn't the JVM be able to just assume that as well?
		addSameFrame(currentInstructionIndex);
	}

	@Override
	public CodeGenerator visitTernary(Java8Parser.TernaryContext ctx) {
		return visitIfStatement(ctx.conditionalOrExpression(), ctx.expression(), ctx.conditionalExpression());
	}

	@Override
	public CodeGenerator visitConditionalOrExpression(Java8Parser.ConditionalOrExpressionContext ctx) {
		if (ctx.conditionalOrExpression() != null) {
			shouldPutResultOfConditionalOnStack = true;
			visit(ctx.conditionalOrExpression());
			visit(ctx.conditionalAndExpression());
			codeGenerator.add(ByteCode.IOR);
			codeGenerator.stack().popN(2);
			codeGenerator.stack().push(ClassProxy.BOOLEAN);
			codeGenerator.resetLastBranchInstructionIndex();
			return codeGenerator;
		} else {
			return visit(ctx.conditionalAndExpression());
		}
	}

	@Override
	public CodeGenerator visitConditionalAndExpression(Java8Parser.ConditionalAndExpressionContext ctx) {
		if (ctx.conditionalAndExpression() != null) {
			shouldPutResultOfConditionalOnStack = true;
			visit(ctx.conditionalAndExpression());
			visit(ctx.inclusiveOrExpression());
			codeGenerator.add(ByteCode.IAND);
			codeGenerator.stack().popN(2);
			codeGenerator.stack().push(ClassProxy.BOOLEAN);
			codeGenerator.resetLastBranchInstructionIndex();
			return codeGenerator;
		} else {
			return visit(ctx.inclusiveOrExpression());
		}
	}

	@Override
	public CodeGenerator visitExclusiveOrExpression(Java8Parser.ExclusiveOrExpressionContext ctx) {
		if (ctx.exclusiveOrExpression() != null) {
			shouldPutResultOfConditionalOnStack = true;
			visit(ctx.exclusiveOrExpression());
			visit(ctx.andExpression());
			codeGenerator.add(ByteCode.IXOR);
			codeGenerator.stack().popN(2);
			codeGenerator.stack().push(ClassProxy.BOOLEAN);
			codeGenerator.resetLastBranchInstructionIndex();
			return codeGenerator;
		} else {
			return visit(ctx.andExpression());
		}
	}

	@Override
	public CodeGenerator visitReferenceEquals(Java8Parser.ReferenceEqualsContext ctx) {
		return visitBranchExpression(new JumpInstructionDeterminator() {
			@Override
			public ByteCode determineByteCode() {
				if (isCompatibleWithIntergerJumpInstructions(codeGenerator.stack().peek())) {
					return shouldReverseBranchCondition ? ByteCode.IF_ICMPNE : ByteCode.IF_ICMPEQ;
				} else {
					return shouldReverseBranchCondition ? ByteCode.IF_ACMPNE : ByteCode.IF_ACMPEQ;
				}
			}
		}, ctx.equalityExpression(), ctx.relationalExpression());
	}

	@Override
	public CodeGenerator visitNotEquals(Java8Parser.NotEqualsContext ctx) {
		return visitBranchExpression(new JumpInstructionDeterminator() {
			@Override
			public ByteCode determineByteCode() {
				if (isCompatibleWithIntergerJumpInstructions(codeGenerator.stack().peek())) {
					return shouldReverseBranchCondition ? ByteCode.IF_ICMPEQ : ByteCode.IF_ICMPNE;
				} else {
					return shouldReverseBranchCondition ? ByteCode.IF_ACMPEQ : ByteCode.IF_ACMPNE;
				}
			}
		}, ctx.equalityExpression(), ctx.relationalExpression());
	}

	private boolean isCompatibleWithIntergerJumpInstructions(ClassProxy classProxy) {
		return classProxy.equals(ClassProxy.BOOLEAN) || classProxy.equals(ClassProxy.INT) || classProxy.equals(ClassProxy.LONG) || classProxy.equals(ClassProxy.SHORT) || classProxy
				.equals(ClassProxy.BYTE);
	}

	@Override
	public CodeGenerator visitLessThan(Java8Parser.LessThanContext ctx) {
		return visitBranchExpression(() -> shouldReverseBranchCondition ? ByteCode.IF_ICMPGE : ByteCode.IF_ICMPLT,
		                             ctx.relationalExpression(),
		                             ctx.shiftExpression());
	}

	@Override
	public CodeGenerator visitLessThanOrEqualTo(Java8Parser.LessThanOrEqualToContext ctx) {
		return visitBranchExpression(() -> shouldReverseBranchCondition ? ByteCode.IF_ICMPGT : ByteCode.IF_ICMPLE,
		                             ctx.relationalExpression(),
		                             ctx.shiftExpression());
	}

	@Override
	public CodeGenerator visitGreaterThan(Java8Parser.GreaterThanContext ctx) {
		return visitBranchExpression(() -> shouldReverseBranchCondition ? ByteCode.IF_ICMPLE : ByteCode.IF_ICMPGT,
		                             ctx.relationalExpression(),
		                             ctx.shiftExpression());
	}

	@Override
	public CodeGenerator visitGreaterThanOrEqualTo(Java8Parser.GreaterThanOrEqualToContext ctx) {
		return visitBranchExpression(() -> shouldReverseBranchCondition ? ByteCode.IF_ICMPLT : ByteCode.IF_ICMPGE,
		                             ctx.relationalExpression(),
		                             ctx.shiftExpression());
	}

	private CodeGenerator visitBranchExpression(JumpInstructionDeterminator jumpInstructionDeterminator, ParserRuleContext lhs, ParserRuleContext rhs) {
		// todo: support special things when the RHS or LHS evaluates to 0, like IFNE, etc
		visit(lhs);
		visit(rhs);
		PendingShortJumpOffset branchNotTakenTargetOffset = new PendingShortJumpOffset();
		this.branchNotTakenTargetOffset.push(branchNotTakenTargetOffset);
		short jumpIndex = codeGenerator.add(jumpInstructionDeterminator.determineByteCode(), branchNotTakenTargetOffset);
		codeGenerator.stack().popN(2);
		if (shouldPutResultOfConditionalOnStack) {
			// todo: maybe we could have something better like "will evaluate to a boolean that goes on the stack", that we can determine automatically

			// load the "if" value onto the stack
			codeGenerator.add(ByteCode.ICONST_1);
			codeGenerator.stack().push(ClassProxy.BOOLEAN);
			// the "else" value will be loaded onto the stack *after* this, so we need to jump past it to continue execution
			PendingShortJumpOffset branchTakenTargetOffset = new PendingShortJumpOffset();
			short gotoIndex = codeGenerator.add(ByteCode.GOTO, branchTakenTargetOffset);

			// load the "else" value onto the stack
			short elseIndex = codeGenerator.add(ByteCode.ICONST_0);
			codeGenerator.stack().push(ClassProxy.BOOLEAN);

			// calculate the jump offsets for both "if" and "else" branches
			branchTakenTargetOffset.setValue((short) (codeGenerator.getNextByteCodeArrayIndex() - gotoIndex));

			setBranchNotTakenTargetOffset(jumpIndex, elseIndex);
			addStackMapFrame();
			// this can be reset here, as all we want out of this particular branch is the resulting value on the stack
			codeGenerator.resetLastBranchInstructionIndex();
		}
		return codeGenerator;
	}

	private void addStackMapFrame0() {
		addSameLocals1StackItemFrame();
	}

	private void addStackMapFrame() {

		// todo: can we even determine this here, or do we need to generate it from the local variable table?
		// if that was possible, though, the JVM people would be able to do the same thing, so presumably that's not it

		// we actually need to look into the future to see what will be necessary in this branch, to determine what the frame should look like. 
		// that's really annoying =(

		// todo: make sure this -1 stuff only happens in the correct place, which is after the first frame. maybe we can start at -1, and still have it work somehow?
		short nextByteCodeArrayIndex = (short) (codeGenerator.getNextByteCodeArrayIndex() - codeGenerator.stackMapTable().getCurrentOffset() - 1);
		if (codeGenerator.stack().isEmpty()) {
			addSameFrame(nextByteCodeArrayIndex);
			// todo: how can we know, at this time, that we need more variables? 
			// or that we'll drop them?
			// are these only used for loops?

			// todo: use ChopFrame here to remove the last k local variables, for example when existing a loop
			// todo: use AppendFrame here to add k additional local variables
		} else if (codeGenerator.stack().getCurrentHeight() > 1) {
			addSameLocals1StackItemFrame();
		} else {
			// todo. use FullFrame here to represent the whole thing
			throw new IllegalStateException("We shouldn't be able to hit the full frame stuff yet");
		}
	}

	private void addSameFrame(short nextByteCodeArrayIndex) {
		if (nextByteCodeArrayIndex >= 0 && nextByteCodeArrayIndex <= 63) {
			codeGenerator.stackMapTable().add(new SameFrame((byte) nextByteCodeArrayIndex));
		} else {
			codeGenerator.stackMapTable().add(new SameFrameExtended(nextByteCodeArrayIndex));
		}
	}

	private void addSameLocals1StackItemFrame() {
		ClassProxy classOnStack = codeGenerator.stack().peek();
		VerificationTypeInfo variableInfo;

		if (classOnStack.equals(ClassProxy.INT) || classOnStack.equals(ClassProxy.BOOLEAN) || classOnStack.equals(ClassProxy.SHORT) || classOnStack.equals(
				ClassProxy.CHAR) || classOnStack.equals(ClassProxy.BYTE)) {
			variableInfo = new IntegerVariableInfo();
		} else if (classOnStack.equals(ClassProxy.DOUBLE)) {
			variableInfo = new DoubleVariableInfo();
		} else if (classOnStack.equals(ClassProxy.FLOAT)) {
			variableInfo = new FloatVariableInfo();
		} else if (classOnStack.equals(ClassProxy.LONG)) {
			variableInfo = new LongVariableInfo();
		} else {
			variableInfo = new ObjectVariableInfo(classFileGenerator.constantPoolReference(classOnStack));
			// todo: figure out how these work
				/*
				variableInfo = new ObjectVariableInfo();
				variableInfo = new NullVariableInfo();
				variableInfo = new TopVariableInfo();
				variableInfo = new UninitializedThisVariableInfo();
				variableInfo = new UninitializedVariableInfo();
				*/
			//throw new UnsupportedOperationException("SameFrame not yet supported for " + classOnStack);
		}

		// todo: calculate this frame type
		byte offsetDelta = 0;
		int frameType = offsetDelta + 64;

		if (frameType >= 64 && frameType <= 127) {
			codeGenerator.stackMapTable().add(new SameLocals1StackItemFrame((byte) frameType, variableInfo));
		} else {
			codeGenerator.stackMapTable().add(new SameLocals1StackItemFrameExtended((short) frameType, variableInfo));
		}
	}

	@Override
	public CodeGenerator visitUnqualifiedClassInstanceCreationExpression(Java8Parser.UnqualifiedClassInstanceCreationExpressionContext ctx) {
		// todo. handle type arguments 
		// todo: handle class body
		// todo: handle annotations

		ClassProxy clazz = ctx.classOrInterfaceTypeToInstantiate().accept(new ClassProxyResolverVisitor(classFileGenerator.getImports()));
		short classReference =
				classFileGenerator.getConstantPool().add(new ClassInfo(classFileGenerator.getConstantPool().utf8(clazz.getCanonicalName().replace('.', '/'))));
		codeGenerator.add(ByteCode.NEW, classReference);
		codeGenerator.stack().push(clazz);
		codeGenerator.add(ByteCode.DUP);
		codeGenerator.stack().push(clazz);

		ClassProxy[] signature = handleArgumentListAndGetSignature(ctx.argumentList());

		MethodProxy constructor = clazz.getConstructor(signature).orElseThrow(unknownConstructorAt(ctx));
		short constructorReference = classFileGenerator.constantPoolReference(constructor);
		codeGenerator.add(ByteCode.INVOKESPECIAL, constructorReference);
		codeGenerator.stack().popN(constructor.getParameterCount() + 1); // pops the parameters off of the stack, as well as 'this'

		return codeGenerator;
	}

	private Supplier<CompilerException> unknownClassAt(ParserRuleContext token) {
		return () -> new CompilerException(token.start, String.format("Unknown class %s", token.getText()));
	}

	private Supplier<CompilerException> unknownConstructorAt(ParserRuleContext token) {
		return () -> new CompilerException(token.start, String.format("Unknown constructor %s", token.getText()));
	}

	private Supplier<CompilerException> unknownFieldAt(ParserRuleContext token) {
		return () -> new CompilerException(token.start, String.format("Unknown field %s", token.getText()));
	}

	private Supplier<CompilerException> unknownMethodAt(ParserRuleContext token) {
		return () -> new CompilerException(token.start, String.format("Unknown method %s", token.getText()));
	}

	@Override
	public CodeGenerator visitLocalVariableDeclaration(Java8Parser.LocalVariableDeclarationContext ctx) {
		// todo: handle final and annotations

		ClassProxy variableType = ctx.unannotatedType().accept(new ClassProxyResolverVisitor(classFileGenerator.getImports()));
		for (Java8Parser.VariableDeclarationContext variableDeclarationContext : ctx.variableDeclaratorList().variableDeclaration()) {
			String name = variableDeclarationContext.variableDeclaratorId().getText();
			LocalVariable localVariable = codeGenerator.variables().addLocalVariable(variableType, name);
			// todo: deal with array dimensions and array initialization
			if (variableDeclarationContext.Equals() != null) {
				shouldPutResultOfConditionalOnStack = true;
				expectedExpressionReturnType = variableType;
				visitChildren(variableDeclarationContext.variableInitializer().expression());
				ByteCodes.store(localVariable, codeGenerator);
				// the scope starts *after* the store completes
				localVariable.startScope(codeGenerator.getNextByteCodeArrayIndex());
			}
		}
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitExpressionName(Java8Parser.ExpressionNameContext ctx) {
		List<String> namePartsAsList = reverseExpressionName(ctx);
		Iterator<String> nameParts = namePartsAsList.iterator();
		String namePart = nameParts.next();

		// todo: this should probably set expectedExpressionReturnType, and in the loop as well

		ClassProxy referencedType = null;
		LocalVariable localVariable = codeGenerator.variables().getLocalVariable(namePart);
		if (localVariable != null) {
			// todo: this branch will not work correctly if there are multiple parts to the name
			ByteCodes.load(localVariable, codeGenerator);
		} else {
			Optional<FieldProxy> optionalField = findFieldInInheritanceHierarchy(namePart, THIS);
			if (optionalField.isPresent()) {
				FieldProxy field = optionalField.get();
				short fieldReference = classFileGenerator.constantPoolReference(field);
				if (Modifier.isStatic(field.getModifiers())) {
					codeGenerator.add(ByteCode.GETSTATIC, fieldReference);
				} else {
					codeGenerator.add(ByteCode.ALOAD_0);
					codeGenerator.add(ByteCode.GETFIELD, fieldReference);
					// technically we both add and remove 'this' from the stack, but we don't have to bother with that, since we do both
					codeGenerator.stack().push(field.getType()); // push the result
				}
				codeGenerator.stack().push(field.getType());
			} else {
				Optional<String> staticMethod = classFileGenerator.getImports().resolveStaticMethod(namePart);
				if (staticMethod.isPresent()) {
					throw new CompilerException(ctx.start, "We don't yet support accessing static methods from imports");
					// todo: we'll need to actually resolve this field, so that we can get the descriptor
					//short fieldReference = classFileGenerator.constantPoolReference(staticMethod);
					//codeGenerator.add(ByteCode.GETSTATIC, fieldReference);
				} else {
					Optional<String> staticField = classFileGenerator.getImports().resolveStaticField(namePart);
					if (staticField.isPresent()) {
						throw new CompilerException(ctx.start, "We don't yet support accessing static fields from imports");
						// todo: we'll need to actually resolve this field, so that we can get the descriptor
						//short fieldReference = classFileGenerator.constantPoolReference(staticField);
						//codeGenerator.add(ByteCode.GETSTATIC, fieldReference);
					} else {
						Optional<String> type = classFileGenerator.getImports().resolveType(namePart);
						if (type.isPresent()) {
							// this is for example System.out, which resolves to java.lang.System.out
							referencedType = ClassProxy.forName(type.get()).orElseThrow(unknownClassAt(ctx));
							staticTypeStack.push(referencedType);
						} else if (nameParts.hasNext()) {
							// maybe the whole thing is a class or a type?
							// try it, and if it is, we're done processing the whole expression name
							String expressionName = namePartsAsList.stream().collect(Collectors.joining("."));
							referencedType = ClassProxy.forName(expressionName).orElseThrow(unknownClassAt(ctx));
							staticTypeStack.push(referencedType);
							return codeGenerator;
						} else {
							throw new CompilerException(ctx.start, "Unknown field: " + namePart);
						}
					}
				}
			}
		}
		while (nameParts.hasNext()) {
			namePart = nameParts.next();
			if (referencedType == null) {
				referencedType = codeGenerator.stack().peek();
			} else if (!staticTypeStack.isEmpty()) {
				// todo: this thing here is causing all kinds of trouble
				staticTypeStack.pop();
			}
			FieldProxy field = referencedType.getField(namePart).orElseThrow(unknownFieldAt(ctx));
			short fieldReference = classFileGenerator.constantPoolReference(field);
			if (Modifier.isStatic(field.getModifiers())) {
				codeGenerator.add(ByteCode.GETSTATIC, fieldReference);
			} else {
				codeGenerator.add(ByteCode.GETFIELD, fieldReference);
				codeGenerator.stack().pop();
			}
			codeGenerator.stack().push(field.getType());
			referencedType = field.getType();
		}
		return codeGenerator;
	}

	private Optional<FieldProxy> findFieldInInheritanceHierarchy(String name, ClassProxy startingPoint) {
		Optional<ClassProxy> clazz = Optional.of(startingPoint);
		// todo: this recursion is actually already done in java.lang.Class. Perhaps we should just implement it in ours instead.

		while (clazz.isPresent()) {
			ClassProxy c = clazz.get();
			Optional<FieldProxy> field = c.getField(name);
			if (field.isPresent()) {
				return field;
			} else {
				clazz = c.getSuperClass();
			}
		}
		return Optional.empty();
	}

	private List<String> reverseExpressionName(Java8Parser.ExpressionNameContext ctx) {
		Java8Parser.ExpressionNameContext part = ctx.expressionName();
		LinkedList<String> parts = new LinkedList<>();
		while (part != null) {
			parts.addFirst(part.Identifier().getText());
			part = part.expressionName();
		}
		parts.addLast(ctx.Identifier().getText());
		return parts;
	}

	@Override
	public CodeGenerator visitUnclassifiedMethodInvocation(Java8Parser.UnclassifiedMethodInvocationContext ctx) {
		String methodName = ctx.methodName().getText();
		ClassProxy[] signature = previewSignature(ctx.argumentList());
		MethodProxy method = THIS.getMethod(methodName, signature).or(() -> resolveStaticMethod(ctx, methodName, signature)).orElseThrow(unknownMethodAt(ctx));
		if (!Modifier.isStatic(method.getModifiers())) {
			pushThisOntoTheStack();
		}
		handleArgumentListAndGetSignature(ctx.argumentList());
		invokeMethod(method);
		return codeGenerator;
	}

	/**
	 * This populates the stack with the actual parameter values
	 */
	private ClassProxy[] handleArgumentListAndGetSignature(Java8Parser.ArgumentListContext argumentListContext) {
		boolean originalShouldPutResultOfConditionalOnStack = shouldPutResultOfConditionalOnStack;
		try {
			shouldPutResultOfConditionalOnStack = true;
			ArgumentListVisitor visitor = new ArgumentListVisitor(this, codeGenerator);
			return visitor.visitArgumentList(argumentListContext);
		} finally {
			shouldPutResultOfConditionalOnStack = originalShouldPutResultOfConditionalOnStack;
		}
	}

	/**
	 * Unlike {@link #handleArgumentListAndGetSignature(Java8Parser.ArgumentListContext)} this does not add new byte codes or modify the stack.
	 */
	private ClassProxy[] previewSignature(Java8Parser.ArgumentListContext argumentListContext) {
		ExpressionVisitor previewVisitor = createPreviewVisitor(argumentListContext);
		ArgumentListVisitor visitor = new ArgumentListVisitor(previewVisitor, previewVisitor.codeGenerator);
		previewVisitor.shouldPutResultOfConditionalOnStack = true;
		return visitor.visitArgumentList(argumentListContext);
	}

	private Optional<MethodProxy> resolveStaticMethod(Java8Parser.UnclassifiedMethodInvocationContext ctx, String methodName, ClassProxy[] signature) {
		// todo: in the future, we'll have to look for super classes and interfaces as well.
		// I wonder if there's a smart way to do that using reflection?
		Optional<String> classForStaticMethod = classFileGenerator.getImports().resolveStaticMethod(methodName);
		if (!classForStaticMethod.isPresent()) {
			return Optional.empty();
		} else {
			return ClassProxy.forName(classForStaticMethod.get()).orElseThrow(unknownClassAt(ctx.methodName())).getMethod(methodName, signature);
		}
	}

	@Override
	public CodeGenerator visitDotMethodInvocation(Java8Parser.DotMethodInvocationContext ctx) {
		// todo: handle typeArguments
		String methodName = ctx.Identifier().getText();

		ClassProxy methodOwnerClass;
		if (!staticTypeStack.isEmpty()) {
			methodOwnerClass = staticTypeStack.pop();
		} else {
			methodOwnerClass = codeGenerator.stack().peek();
		}

		ClassProxy[] signature = handleArgumentListAndGetSignature(ctx.argumentList());
		MethodProxy method = methodOwnerClass.getMethod(methodName, signature).orElseThrow(unknownMethodAt(ctx));
		invokeMethod(method);
		return codeGenerator;
	}

	private void invokeMethod(MethodProxy method) {
		short methodRef = classFileGenerator.constantPoolReference(method);
		int modifiers = method.getModifiers();
		if (Modifier.isStatic(modifiers)) {
			codeGenerator.add(ByteCode.INVOKESTATIC, methodRef);
			codeGenerator.stack().popN(method.getParameterCount());
		} else {
			if (Modifier.isPrivate(modifiers)) {
				codeGenerator.add(ByteCode.INVOKESPECIAL, methodRef);
			} else {
				if (method.getDeclaringClass().isInterface()) {
					// the number of parameters to pass to the interface method, including the object on which we're invoking the method
					byte count = (byte) (method.getParameterCount() + 1);
					codeGenerator.add(ByteCode.INVOKEINTERFACE, methodRef, count, (byte) 0);
				} else {
					codeGenerator.add(ByteCode.INVOKEVIRTUAL, methodRef);
				}
			}
			codeGenerator.stack().pop(); // pop 'this' or whatever object on which we called the method
			codeGenerator.stack().popN(method.getParameterCount());
		}
		if (!returnsVoid(method)) {
			codeGenerator.stack().push(method.getReturnType());
		}
	}

	@Override
	public CodeGenerator visitPureFieldAccess(Java8Parser.PureFieldAccessContext ctx) {
		// todo:handle stuff like MyClass.super.myField
		pushThisOntoTheStack();
		return readField(ctx, ctx.Identifier().getText(), classFileGenerator.getSuperClass());
	}

	@Override
	public CodeGenerator visitPrimaryFieldAccess(Java8Parser.PrimaryFieldAccessContext ctx) {
		return readField(ctx, ctx.Identifier().getText());
	}

	private CodeGenerator readField(ParserRuleContext ctx, String fieldName) {

		ClassProxy fieldOwnerClass;
		if (!staticTypeStack.isEmpty()) {
			fieldOwnerClass = staticTypeStack.pop();
		} else {
			fieldOwnerClass = codeGenerator.stack().peek();
		}

		return readField(ctx, fieldName, fieldOwnerClass);
	}

	private CodeGenerator readField(ParserRuleContext ctx, String fieldName, ClassProxy fieldOwnerClass) {
		FieldProxy field = findFieldInInheritanceHierarchy(fieldName, fieldOwnerClass).orElseThrow(unknownFieldAt(ctx));
		short fieldRef = classFileGenerator.constantPoolReference(field);

		if (Modifier.isStatic(field.getModifiers())) {
			codeGenerator.add(ByteCode.GETSTATIC, fieldRef);
		} else {
			codeGenerator.add(ByteCode.GETFIELD, fieldRef);
			codeGenerator.stack().pop();
		}
		codeGenerator.stack().push(field.getType());
		return codeGenerator;
	}

	private boolean returnsVoid(MethodProxy method) {
		return method.getReturnType().equals(ClassProxy.forClass(Void.class)) || method.getReturnType().equals(ClassProxy.VOID);
	}

	@Override
	public CodeGenerator visitStringLiteral(Java8Parser.StringLiteralContext ctx) {
		String value = ctx.StringLiteral().getText();
		String unquotedValue = value.substring(1, value.length() - 1);
		loadConstant(new StringInfo(classFileGenerator.getConstantPool().utf8(unquotedValue)));
		codeGenerator.stack().push(ClassProxy.STRING);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitIntegerLiteral(Java8Parser.IntegerLiteralContext ctx) {
		String value = ctx.IntegerLiteral().getText();
		// todo: look at the expected type, and use a conversion byte code if it's not correct
		long parsedValue;
		if (value.toUpperCase().endsWith("L")) {
			value = value.substring(0, value.length() - 1);
		}
		value = value.replace("_", "");
		if (value.toLowerCase().startsWith("0b")) {
			parsedValue = Long.parseLong(value.substring(2), 2);
		} else {
			parsedValue = Long.decode(value);
		}
		if (parsedValue == (int) parsedValue) {
			if (parsedValue == 0) {
				codeGenerator.add(ByteCode.ICONST_0);
			} else if (parsedValue == 1) {
				codeGenerator.add(ByteCode.ICONST_1);
			} else if (parsedValue == 2) {
				codeGenerator.add(ByteCode.ICONST_2);
			} else if (parsedValue == 3) {
				codeGenerator.add(ByteCode.ICONST_3);
			} else if (parsedValue == 4) {
				codeGenerator.add(ByteCode.ICONST_4);
			} else if (parsedValue == 5) {
				codeGenerator.add(ByteCode.ICONST_5);
			} else if (parsedValue == -1) {
				codeGenerator.add(ByteCode.ICONST_M1);
			} else if (parsedValue == (byte) parsedValue) {
				codeGenerator.add(ByteCode.BIPUSH, (byte) parsedValue);
			} else if (parsedValue == (short) parsedValue) {
				codeGenerator.add(ByteCode.SIPUSH, (short) parsedValue);
			} else {
				loadConstant(new IntegerInfo((int) parsedValue));
			}
			codeGenerator.stack().push(ClassProxy.INT);
		} else {
			loadConstant(new LongInfo(parsedValue));
			codeGenerator.stack().push(ClassProxy.LONG);
		}

		return codeGenerator;
	}

	private void loadConstant(ConstantPoolInfo constantPoolInfo) {
		short index = classFileGenerator.getConstantPool().add(constantPoolInfo);
		if (constantPoolInfo instanceof DoubleInfo || constantPoolInfo instanceof LongInfo) {
			codeGenerator.add(ByteCode.LDC2_W, index);
		} else if (index == (byte) index) {
			codeGenerator.add(ByteCode.LDC, (byte) index);
		} else {
			codeGenerator.add(ByteCode.LDC_W, index);
		}
	}

	@Override
	public CodeGenerator visitFloatingPointLiteral(Java8Parser.FloatingPointLiteralContext ctx) {
		String value = ctx.FloatingPointLiteral().getText();
		double parsedValue = Double.parseDouble(value);
		// todo: look at the expected type, and use a conversion byte code if it's not correct
		if (parsedValue == (float) parsedValue) {
			loadConstant(new FloatInfo((float) parsedValue));
			codeGenerator.stack().push(ClassProxy.FLOAT);
		} else {
			loadConstant(new DoubleInfo(parsedValue));
			codeGenerator.stack().push(ClassProxy.DOUBLE);
		}
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitBooleanLiteral(Java8Parser.BooleanLiteralContext ctx) {
		String value = ctx.BooleanLiteral().getText();
		boolean parsedValue = Boolean.parseBoolean(value);
		if (parsedValue) {
			codeGenerator.add(ByteCode.ICONST_1);
		} else {
			codeGenerator.add(ByteCode.ICONST_0);
		}
		codeGenerator.stack().push(ClassProxy.BOOLEAN);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitCharacterLiteral(Java8Parser.CharacterLiteralContext ctx) {
		String value = ctx.CharacterLiteral().getText();
		char parsedValue = value.charAt(0);
		// todo: ensure that this is correct
		loadConstant(new IntegerInfo((int) parsedValue));
		codeGenerator.stack().push(ClassProxy.CHAR);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitNullLiteral(Java8Parser.NullLiteralContext ctx) {
		codeGenerator.add(ByteCode.ACONST_NULL);
		codeGenerator.stack().push(ClassProxy.OBJECT);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitPrimaryThis(Java8Parser.PrimaryThisContext ctx) {
		pushThisOntoTheStack();
		return codeGenerator;
	}

	private void pushThisOntoTheStack() {
		codeGenerator.add(ByteCode.ALOAD_0); // load "this" as the object reference onto which to push this object
		codeGenerator.stack().push(THIS);
	}

	@Override
	public CodeGenerator visitReturnStatement(Java8Parser.ReturnStatementContext ctx) {
		shouldPutResultOfConditionalOnStack = true;
		expectedExpressionReturnType = expectedMethodReturnType;

		if (ctx.expression() != null) {
			visitChildren(ctx);
			potentiallyVisitFinallyClause();
			ClassProxy typeToReturn = codeGenerator.stack().pop();
			codeGenerator.add(ByteCodes.returnTypeForDescriptor(typeToReturn));
		} else {
			potentiallyVisitFinallyClause();
			codeGenerator.add(ByteCode.RETURN);
		}
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitAssignment(Java8Parser.AssignmentContext ctx) {
		shouldPutResultOfConditionalOnStack = true;
		if (ctx.assignmentOperator().Equals() != null) {
			Java8Parser.LeftHandSideContext leftHandSideContext = ctx.leftHandSide();
			Java8Parser.FieldAccessContext fieldAccessContext = leftHandSideContext.fieldAccess();
			if (fieldAccessContext != null) {

				visitChildren(fieldAccessContext.primary());
				String fieldName = fieldAccessContext.Identifier().getText();

				ClassProxy typeOnTopOfStack = codeGenerator.stack().peek();

				FieldProxy field = typeOnTopOfStack.getField(fieldName).orElseThrow(unknownFieldAt(ctx));
				expectedExpressionReturnType = field.getType();

				// "execute" the RHS of the expression, so that we have something to assign to the field
				visitChildren(ctx.expression());

				short fieldrefIndex = classFileGenerator.constantPoolReference(field);
				codeGenerator.add(ByteCode.PUTFIELD, fieldrefIndex);
				codeGenerator.stack().popN(2);
				return codeGenerator;
			} else {
				Java8Parser.ExpressionNameContext expressionName = leftHandSideContext.expressionName();
				if (expressionName != null) {

					// NOTE: This is similar to visitExpressionNameContext(), but since we're interested in *storing*, 
					// not *loading*, we have to do a separate thing here.
					// todo: investigate merging the two
					// I tried merging the two, and it was a fantastic pain in the ass. Perhaps we can try again later some time... =(
					List<String> expressionNameAsList = reverseExpressionName(expressionName);
					Iterator<String> nameParts = expressionNameAsList.iterator();

					ClassProxy fieldOwnerType = null;
					FieldProxy field = null;
					LocalVariable localVariable = null;
					while (nameParts.hasNext()) {
						String namePart = nameParts.next();
						if (fieldOwnerType == null) {
							// check local variable
							localVariable = codeGenerator.variables().getLocalVariable(namePart);
							if (localVariable != null) {
								if (nameParts.hasNext()) {
									// there's another dot, so load this variable and continue looping
									ByteCodes.load(localVariable, codeGenerator);
									fieldOwnerType = localVariable.getType();
									continue;
								} else {
									// we're done, break and get to the assigning of the variable
									break;
								}
							}

							// check field
							Optional<FieldProxy> optionalClassField = findFieldInInheritanceHierarchy(namePart, THIS);
							if (optionalClassField.isPresent()) {
								field = optionalClassField.get();
								pushThisOntoTheStack();
								if (nameParts.hasNext()) {
									// there's another dot, so load this field and continue looping
									short classFieldrefIndex = classFileGenerator.constantPoolReference(field);
									codeGenerator.add(ByteCode.GETFIELD, classFieldrefIndex);
									codeGenerator.stack().push(field.getType());
									fieldOwnerType = field.getType();
									continue;
								} else {
									// we're done, break and get to the assigning of the field
									break;
								}
							}
							// check statically imported field
							Optional<String> fieldOwnerClassName = classFileGenerator.getImports().resolveStaticField(namePart);
							if (fieldOwnerClassName.isPresent()) {
								fieldOwnerType = ClassProxy.forName(fieldOwnerClassName.get()).orElseThrow(unknownClassAt(expressionName));
								field = fieldOwnerType.getField(namePart).orElseThrow(unknownFieldAt(expressionName));
								if (nameParts.hasNext()) {
									// there's another dot, so load this statically imported field and continue looping
									short staticClassFieldrefIndex = classFileGenerator.constantPoolReference(field);
									codeGenerator.add(ByteCode.GETSTATIC, staticClassFieldrefIndex);
									codeGenerator.stack().push(field.getType());
									fieldOwnerType = field.getType();
									continue;
								} else {
									// we're done, break and get to the assigning of the static field
									break;
								}
							}
							// check statically imported class
							Optional<String> type = classFileGenerator.getImports().resolveType(namePart);
							if (type.isPresent()) {
								if (nameParts.hasNext()) {
									// there's another dot, so load this type and continue looping
									fieldOwnerType = ClassProxy.forName(type.get()).orElseThrow(unknownClassAt(expressionName));
									continue;
								} else {
									// we're done, break and get to the assigning of the field
									break;
								}
							}
							// try the whole thing, minus the last part, as an FQN.
							// imagine net.jevring.javac.MyType.myField = 12;
							if (nameParts.hasNext()) {
								String typeName = expressionNameAsList.stream().limit(expressionNameAsList.size() - 1).collect(Collectors.joining("."));
								fieldOwnerType = ClassProxy.forName(typeName).orElseThrow(unknownClassAt(expressionName));
								field = fieldOwnerType.getField(namePart).orElseThrow(unknownFieldAt(expressionName));
								// since we've drained the whole iterator, effectively, we don't want to loop anymore
								break;
							} else {
								throw new CompilerException(expressionName.start, "Unknown field: " + namePart);
							}
						} else {
							field = fieldOwnerType.getField(namePart).orElseThrow(unknownFieldAt(expressionName));
							fieldOwnerType = field.getType();
							if (nameParts.hasNext()) {
								short fieldrefIndex = classFileGenerator.constantPoolReference(field);
								if (Modifier.isStatic(field.getModifiers())) {
									codeGenerator.add(ByteCode.GETSTATIC, fieldrefIndex);
									// todo: if this is a chain of static field fetches, we can either short-circuit them, or POP the loads from the stack, since they're not needed
								} else {
									// we assume that whatever owns the field is already on the stack 
									codeGenerator.add(ByteCode.GETFIELD, fieldrefIndex);
									codeGenerator.stack().pop();
								}
								codeGenerator.stack().push(field.getType());
							}
							// with more than one dot, the result can never be a local variable
							localVariable = null;
						}
					}
					if (localVariable != null) {
						expectedExpressionReturnType = localVariable.getType();

						// "execute" the RHS of the expression, so that we have something to assign to the variable
						visitChildren(ctx.expression());

						ByteCodes.store(localVariable, codeGenerator);
						localVariable.startScope(codeGenerator.getNextByteCodeArrayIndex());
					} else if (field != null) {
						short fieldrefIndex = classFileGenerator.constantPoolReference(field);
						if (Modifier.isStatic(field.getModifiers())) {
							expectedExpressionReturnType = field.getType();
							// "execute" the RHS of the expression, so that we have something to assign to the variable
							visitChildren(ctx.expression());
							codeGenerator.add(ByteCode.PUTSTATIC, fieldrefIndex);
							codeGenerator.stack().pop();
						} else {
							expectedExpressionReturnType = field.getType();
							// "execute" the RHS of the expression, so that we have something to assign to the variable
							visitChildren(ctx.expression());
							codeGenerator.add(ByteCode.PUTFIELD, fieldrefIndex);
							codeGenerator.stack().popN(2);
						}

					} else {
						throw new CompilerException(expressionName.start,
						                            "Could not resolve expression: " + expressionNameAsList.stream().collect(Collectors.joining(".")));
					}
					return codeGenerator;
				} else {
					Java8Parser.ArrayAccessContext arrayAccessContext = leftHandSideContext.arrayAccess();
					if (arrayAccessContext != null) {
						throw new CompilerException(ctx.start, "Array access not supported yet");
					}
				}
			}
		}
		// todo: support more assignments than equals!
		throw new CompilerException(ctx.start, "todo: support more assignments than equals!");
	}

	@Override
	public CodeGenerator visitAdditiveExpression(Java8Parser.AdditiveExpressionContext ctx) {
		if (ctx.Plus() != null) {
			// todo: right now this will likely generate more StringBuilders than necessary, if we have more than two things to concatenate, but we can optimize that in the future

			boolean appendStrings = expectedExpressionReturnType.equals(ClassProxy.STRING);
			if (!appendStrings && notCurrentlyPreviewingNode(ctx.additiveExpression()) && notCurrentlyPreviewingNode(ctx.multiplicativeExpression())) {
				// todo: is this the right thing to do? Why doesn't it work with the old approach of evaluating everything?
				// under which conditions does that fail, and why?
				appendStrings = atLeastOneStringOnStack(ctx.additiveExpression()) || atLeastOneStringOnStack(ctx.multiplicativeExpression());
			}

			if (appendStrings) {
				StringConcatenationHelper stringConcatenationHelper = new StringConcatenationHelper(classFileGenerator, codeGenerator);
				stringConcatenationHelper.newStringBuilder();
				visit(ctx.additiveExpression());
				stringConcatenationHelper.callAppend();
				visit(ctx.multiplicativeExpression());
				stringConcatenationHelper.callAppend();
				stringConcatenationHelper.callToString();
			} else {
				// todo: support type conversion as well
				visit(ctx.additiveExpression());
				visit(ctx.multiplicativeExpression());
				if (expectedExpressionReturnType.equals(ClassProxy.INT)) {
					codeGenerator.add(ByteCode.IADD);
					codeGenerator.stack().push(ClassProxy.INT);
				} else {
					throw new CompilerException(ctx.start, "Types other than int not supported yet for addition");
				}
			}
			return codeGenerator;
		} else if (ctx.Minus() != null) {
			// todo: support type conversion as well
			visit(ctx.additiveExpression());
			visit(ctx.multiplicativeExpression());
			if (expectedExpressionReturnType.equals(ClassProxy.INT)) {
				codeGenerator.add(ByteCode.ISUB);
				codeGenerator.stack().push(ClassProxy.INT);
			} else {
				throw new CompilerException(ctx.start, "Types other than int not supported yet for subtraction");
			}
			return codeGenerator;
		} else {
			return visit(ctx.multiplicativeExpression());
		}
	}

	private boolean atLeastOneStringOnStack(ParserRuleContext ctx) {
		ExpressionVisitor previewVisitor = createPreviewVisitor(ctx);
		TrackingStack previewStack = previewVisitor.codeGenerator.stack();
		ctx.accept(previewVisitor);
		boolean atLeastOneStringOnPreviewStack = false;
		int newItemsAddedToStack = previewStack.getCurrentHeight() - codeGenerator.stack().getCurrentHeight();
		for (int i = 0; i < newItemsAddedToStack; i++) {
			if (previewStack.pop().equals(ClassProxy.STRING)) {
				atLeastOneStringOnPreviewStack = true;
				break;
			}
		}
		return atLeastOneStringOnPreviewStack;
	}

	private boolean notCurrentlyPreviewingNode(ParserRuleContext ctx) {
		return !previewNodes.contains(ctx);
	}

	private ExpressionVisitor createPreviewVisitor(ParserRuleContext node) {
		ConstantPool constantPool = classFileGenerator.getConstantPool();
		TrackingStack previewStack = new TrackingStack(codeGenerator.stack());
		CodeGenerator previewCodeGenerator =
				new CodeGenerator(codeGenerator.variables(), codeGenerator.lineNumbers(), new StackMapTableGenerator(constantPool), constantPool, previewStack);
		ExpressionVisitor previewVisitor = new ExpressionVisitor(classFileGenerator,
		                                                         expectedMethodReturnType,
		                                                         previewCodeGenerator,
		                                                         "preview: [" + contextHint + " -> " + (node == null ? "node was null?" : node.getText()) + "]");
		previewVisitor.previewNodes.addAll(previewNodes);
		previewVisitor.previewNodes.add(node);
		previewVisitor.shouldReverseBranchCondition = this.shouldReverseBranchCondition;
		previewVisitor.shouldPutResultOfConditionalOnStack = this.shouldPutResultOfConditionalOnStack;
		return previewVisitor;
	}

	// todo: keep in mind that the PREfix things leave a different value on the stack
	@Override
	public CodeGenerator visitPostIncrementExpression(Java8Parser.PostIncrementExpressionContext ctx) {
		return visitPostFixExpression(ctx.postfixExpression(), (byte) 1, ByteCode.IADD);
	}

	@Override
	public CodeGenerator visitPostDecrementExpression(Java8Parser.PostDecrementExpressionContext ctx) {
		return visitPostFixExpression(ctx.postfixExpression(), (byte) -1, ByteCode.ISUB);
	}

	private CodeGenerator visitPostFixExpression(Java8Parser.PostfixExpressionContext ctx, byte incrementIfLocalVariable, ByteCode instructionIfField) {
		// if this is a variable, use IINC. If it's a field, use IADD
		if (!expectedExpressionReturnType.equals(ClassProxy.INT) && !expectedExpressionReturnType.equals(ClassProxy.LONG)) {
			throw new CompilerException(ctx.start, "Postfix expressions not supported on any other data type than int");
		}

		Java8Parser.ExpressionNameContext expressionName = ctx.expressionName();
		String name = expressionName.getText();
		if (name != null) {
			if (name.contains(".")) {
				// there are dots, treat this as a full expression name
				throw new CompilerException(ctx.start, "postfix expression not supported on expression names yet");
			} else {
				LocalVariable localVariable = codeGenerator.variables().getLocalVariable(name);
				if (localVariable != null) {
					codeGenerator.add(ByteCode.IINC, localVariable.getSlot(), incrementIfLocalVariable);
					// doesn't change the stack
					return codeGenerator;
				}

				pushThisOntoTheStack();
				visitChildren(ctx);
				codeGenerator.add(ByteCode.ICONST_1);
				codeGenerator.add(instructionIfField);
				Field field = classFileGenerator.getFieldsGenerator().getField(name).orElseThrow(unknownFieldAt(ctx));
				codeGenerator.add(ByteCode.PUTFIELD, classFileGenerator.constantPoolReference(field));
				codeGenerator.stack().popN(2);
			}
		} else {
			throw new CompilerException(ctx.start, "Postfix expression " + ctx.getText() + " not supported yet");
		}
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitThrowStatement(Java8Parser.ThrowStatementContext ctx) {
		expectedExpressionReturnType = ClassProxy.THROWABLE;
		visit(ctx.expression());
//		potentiallyVisitFinallyClause(); // todo: this causes a stack overflow exception. why?
		codeGenerator.add(ByteCode.ATHROW);
		return codeGenerator;
	}

	@Override
	public CodeGenerator visitTryStatement(Java8Parser.TryStatementContext ctx) {
		Java8Parser.CatchesContext catches = ctx.catches();
		Java8Parser.XFinallyContext xFinally = ctx.xFinally();
		if (catches == null && xFinally == null) {
			throw new CompilerException(ctx.stop, "Missing 'catch' statement");
		}
		List<PreparedShortJumpOffset> endOfTryCatchFinallyBlockOffsets = new ArrayList<>();
		List<PendingByteArrayIndex> finallyBlockReferences = new ArrayList<>();
		short tryBlockStart = codeGenerator.getNextByteCodeArrayIndex();
		if (xFinally != null) {
			finallyContext.push(xFinally);
		}
		visit(ctx.block());
		short tryBlockEnd = codeGenerator.getNextByteCodeArrayIndex();
		PendingByteArrayIndex mainBodyJumpToFinallyClause = new PendingByteArrayIndex();
		finallyBlockReferences.add(mainBodyJumpToFinallyClause);
		visitFinallyClauseUnlessAlreadyConsumed(xFinally);

		if (!codeGenerator.lastOpIsThrow() && !codeGenerator.lastOpIsReturn()) {
			// todo: only do this if there is *actually* something after the blocks
			PreparedShortJumpOffset endOfTryCatchFinallyBlockOffset = new PreparedShortJumpOffset(codeGenerator.getNextByteCodeArrayIndex());
			endOfTryCatchFinallyBlockOffsets.add(endOfTryCatchFinallyBlockOffset);
			codeGenerator.add(ByteCode.GOTO, endOfTryCatchFinallyBlockOffset);
		}
		if (catches != null) {
			for (Java8Parser.CatchClauseContext catchClause : catches.catchClause()) {
				codeGenerator.lineNumbers().add(codeGenerator.getNextByteCodeArrayIndex(), catchClause.start.getLine());

				Java8Parser.CatchFormalParameterContext catchFormalParameter = catchClause.catchFormalParameter();
				// todo: support 'final' and annotations (can they really occur in all these places, annotations?)
				Java8Parser.VariableDeclaratorIdContext variableDeclaratorId = catchFormalParameter.variableDeclaratorId();
				if (variableDeclaratorId.arrayDimensions() != null) {
					throw new CompilerException(variableDeclaratorId.arrayDimensions().start, "Can't catch an array of exceptions");
				}
				String caughtExceptionVariableName = variableDeclaratorId.Identifier().getText();
				Java8Parser.CatchTypeContext catchType = catchFormalParameter.catchType();
				List<ClassProxy> caughtExceptions = new ArrayList<>();
				ClassProxyResolverVisitor classProxyResolverVisitor = new ClassProxyResolverVisitor(classFileGenerator.getImports());

				ClassProxy firstException = catchType.unannotatedClassType().accept(classProxyResolverVisitor);
				// todo: find out what *type* this exception should be


				// todo: apparently this also needs to be done before we return from the try-catch block
				// todo: we're also generating a GOTO when we already have a RETURN

				LocalVariable exceptionVariable = codeGenerator.variables().addLocalVariable(firstException, caughtExceptionVariableName);
				// apparently we have to do an ASTORE here for the exception variable.
				// I'm sure that's in the spec somewhere, but it was a surprise seeing it in javap.
				// I thought the exception handler did that automatically, but I shouldn't have been so naive.
				codeGenerator.stack().push(firstException);
				short exceptionVariableWrite = codeGenerator.getNextByteCodeArrayIndex();
				ByteCodes.store(exceptionVariable, codeGenerator);
				short catchClauseStart = codeGenerator.getNextByteCodeArrayIndex();
				// we have to fake that this variable is one depth down, as the block hasn't *technically* started yet
				exceptionVariable.startScope(catchClauseStart, codeGenerator.variables().getDepth() + 1);
				caughtExceptions.add(firstException);

				for (Java8Parser.ClassOrInterfaceTypeContext caughtException : catchType.classOrInterfaceType()) {
					caughtExceptions.add(caughtException.accept(classProxyResolverVisitor));
				}
				for (ClassProxy caughtException : caughtExceptions) {
					codeGenerator.add(new Code.Exception(tryBlockStart,
					                                     tryBlockEnd,
					                                     new PendingByteArrayIndex(exceptionVariableWrite),
					                                     classFileGenerator.constantPoolReference(caughtException)));
				}
				visit(catchClause.block());
				short catchBlockEnd = codeGenerator.getNextByteCodeArrayIndex();
				if (xFinally != null) {
					visitFinallyClauseUnlessAlreadyConsumed(xFinally);
					PendingByteArrayIndex jumpToFinallyClause = new PendingByteArrayIndex();
					finallyBlockReferences.add(jumpToFinallyClause);
					codeGenerator.add(new Code.Exception(exceptionVariableWrite, catchBlockEnd, jumpToFinallyClause, (short) 0));
				}
				if (!codeGenerator.lastOpIsThrow() && !codeGenerator.lastOpIsReturn()) {
					// todo: only do this if there is *actually* something after the blocks
					PreparedShortJumpOffset endOfTryCatchFinallyBlockOffset = new PreparedShortJumpOffset(codeGenerator.getNextByteCodeArrayIndex());
					endOfTryCatchFinallyBlockOffsets.add(endOfTryCatchFinallyBlockOffset);
					codeGenerator.add(ByteCode.GOTO, endOfTryCatchFinallyBlockOffset);
				}
			}
		}
		if (xFinally != null) {
			// this exception table entry is added here, as all 'finally' blocks need to be added *after* the 'catch' blocks
			codeGenerator.add(new Code.Exception(tryBlockStart, tryBlockEnd, mainBodyJumpToFinallyClause, (short) 0));

			short finallyClauseStart = codeGenerator.getNextByteCodeArrayIndex();
			short weirdHiddenVariableEnd = visitFinallyClause(xFinally);
			if (weirdHiddenVariableEnd != -1) {
				// todo: why should there be a finally guard around the finally clause itself? This makes no sense. javac does this, however, so we should too...
				// todo: this isn't done for try-finally. why?
				codeGenerator.add(new Code.Exception(finallyClauseStart, weirdHiddenVariableEnd, new PendingByteArrayIndex(finallyClauseStart), (short) 0));
			}
			for (PendingByteArrayIndex finallyBlockReference : finallyBlockReferences) {
				finallyBlockReference.setValue(finallyClauseStart);
			}
			codeGenerator.add(ByteCode.ATHROW);

		}
		for (PreparedShortJumpOffset endOfTryCatchFinallyBlockOffset : endOfTryCatchFinallyBlockOffsets) {
			endOfTryCatchFinallyBlockOffset.setJumpOffsetToInstruction(codeGenerator.getNextByteCodeArrayIndex());
		}
		cleanUpFinallyContext(xFinally);
		return codeGenerator;
	}

	private void cleanUpFinallyContext(Java8Parser.XFinallyContext xFinally) {
		if (!finallyContext.empty() && finallyContext.peek() == xFinally) {
			// clean up our finally-state after us
			finallyContext.pop();
		}
	}

	private void visitFinallyClauseUnlessAlreadyConsumed(Java8Parser.XFinallyContext xFinally) {
		if (!finallyContext.empty() && finallyContext.peek() == xFinally) {
			// if it's not already been consumed by a 'return' or 'throw' statement, consume it here.
			visitFinallyClause(finallyContext.peek());

			// the finally block handler has to be added to *every* catch-block and the normal block, so that it's executed around there as well
			// the target block remains the same, but the handler is registered for each catch block
			// the actual *body* of the finally-statement is duplicated at the end of each block. odd. I would have assumed a goto.
		}
	}

	private short visitFinallyClause(Java8Parser.XFinallyContext xFinally) {
		// store whatever's currently on top of the stack, so that we can return it later, no matter what the finally-block has done to the stack
		boolean storeHiddenVariable = !codeGenerator.stack().isEmpty();
		if (storeHiddenVariable) {
			ClassProxy currentTopOfStack = codeGenerator.stack().peek();
			LocalVariable hiddenExceptionVariable = codeGenerator.variables().addLocalVariable(currentTopOfStack, HIDDEN);
			hiddenExceptionVariable.startScope(codeGenerator.getNextByteCodeArrayIndex());
			codeGenerator.stack().push(currentTopOfStack);
			ByteCodes.store(hiddenExceptionVariable, codeGenerator);
			short hiddenVariableEnd = codeGenerator.getNextByteCodeArrayIndex();
			visit(xFinally.block());
			ByteCodes.load(hiddenExceptionVariable, codeGenerator);
			return hiddenVariableEnd;
		} else {
			visit(xFinally.block());
			// todo: check for this, or make it optional
			return -1;
		}
	}                                                                                                                                   

	private void potentiallyVisitFinallyClause() {
		if (!finallyContext.empty()) {
			visitFinallyClause(finallyContext.peek());
		}
	}

	private interface JumpInstructionDeterminator {
		ByteCode determineByteCode();
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.proxies.ClassProxy;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class FormalParameterVisitor extends AggregateJava8Visitor<LocalVariableTableGenerator> {
	private final Imports imports;

	public FormalParameterVisitor(LocalVariableTableGenerator localVariables, Imports imports) {
		super(localVariables);
		this.imports = imports;
	}

	@Override
	public LocalVariableTableGenerator visitReceiverParameter(Java8Parser.ReceiverParameterContext ctx) {
		// https://docs.oracle.com/javase/specs/jls/se8/html/jls-8.html#jls-ReceiverParameter
		throw new AssertionError("Should never see receiver parameters");
	}

	@Override
	public LocalVariableTableGenerator visitFormalParameter(Java8Parser.FormalParameterContext ctx) {
		// todo: what should we do about the access flags (and annotations)? 
		// they don't fit into the local variable table. Are they used for other things perhaps?
		// AccessFlags accessFlags = ctx.accept(new VariableModifierVisitor()).build();

		ClassProxy type = ctx.unannotatedType().accept(new ClassProxyResolverVisitor(imports));
		if (ctx.variableDeclaratorId().arrayDimensions() != null) {
			// todo: we never end up here, because this isn't where the array dimensions are. Unless you can do "String args[]", which I really hope you can't...
			throw new UnsupportedOperationException("We don't support arrays yet");
		}
		String name = ctx.variableDeclaratorId().Identifier().getText();
		aggregate.addMethodVariable(type, name);
		return aggregate;
	}

	@Override
	public LocalVariableTableGenerator visitLastFormalParameter(Java8Parser.LastFormalParameterContext ctx) {
		if (ctx.formalParameter() != null) {
			return visitFormalParameter(ctx.formalParameter());
		} else {
			// todo: this has annotations as modifiers AND before the ellipsis. Wtf?!
			ClassProxy type = ctx.unannotatedType().accept(new ClassProxyResolverVisitor(imports));
			if (ctx.variableDeclaratorId().arrayDimensions() != null) {
				throw new UnsupportedOperationException("We don't support arrays yet");
			}
			if (ctx.Elipsis() != null) {
				throw new UnsupportedOperationException("We don't support varargs yet");
			}
			String name = ctx.variableDeclaratorId().Identifier().getText();
			aggregate.addMethodVariable(type, name);
			return aggregate;
		}
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.Imports;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @deprecated Switch to ClassProxyResolverVisitor instead
 */
@Deprecated
public class ClassOrInterfaceTypeVisitor extends Java8BaseVisitor<String> {
	private final Imports imports;

	public ClassOrInterfaceTypeVisitor(Imports imports) {
		this.imports = imports;
	}

	@Override
	public String visitClassOrInterfaceType(Java8Parser.ClassOrInterfaceTypeContext ctx) {
		if (ctx.classOrInterfaceType() == null) {
			return imports.resolveType(ctx.Identifier().getText())
			              .orElseThrow(() -> new CompilerException(ctx.start, String.format("Could not resolve interface %s", ctx.getText())));
		} else {
			// do the backwards walk, like for the package name
			return reverseClassOrInterfaceType(ctx).stream().collect(Collectors.joining("."));
		}
	}

	private List<String> reverseClassOrInterfaceType(Java8Parser.ClassOrInterfaceTypeContext ctx) {
		Java8Parser.ClassOrInterfaceTypeContext part = ctx.classOrInterfaceType();
		LinkedList<String> parts = new LinkedList<>();
		while (part != null) {
			parts.addFirst(part.Identifier().getText());
			part = part.classOrInterfaceType();
		}
		parts.addLast(ctx.Identifier().getText());
		return parts;
	}

}

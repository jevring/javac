/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.ClassPath;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.output.structure.ConstantPool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class CompilationUnitVisitor extends Java8BaseVisitor<ClassFileGenerator> {
	private final ConstantPool constantPool = new ConstantPool();
	private final ClassPath classPath;

	public CompilationUnitVisitor(ClassPath classPath) {
		this.classPath = classPath;
	}

	@Override
	public ClassFileGenerator visitCompilationUnit(Java8Parser.CompilationUnitContext ctx) {
		// todo: this got a lot uglier when we had to specify which thing to visit...
		// we have to do that, though, because the *combination* of visiting children basically only take the last node.
		// this makes sense, but it's still somewhat annoying =(

		List<String> packageName = new ArrayList<>();
		if (ctx.packageDeclaration() != null) {
			packageName = ctx.packageDeclaration().accept(new Java8BaseVisitor<List<String>>() {
				@Override
				public List<String> visitPackageDeclaration(Java8Parser.PackageDeclarationContext ctx) {
					Java8Parser.PackageNameContext packageNamePart = ctx.packageName();
					LinkedList<String> packageName = new LinkedList<>();
					while (packageNamePart != null) {
						packageName.addFirst(packageNamePart.Identifier().getText());
						packageNamePart = packageNamePart.packageName();
					}
					return packageName;
				}
			});
		}
		Imports imports = new Imports(packageName, classPath);
		for (Java8Parser.ImportDeclarationContext importDeclarationContext : ctx.importDeclaration()) {
			importDeclarationContext.accept(imports);
		}
		// todo: there can actually be 0 or 1 OR MORE type declarations per file, but deal with all those cases later
		// we can handle that by going over the list of type declarations here, but that's another story. 
		// we could likely generate multiple class files with the same SourceFile attribute.
		return ctx.typeDeclaration(0).accept(new TypeVisitor(packageName.stream().collect(Collectors.joining(".")), constantPool, imports));
	}
}

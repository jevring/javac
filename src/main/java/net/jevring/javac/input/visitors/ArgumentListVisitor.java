/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.proxies.ClassProxy;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ArgumentListVisitor extends Java8BaseVisitor<ClassProxy[]> {
	private final ExpressionVisitor expressionVisitor;
	private final CodeGenerator codeGenerator;

	public ArgumentListVisitor(ExpressionVisitor expressionVisitor, CodeGenerator codeGenerator) {
		this.expressionVisitor = expressionVisitor;
		this.codeGenerator = codeGenerator;
	}

	@Override
	public ClassProxy[] visitArgumentList(Java8Parser.ArgumentListContext ctx) {
		List<ClassProxy> signature = new ArrayList<>();
		if (ctx != null) {
			for (Java8Parser.ExpressionContext expression : ctx.expression()) {
				// todo: we need to be able to set the expected expression return type here, so that we can properly do string concatenation inline as a part of a call to a method
				expressionVisitor.visit(expression);
				ClassProxy clazz = codeGenerator.stack().peek();
				signature.add(clazz);
			}
		}
		return signature.toArray(new ClassProxy[signature.size()]);
	}

}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.visitors;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.fields.Field;
import net.jevring.javac.generators.methods.Method;
import net.jevring.javac.generators.methods.MethodSignature;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.grammar.Java8BaseVisitor;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.intermediate.ConstructorInitializer;
import net.jevring.javac.input.intermediate.FieldInitializer;
import net.jevring.javac.input.intermediate.MethodInitializer;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ClassBodyVisitor extends Java8BaseVisitor<ClassFileGenerator> {
	private final ClassFileGenerator classFileGenerator;
	private final ConstantPool constantPool;
	private final CodeGenerator clinit;
	private final Imports imports;

	public ClassBodyVisitor(ClassFileGenerator classFileGenerator, ConstantPool constantPool, CodeGenerator clinit, Imports imports) {
		this.classFileGenerator = classFileGenerator;
		this.constantPool = constantPool;
		this.clinit = clinit;
		this.imports = imports;
	}

	@Override
	public ClassFileGenerator visitStaticInitializer(Java8Parser.StaticInitializerContext ctx) {
		ctx.accept(new ExpressionVisitor(classFileGenerator,
		                                 ClassProxy.VOID,
		                                 clinit,
		                                 String.format("<static initializer in class: %s>", classFileGenerator.getName())));
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) {
		// this pattern works when we're visiting *multiple* things of the same kind, like modifiers
		AccessFlags accessFlags = ctx.accept(new FieldModifierVisitor()).build();

		ClassProxy fieldType = ctx.unannotatedType().accept(new ClassProxyResolverVisitor(imports));
		for (Java8Parser.VariableDeclarationContext variableDeclarationContext : ctx.variableDeclaratorList().variableDeclaration()) {
			String fieldName = variableDeclarationContext.variableDeclaratorId().getText();
			Field field = classFileGenerator.getFieldsGenerator().addField(accessFlags, fieldType, fieldName);
			// todo: deal with array dimensions and array initialization
			if (variableDeclarationContext.Equals() != null) {
				Java8Parser.ExpressionContext initializerExpression = variableDeclarationContext.variableInitializer().expression();
				short fieldrefIndex = classFileGenerator.constantPoolReference(field);
				if (accessFlags.getFlags().contains(Flag.ACC_STATIC)) {
					initializerExpression.accept(new ExpressionVisitor(classFileGenerator,
					                                                   Descriptor.to(field.getDescriptor()),
					                                                   clinit,
					                                                   String.format("<static field initializer: %s>", fieldName)));
					clinit.add(ByteCode.PUTSTATIC, fieldrefIndex);
				} else {
					classFileGenerator.getFieldInitializers()
					                  .add(new FieldInitializer(initializerExpression, Descriptor.to(field.getDescriptor()), fieldrefIndex));
				}
			}
		}
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitConstructorDeclaration(Java8Parser.ConstructorDeclarationContext ctx) {
		// todo: this currently requires constructors to come AFTER fields. Make sure we support any order.

		// this pattern works when we're visiting *multiple* things of the same kind, like modifiers
		AccessFlags accessFlags = ctx.accept(new ConstructorModifierVisitor()).build();
		// todo: handle typeParameters

		LocalVariableTableGenerator localVariables = new LocalVariableTableGenerator(constantPool);
		localVariables.addMethodVariable(ClassProxy.forClass(classFileGenerator), "this"); // first variable is always 'this'

		Java8Parser.ConstructorDeclaratorContext constructorDeclarator = ctx.constructorDeclarator();
		constructorDeclarator.accept(new FormalParameterVisitor(localVariables, imports));
		MethodSignature methodSignature = localVariables.generateMethodSignature(ClassProxy.VOID);
		String name = constructorDeclarator.simpleTypeName().Identifier().getText();
		if (!name.equals(classFileGenerator.getName())) {
			throw new CompilerException(constructorDeclarator.simpleTypeName().start, "Constructor does not have the same name as the class");
		}

		Method method = classFileGenerator.getMethodsGenerator().addMethod(accessFlags, methodSignature, MethodsGenerator.CONSTRUCTOR_NAME);
		if (ctx.xThrows() != null) {
			ctx.xThrows().accept(new ThrowsVisitor(constantPool, imports)).ifPresent(method::addAttribute);
		}
		classFileGenerator.getConstructorInitializers().add(new ConstructorInitializer(ctx.constructorBody(), localVariables, method));
		return classFileGenerator;
	}

	@Override
	public ClassFileGenerator visitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
		// this pattern works when we're visiting *multiple* things of the same kind, like modifiers
		AccessFlags accessFlags = ctx.accept(new MethodModifierVisitor()).build();

		CodeGenerator codeGenerator = new CodeGenerator(constantPool);
		LocalVariableTableGenerator localVariables = codeGenerator.variables();

		if (!accessFlags.getFlags().contains(Flag.ACC_STATIC)) {
			localVariables.addMethodVariable(ClassProxy.forClass(classFileGenerator), "this"); // first variable is always 'this'
		}
		// todo: handle annotations & type parameters & exceptions

		ctx.methodHeader().methodDeclarator().accept(new FormalParameterVisitor(localVariables, imports));

		String name = ctx.methodHeader().methodDeclarator().Identifier().getText();
		ClassProxy returnType;
		if (ctx.methodHeader().result().Void() != null) {
			returnType = ClassProxy.VOID;
		} else {
			returnType = ctx.methodHeader().result().unannotatedType().accept(new ClassProxyResolverVisitor(imports));
		}
		MethodSignature methodSignature = localVariables.generateMethodSignature(returnType);
		// todo: generate net.jevring.javac.output.structure.attributes.MethodParameters

		// todo: also, only do this if the method actually HAS a method body (isn't abstract, and isn't an interface method)
		Method method = classFileGenerator.getMethodsGenerator().addMethod(accessFlags, methodSignature, name);

		if (ctx.methodHeader().xThrows() != null) {
			ctx.methodHeader().xThrows().accept(new ThrowsVisitor(constantPool, imports)).ifPresent(method::addAttribute);
		}
		classFileGenerator.getMethodInitializers().add(new MethodInitializer(ctx.methodBody(), methodSignature, codeGenerator, method));
		return classFileGenerator;
	}
}

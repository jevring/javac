/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input;

import net.jevring.javac.input.proxies.ClassProxy;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.EmptyStackException;

/**
 * A stack that keeps track of it's max height.
 *
 * @author markus@jevring.net
 */
public class TrackingStack {
	private final Deque<ClassProxy> stack;
	private int currentHeight = 0;
	private int maxHeight = 0;

	public TrackingStack() {
		stack = new ArrayDeque<>();
	}

	public TrackingStack(TrackingStack existing) {
		stack = new ArrayDeque<>(existing.stack);
		currentHeight = existing.currentHeight;
		maxHeight = existing.maxHeight;
	}

	public void push(ClassProxy item) {
		stack.push(item);
		if (item.isDoubleSize()) {
			currentHeight += 2;
		} else {
			currentHeight += 1;
		}
		maxHeight = Math.max(maxHeight, currentHeight);
	}

	public ClassProxy pop() {
		ClassProxy poppedItem = stack.pop();
		if (poppedItem.isDoubleSize()) {
			currentHeight -= 2;
		} else {
			currentHeight -= 1;
		}
		return poppedItem;
	}

	public boolean isEmpty() {
		return stack.isEmpty();
	}

	public ClassProxy peek() {
		ClassProxy head = stack.peek();
		if (head == null) {
			throw new EmptyStackException();
		}
		return head;
	}

	public void popN(int timesToPop) {
		for (int i = 0; i < timesToPop; i++) {
			pop();
		}
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public int getCurrentHeight() {
		return currentHeight;
	}

	@Override
	public String toString() {
		return "TrackingStack{" + "currentHeight=" + currentHeight + ", maxHeight=" + maxHeight + '}';
	}
}

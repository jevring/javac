/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.intermediate;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.methods.Method;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.CompilerException;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.input.proxies.MethodProxy;
import net.jevring.javac.input.visitors.ConstructorBodyVisitor;
import net.jevring.javac.input.visitors.ExpressionVisitor;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.constantpool.MethodrefInfo;
import net.jevring.javac.output.structure.constantpool.NameAndTypeInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ConstructorInitializer {
	private final Java8Parser.ConstructorBodyContext constructorBodyContext;
	private final LocalVariableTableGenerator localVariables;
	private final Method constructor;

	public ConstructorInitializer(Java8Parser.ConstructorBodyContext constructorBodyContext, LocalVariableTableGenerator localVariables, Method constructor) {
		this.constructorBodyContext = constructorBodyContext;
		this.localVariables = localVariables;
		this.constructor = constructor;
	}

	public void callSuperConstructor(ClassFileGenerator classFileGenerator, ConstantPool constantPool, CodeGenerator codeGenerator) {
		if (constructorBodyContext != null) {
			Java8Parser.ExplicitConstructorInvocationContext explicitConstructorInvocation = constructorBodyContext.explicitConstructorInvocation();
			if (explicitConstructorInvocation == null) {
				invokeSuperConstructor(classFileGenerator, constantPool, codeGenerator);
			} else {
				// what the hell are these other ways of invoking a constructor? I've never seem them, I don't think
				// todo: support type arguments. not even sure what that would look like
				// todo: support expressionName Dot...
				// todo: support primary Dot...
				codeGenerator.add(ByteCode.ALOAD_0); // load "this"
				ClassProxy constructorOwner;

				if (explicitConstructorInvocation.Super() != null) {
					constructorOwner = classFileGenerator.getSuperClass();
				} else if (explicitConstructorInvocation.This() != null) {
					constructorOwner = ClassProxy.forClass(classFileGenerator);
				} else {
					throw new CompilerException(explicitConstructorInvocation.start, "This kind of explicit super constructor invocation is not supported");
				}
				ClassProxy[] parameterTypes = handleArgumentListAndGetSignature(explicitConstructorInvocation.argumentList(),
				                                                                codeGenerator,
				                                                                new ExpressionVisitor(classFileGenerator,
				                                                                                      ClassProxy.VOID,
				                                                                                      codeGenerator,
				                                                                                      "<super ctor>"));
				MethodProxy constructor = constructorOwner.getConstructor(parameterTypes)
				                                          .orElseThrow(() -> new CompilerException(explicitConstructorInvocation.start,
				                                                                                   "Could not find a constructor matching parameters " + Arrays.asList(
						                                                                                   parameterTypes)));
				short methodRef = classFileGenerator.constantPoolReference(constructor);
				codeGenerator.add(ByteCode.INVOKESPECIAL, methodRef); // call to parent constructor
			}
		} else {
			invokeSuperConstructor(classFileGenerator, constantPool, codeGenerator);
		}
	}

	private ClassProxy[] handleArgumentListAndGetSignature(Java8Parser.ArgumentListContext argumentListContext,
	                                                       CodeGenerator codeGenerator,
	                                                       ExpressionVisitor expressionVisitor) {
		// todo: this is duplicated from ExpressionVisitor. This isn't ideal
		List<ClassProxy> signature = new ArrayList<>();
		if (argumentListContext != null) {
			for (Java8Parser.ExpressionContext expression : argumentListContext.expression()) {
				expressionVisitor.visit(expression);
				ClassProxy clazz = codeGenerator.stack().peek();
				signature.add(clazz);
			}
		}
		return signature.toArray(new ClassProxy[signature.size()]);
	}

	private void invokeSuperConstructor(ClassFileGenerator classFileGenerator, ConstantPool constantPool, CodeGenerator codeGenerator) {
		// todo: only allow this if the super-class has a no-args constructor
		short noArgsConstructorIndex = constantPool.add(new NameAndTypeInfo(constantPool.utf8(MethodsGenerator.CONSTRUCTOR_NAME), constantPool.utf8("()V")));
		short methodRefToInit = constantPool.add(new MethodrefInfo(classFileGenerator.getSuperClassIndex(), noArgsConstructorIndex));

		codeGenerator.add(ByteCode.ALOAD_0); // load "this"
		codeGenerator.add(ByteCode.INVOKESPECIAL, methodRefToInit); // call to parent constructor
	}

	public void fieldInitialization(ClassFileGenerator classFileGenerator, CodeGenerator codeGenerator) {
		if (constructorBodyContext != null) {
			constructorBodyContext.accept(new ConstructorBodyVisitor(classFileGenerator, codeGenerator));
		}
		codeGenerator.add(ByteCode.RETURN);
		constructor.addAttribute(codeGenerator.generate());
	}

	public LocalVariableTableGenerator getLocalVariables() {
		return localVariables;
	}
}

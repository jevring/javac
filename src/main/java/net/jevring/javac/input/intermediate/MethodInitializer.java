/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.intermediate;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.methods.Method;
import net.jevring.javac.generators.methods.MethodSignature;
import net.jevring.javac.grammar.Java8Parser;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.input.visitors.ExpressionVisitor;
import net.jevring.javac.output.structure.ByteCode;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class MethodInitializer {
	private final Java8Parser.MethodBodyContext methodBodyContext;
	private final MethodSignature methodSignature;
	private final CodeGenerator codeGenerator;
	private final Method method;

	public MethodInitializer(Java8Parser.MethodBodyContext methodBodyContext, MethodSignature methodSignature, CodeGenerator codeGenerator, Method method) {
		this.methodBodyContext = methodBodyContext;
		this.methodSignature = methodSignature;
		this.codeGenerator = codeGenerator;
		this.method = method;
	}

	public void initialize(ClassFileGenerator classFileGenerator) {
		ClassProxy returnType = methodSignature.getReturnType();
		methodBodyContext.block().blockStatements().accept(new ExpressionVisitor(classFileGenerator, returnType, codeGenerator, method.getName()));
		if (returnType.equals(ClassProxy.VOID) && !codeGenerator.lastOpIsReturn() && !codeGenerator.lastOpIsThrow()) {
			// todo: if last op is throw BUT we're the target of a jump, we should still add a return
			// it doesn't actually HURT if we have an extra return, but we don't actually want it

			// todo: this should be done elsewhere, but for now let's just make sure the setters work
			// technically this should ONLY be done if the method itself doesn't end with a return-statement, so we should have a different check
			codeGenerator.add(ByteCode.RETURN);
		}
		method.addAttribute(codeGenerator.generate());
	}

}

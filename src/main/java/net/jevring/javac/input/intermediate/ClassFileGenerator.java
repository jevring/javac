/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.input.intermediate;

import lombok.Data;
import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.fields.Field;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.generators.methods.Method;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.Imports;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.input.proxies.FieldProxy;
import net.jevring.javac.input.proxies.MethodProxy;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.attributes.SourceFile;
import net.jevring.javac.output.structure.constantpool.*;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
@Data
public class ClassFileGenerator {
	private final List<ConstructorInitializer> constructorInitializers = new ArrayList<>();
	private final List<MethodInitializer> methodInitializers = new ArrayList<>();
	private final List<FieldInitializer> fieldInitializers = new ArrayList<>();
	private final List<AttributeInfo> attributes = new ArrayList<>();
	private final List<Short> interfaces = new ArrayList<>();
	private final MethodsGenerator methodsGenerator;
	private final FieldsGenerator fieldsGenerator;
	private final ConstantPool constantPool;
	private final Imports imports;

	private short thisClassIndex;
	private short superClassIndex;
	private ClassProxy superClass;
	private AccessFlags accessFlags;
	private String fullyQualifiedName;
	private String sourceFile;
	private String name;

	public ClassFileGenerator(ConstantPool constantPool, Imports imports) {
		this.methodsGenerator = new MethodsGenerator(constantPool, ClassProxy.forClass(this));
		this.fieldsGenerator = new FieldsGenerator(constantPool);
		this.constantPool = constantPool;
		this.imports = imports;
	}

	public String getFullyQualifiedNameWithSlashes() {
		return fullyQualifiedName.replace('.', '/');
	}

	public short constantPoolReference(Field field) {
		// this will create whatever doesn't exist in the constant pool, and then return it.
		// if everything exists, this is simply a lookup operation
		short classInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8(getFullyQualifiedNameWithSlashes())));
		short nameIndex = constantPool.utf8(field.getName());
		short descriptorIndex = constantPool.utf8(field.getDescriptor());
		short nameAndTypeIndex = constantPool.add(new NameAndTypeInfo(nameIndex, descriptorIndex));
		return constantPool.add(new FieldrefInfo(classInfoIndex, nameAndTypeIndex));
	}


	public short constantPoolReference(FieldProxy field) {
		// this will create whatever doesn't exist in the constant pool, and then return it.
		// if everything exists, this is simply a lookup operation
		short classInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8(field.getDeclaringClass().getCanonicalName().replace('.', '/'))));
		short nameIndex = constantPool.utf8(field.getName());
		short descriptorIndex = constantPool.utf8(Descriptor.of(field.getType()));
		short nameAndTypeIndex = constantPool.add(new NameAndTypeInfo(nameIndex, descriptorIndex));
		return constantPool.add(new FieldrefInfo(classInfoIndex, nameAndTypeIndex));
	}


	public short constantPoolReference(MethodProxy method) {
		// this will create whatever doesn't exist in the constant pool, and then return it.
		// if everything exists, this is simply a lookup operation

		ClassProxy[] parameterTypes = method.getParameterTypes();
		ClassProxy returnType = method.getReturnType();
		String name = method.getName();
		ClassProxy declaringClass = method.getDeclaringClass();

		// this will create whatever doesn't exist in the constant pool, and then return it.
		// if everything exists, this is simply a lookup operation
		short classInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8(declaringClass.getCanonicalName().replace('.', '/'))));
		short descriptorIndex = constantPool.utf8(createSignatureDescriptor(parameterTypes, returnType));
		short nameAndTypeIndex = constantPool.add(new NameAndTypeInfo(constantPool.utf8(name), descriptorIndex));
		if (method.getDeclaringClass().isInterface()) {
			return constantPool.add(new InterfaceMethodrefInfo(classInfoIndex, nameAndTypeIndex));
		} else {
			return constantPool.add(new MethodrefInfo(classInfoIndex, nameAndTypeIndex));
		}
	}

	public short constantPoolReference(ClassProxy clazz) {
		return constantPool.add(new ClassInfo(constantPool.utf8(clazz.getCanonicalName().replace('.', '/'))));
	}

	private String createSignatureDescriptor(ClassProxy[] parameterTypes, ClassProxy returnType) {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (ClassProxy parameterType : parameterTypes) {
			sb.append(Descriptor.of(parameterType));
		}
		sb.append(")");
		sb.append(Descriptor.of(returnType));
		return sb.toString();
	}


	public ClassFile generate() {
		if (methodsGenerator.missingConstructor()) {
			Method method = methodsGenerator.addMethod(new AccessFlags(Flag.ACC_PUBLIC),
			                                           MethodsGenerator.EMPTY_CONSTRUCTOR_SIGNATURE,
			                                           MethodsGenerator.CONSTRUCTOR_NAME);
			LocalVariableTableGenerator localVariables = new LocalVariableTableGenerator(constantPool);
			localVariables.addMethodVariable(ClassProxy.forClass(this), "this"); // first variable is always 'this'
			constructorInitializers.add(new ConstructorInitializer(null, localVariables, method));
		}
		for (ConstructorInitializer constructorInitializer : constructorInitializers) {
			CodeGenerator codeGenerator = new CodeGenerator(constantPool, constructorInitializer.getLocalVariables());
			constructorInitializer.callSuperConstructor(this, constantPool, codeGenerator);
			for (FieldInitializer fieldInitializer : fieldInitializers) {
				fieldInitializer.addToConstructor(codeGenerator, this);
			}
			constructorInitializer.fieldInitialization(this, codeGenerator);
		}
		for (MethodInitializer methodInitializer : methodInitializers) {
			methodInitializer.initialize(this);
		}

		attributes.add(new AttributeInfo(constantPool.utf8("SourceFile"), new SourceFile(constantPool.utf8(sourceFile))));
		return ClassFile.builder()
		                .accessFlags(accessFlags)
		                .version(ClassFile.Version.JAVA_8)
		                .methods(methodsGenerator.generate())
		                .fields(fieldsGenerator.generate())
		                .attributes(attributes)
		                .constantPool(constantPool.generate())
		                .interfaces(interfaces)
		                .thisClass(thisClassIndex)
		                .superClass(superClassIndex)
		                .build();
	}

	@Override
	public String toString() {
		return fullyQualifiedName;
	}
}

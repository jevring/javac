/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.methods;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.MethodInfo;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;

import java.util.*;

import static net.jevring.javac.helpers.Polymorphism.signatureIsPolymorphicallyCompatible;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class MethodsGenerator {
	public static final MethodSignature EMPTY_CONSTRUCTOR_SIGNATURE = new MethodSignature(new ClassProxy[0], ClassProxy.VOID);
	public static final String CONSTRUCTOR_NAME = "<init>";
	public static final String CLASS_INITIALIZER_NAME = "<clinit>";
	private final List<MethodInfo> methods = new ArrayList<>();
	private final Map<String, List<Method>> methodsByName = new HashMap<>();
	private final ConstantPool constantPool;
	private final ClassProxy declaringClass;

	private boolean constructorAdded = false;

	public MethodsGenerator(ConstantPool constantPool, ClassProxy declaringClass) {
		this.constantPool = constantPool;
		this.declaringClass = declaringClass;
	}

	public Optional<Method> getMethod(String name, ClassProxy... parameterTypes) {
		List<Method> methodsBySignature = methodsByName.get(name);
		if (methodsBySignature != null) {
			for (Method method : methodsBySignature) {
				if (signatureIsPolymorphicallyCompatible(method.getMethodSignature().getParameterTypes(), parameterTypes)) {
					return Optional.of(method);
				}
			}
		}
		return Optional.empty();
	}


	public Method addMethod(AccessFlags accessFlags, MethodSignature methodSignature, String name) {
		List<AttributeInfo> attributes = addMethod0(accessFlags, Descriptor.of(methodSignature), name);
		Method method = new Method(attributes, methodSignature, declaringClass, accessFlags, name);
		methodsByName.computeIfAbsent(name, key -> new ArrayList<>()).add(method);
		return method;
	}

	@Deprecated
	public PendingMethodAttributes addMethod(AccessFlags accessFlags, String descriptor, String name) {
		return new PendingMethodAttributes(addMethod0(accessFlags, descriptor, name));
	}

	private List<AttributeInfo> addMethod0(AccessFlags accessFlags, String descriptor, String name) {
		short nameIndex = constantPool.utf8(name);
		short descriptorIndex = constantPool.utf8(descriptor);
		List<AttributeInfo> attributes = new ArrayList<>();
		methods.add(MethodInfo.builder().accessFlags(accessFlags).attributes(attributes).nameIndex(nameIndex).descriptorIndex(descriptorIndex).build());
		if (CONSTRUCTOR_NAME.equals(name)) {
			constructorAdded = true;
		}
		return attributes;
	}

	public List<MethodInfo> generate() {
		// todo: try to sort these so that the constructors come first
		return methods;
	}

	public boolean missingConstructor() {
		return !constructorAdded;
	}

	public void addPublicEmptyConstructor(String superclassName, ClassProxy classType) {
		AttributeInfo emptyConstructor = CodeGenerator.emptyConstructor(constantPool, superclassName, classType);
		addMethod(new AccessFlags(Flag.ACC_PUBLIC), EMPTY_CONSTRUCTOR_SIGNATURE, CONSTRUCTOR_NAME).addAttribute(emptyConstructor);
	}
}

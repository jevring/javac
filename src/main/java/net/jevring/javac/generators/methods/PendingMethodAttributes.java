/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.methods;

import lombok.Value;
import net.jevring.javac.output.structure.AttributeInfo;

import java.util.List;

/**
 * Used to add method attributes, such as {@link net.jevring.javac.output.structure.attributes.Code},
 * after the method has been added to the class, so that it can be referenced from inside code.
 *
 * @author markus@jevring.net
 */
@Value
public class PendingMethodAttributes {
	// todo: do we need anything more in here?
	private List<AttributeInfo> attributeInfo;

	public PendingMethodAttributes addAttribute(AttributeInfo attributeInfo) {
		this.attributeInfo.add(attributeInfo);
		return this;
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.variables;

import net.jevring.javac.generators.methods.MethodSignature;
import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.attributes.LocalVariableTable;
import net.jevring.javac.output.structure.attributes.LocalVariableTableEntry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Generates a local variable table attribute.
 *
 * @author markus@jevring.net
 */
public class LocalVariableTableGenerator {
	public static final String HIDDEN = "<hidden>";
	private final Map<String, LocalVariable> variables = new LinkedHashMap<>();
	private final ConstantPool constantPool;

	private int nextSlot = 0;
	private int depth = 0;
	private int currentBlock = 0;

	public LocalVariableTableGenerator(ConstantPool constantPool) {
		this.constantPool = constantPool;
	}

	public void enterBlock() {
		currentBlock++;
		depth++;
	}

	public void exitBlock(short byteCodeArrayIndex) {
		for (LocalVariable localVariable : variables.values()) {
			for (LocalVariableScope localVariableScope : localVariable.getScopes()) {
				if (localVariableScope.getDeclarationDepth() == depth && localVariableScope.getLength() == 0) {
					localVariableScope.endScope(byteCodeArrayIndex);
				}
			}
		}
		depth--;
	}

	public int getDepth() {
		return depth;
	}

	int getCurrentBlock() {
		return currentBlock;
	}

	public LocalVariable getLocalVariable(String name) {
		return variables.get(name);
	}

	public LocalVariable addMethodVariable(ClassProxy type, String name) {
		LocalVariable localVariable = addVariable(type, name, true);
		localVariable.startScope((short) 0);
		return localVariable;
	}

	public LocalVariable addLocalVariable(ClassProxy type, String name) {
		return addVariable(type, name, false);
	}

	private LocalVariable addVariable(ClassProxy type, String name, boolean methodVariable) {
		// the slot is *always* the same if the name is the same. the table can have multiple entries for the same 
		// variable, with different scopes, but the name-slot connection will always remain
		LocalVariable existingVariable = variables.get(name);
		if (!HIDDEN.equals(name)) {
			if (existingVariable != null) {
				if (!existingVariable.getType().equals(type)) {
					// todo: this has to take scope into account! a lot of exceptions will be called 'e'
					throw new IllegalArgumentException(String.format("Variable %s has multiple defined types; %s and %s",
					                                                 name,
					                                                 existingVariable.getType(),
					                                                 type));
				}
				return existingVariable;
			}
		}
		byte slot = (byte) nextSlot++;
		LocalVariable localVariable = new LocalVariable(this, methodVariable, type, name, slot);
		variables.put(name, localVariable);
		if (type.equals(ClassProxy.DOUBLE) || type.equals(ClassProxy.LONG)) {
			// doubles and longs take up two slots
			nextSlot++;
		}
		return localVariable;
	}

	/**
	 * Generates a method signature for the method of this local variable table.
	 * This method assumes that the variables have been added in they order
	 * they appear in the signature.
	 */
	public MethodSignature generateMethodSignature(ClassProxy returnType) {
		List<ClassProxy> parameterTypes = new ArrayList<>();
		for (LocalVariable localVariable : variables.values()) {
			if (localVariable.isMethodVariable()) {
				if (!"this".equals(localVariable.getName())) {
					parameterTypes.add(localVariable.getType());
				}
			}
		}
		return new MethodSignature(parameterTypes.toArray(new ClassProxy[parameterTypes.size()]), returnType);

	}

	public AttributeInfo generate() {
		List<LocalVariableTableEntry> table = new ArrayList<>();

		for (LocalVariable localVariable : variables.values()) {
			if (HIDDEN.equals(localVariable.getName())) {
				continue;
			}
			short nameIndex = constantPool.utf8(localVariable.getName());
			short descriptorIndex = constantPool.utf8(Descriptor.of(localVariable.getType()));
			for (LocalVariableScope localVariableScope : localVariable.getScopes()) {
				table.add(new LocalVariableTableEntry(localVariableScope.getStartPc(),
				                                      localVariableScope.getLength(),
				                                      nameIndex,
				                                      descriptorIndex,
				                                      localVariable.getSlot()));
			}
		}
		long sizeOfAllTableEntriesInBytes = 10 * table.size();
		int localVariableTableAttributeLength = (int) (2 + sizeOfAllTableEntriesInBytes);

		return new AttributeInfo(constantPool.utf8("LocalVariableTable"), new LocalVariableTable(localVariableTableAttributeLength, table));
	}

	public short getMaxLocals() {
		return (short) nextSlot;
	}

	public void endScopeForRemainingVariables(short indexBeyondEndOfByteCodes) {
		for (LocalVariable localVariable : variables.values()) {
			if (localVariable.getScopes().isEmpty()) {
				throw new IllegalArgumentException(String.format("No scope started for variable %s of type %s",
				                                                 localVariable.getName(),
				                                                 localVariable.getType()));
				//localVariable.startScope((short) 0);
			}
			for (LocalVariableScope localVariableScope : localVariable.getScopes()) {
				if (localVariableScope.getLength() == 0) {
					localVariableScope.endScope(indexBeyondEndOfByteCodes);
				}
			}
		}
	}

	public boolean isEmpty() {
		return variables.isEmpty();
	}
}

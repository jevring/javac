/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.variables;

import lombok.Value;
import net.jevring.javac.input.proxies.ClassProxy;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
@Value
public class LocalVariable {
	private final List<LocalVariableScope> scopes = new ArrayList<>();
	private final LocalVariableTableGenerator localVariableTableGenerator;
	private final boolean methodVariable;
	private final ClassProxy type;
	private final String name;
	private final byte slot;

	/**
	 * Starts a scope for the current variable, unless that variable already exists in a larger scope
	 * or previously in the same block.
	 */
	public void startScope(short byteCodeArrayIndex) {
		int declarationDepth = localVariableTableGenerator.getDepth();
		startScope(byteCodeArrayIndex, declarationDepth);
	}

	public void startScope(short byteCodeArrayIndex, int declarationDepth) {
		int currentBlock = localVariableTableGenerator.getCurrentBlock();
		for (LocalVariableScope scope : scopes) {
			if (scope.getDeclarationDepth() < declarationDepth) {
				// it was already declared in a wider scope, never mind
				System.out.printf("Variable %s was already declared at or above depth %d, Skipping...%n", name, declarationDepth);
				return;
			}
			if (scope.getDeclarationBlock() == currentBlock) {
				if (scope.getStartPc() < byteCodeArrayIndex) {
					// it was already declared in this block, never mind
					System.out.printf("Variable %s was already declared in block %d, Skipping...%n", name, currentBlock);
					return;
				}
			}

		}
		LocalVariableScope scope = new LocalVariableScope(declarationDepth, currentBlock, byteCodeArrayIndex);
		scopes.add(scope);
	}
}

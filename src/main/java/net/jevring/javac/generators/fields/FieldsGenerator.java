/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.fields;

import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.FieldInfo;
import net.jevring.javac.output.structure.accessflags.AccessFlags;

import java.util.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class FieldsGenerator {
	private final List<FieldInfo> fields = new ArrayList<>();
	private final Map<String, Field> fieldsByName = new HashMap<>();
	private final ConstantPool constantPool;

	public FieldsGenerator(ConstantPool constantPool) {
		this.constantPool = constantPool;
	}

	public Field addField(AccessFlags accessFlags, ClassProxy type, String name) {
		return addField(accessFlags, Descriptor.of(type), name, new ArrayList<>());
	}

	public Field addField(AccessFlags accessFlags, String descriptor, String name) {
		return addField(accessFlags, descriptor, name, new ArrayList<>());
	}

	public Field addField(AccessFlags accessFlags, String descriptor, String name, List<AttributeInfo> attributes) {
		short nameIndex = constantPool.utf8(name);
		short descriptorIndex = constantPool.utf8(descriptor);
		FieldInfo fieldInfo = FieldInfo.builder().accessFlags(accessFlags).attributes(attributes).nameIndex(nameIndex).descriptorIndex(descriptorIndex).build();
		fields.add(fieldInfo);
		Field field = new Field(accessFlags, descriptor, name);
		fieldsByName.put(name, field);
		return field;
	}

	public Optional<Field> getField(String name) {
		return Optional.ofNullable(fieldsByName.get(name));
	}

	public List<FieldInfo> generate() {
		return fields;
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators;

import net.jevring.javac.output.Writer;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.attributes.StackMapTable;
import net.jevring.javac.output.structure.attributes.stackmapframe.StackMapFrame;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class StackMapTableGenerator {
	private final List<StackMapFrame> entries = new ArrayList<>();
	private final ConstantPool constantPool;
	private short currentOffset = 0;

	public StackMapTableGenerator(ConstantPool constantPool) {
		this.constantPool = constantPool;
	}

	public void add(StackMapFrame frame) {
		currentOffset += frame.getOffsetDelta();
		entries.add(frame);
	}

	public short getCurrentOffset() {
		return currentOffset;
	}

	private byte[] generateByteArray(Writer w) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			w.write(dos);
			dos.flush();
		} catch (IOException e) {
			throw new AssertionError("ByteArrayOutputStream can't throw IOException", e);
		}
		return baos.toByteArray();
	}

	public AttributeInfo generate() {
		byte[] attributeBytes = generateByteArray(new Writer() {
			@Override
			public void write(DataOutputStream dos) throws IOException {
				dos.writeShort(entries.size());
				for (StackMapFrame entry : entries) {
					entry.write(dos);
				}
			}
		});


		return new AttributeInfo(constantPool.utf8("StackMapTable"), new StackMapTable(attributeBytes));
	}

	public boolean isEmpty() {
		return entries.isEmpty();
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.generators.code;

import lombok.Value;
import net.jevring.javac.generators.LineNumberTableGenerator;
import net.jevring.javac.generators.StackMapTableGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.input.TrackingStack;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.Writer;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.attributes.Code;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import net.jevring.javac.output.structure.constantpool.MethodrefInfo;
import net.jevring.javac.output.structure.constantpool.NameAndTypeInfo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates {@link Code} attributes for method bodies.
 *
 * @author markus@jevring.net
 */
public class CodeGenerator {
	private final List<Code.Exception> exceptionTable = new ArrayList<>();
	private final List<AttributeInfo> attributes = new ArrayList<>();
	private final List<Op> ops = new ArrayList<>();
	// todo: carry the same stack in a method
	private final LocalVariableTableGenerator localVariables;
	private final LineNumberTableGenerator lineNumbers;
	private final StackMapTableGenerator stackMapTable;
	private final ConstantPool constantPool;
	private final TrackingStack stack;

	private short currentByteCodeArrayIndex = 0;
	private short lastBranchInstructionIndex = 0;

	public CodeGenerator(ConstantPool constantPool) {
		this(constantPool, new LocalVariableTableGenerator(constantPool));
	}

	public CodeGenerator(ConstantPool constantPool, LocalVariableTableGenerator localVariables) {
		this(localVariables, new LineNumberTableGenerator(constantPool), new StackMapTableGenerator(constantPool), constantPool, new TrackingStack());
	}

	public CodeGenerator(LocalVariableTableGenerator localVariables,
	                     LineNumberTableGenerator lineNumbers,
	                     StackMapTableGenerator stackMapTable,
	                     ConstantPool constantPool,
	                     TrackingStack stack) {
		this.localVariables = localVariables;
		this.lineNumbers = lineNumbers;
		this.stackMapTable = stackMapTable;
		this.constantPool = constantPool;
		this.stack = stack;
	}

	public LocalVariableTableGenerator variables() {
		return localVariables;
	}

	public TrackingStack stack() {
		return stack;
	}

	public StackMapTableGenerator stackMapTable() {
		return stackMapTable;
	}

	public LineNumberTableGenerator lineNumbers() {
		return lineNumbers;
	}

	public short add(ByteCode byteCode) {
		return addOp(new OnlyByteCode(byteCode));
	}

	public short add(ByteCode byteCode, short operand) {
		return addOp(new SingleOperandAsShort(byteCode, operand));
	}

	public short add(ByteCode byteCode, ShortJumpOffset operand) {
		return addOp(new SinglePendingOperandAsShort(byteCode, operand));
	}

	public short add(ByteCode byteCode, byte operand) {
		return addOp(new SingleOperandAsByte(byteCode, operand));
	}

	public short add(ByteCode byteCode, byte operand1, byte operand2) {
		return addOp(new ByteByteOperands(byteCode, operand1, operand2));
	}

	public void add(ByteCode byteCode, short operand1, byte operand2, byte operand3) {
		addOp(new ShortByteByteOperands(byteCode, operand1, operand2, operand3));
	}

	public void add(AttributeInfo attributeInfo) {
		this.attributes.add(attributeInfo);
	}

	public void add(Code.Exception exceptionTableEntry) {
		this.exceptionTable.add(exceptionTableEntry);
	}

	public AttributeInfo generate() {
		localVariables.endScopeForRemainingVariables(getNextByteCodeArrayIndex());
		if (!lineNumbers.isEmpty()) {
			add(lineNumbers.generate());
		}
		if (!localVariables.isEmpty()) {
			add(localVariables.generate());
		}
		if (!stackMapTable.isEmpty()) {
			add(stackMapTable.generate());
		}

		short maxLocals = localVariables.getMaxLocals();
		short maxStack = (short) stack.getMaxHeight();

		// doubles and longs require 2 locals: https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-2.html#jvms-2.6.1

		byte[] codeBytes = generateBytecodeBytes();
		byte[] attributeBytes = generateAttributesBytes();
		byte[] exceptionTableBytes = generateExceptionTableArray();

		int attributeLength = 2 + 2 + 4 + codeBytes.length + exceptionTableBytes.length + attributeBytes.length;

		return new AttributeInfo(constantPool.utf8("Code"), new Code(attributeLength, maxStack, maxLocals, codeBytes, exceptionTableBytes, attributeBytes));
	}

	public short getNextByteCodeArrayIndex() {
		return currentByteCodeArrayIndex;
	}

	public short getLastBranchInstructionIndex() {
		return lastBranchInstructionIndex;
	}

	public void resetLastBranchInstructionIndex() {
		lastBranchInstructionIndex = 0;
	}

	private short addOp(Op op) {
		short current = currentByteCodeArrayIndex;
		ops.add(op);
		currentByteCodeArrayIndex += op.getRequiredBytes();
		if (op.getByteCode().isBranchInstruction()) {
			lastBranchInstructionIndex = current;
		}
		return current;
	}

	private byte[] generateBytecodeBytes() {
		return generateByteArray(new Writer() {
			@Override
			public void write(DataOutputStream dos) throws IOException {
				for (Op op : ops) {
					op.write(dos);
				}
			}
		});
	}

	private byte[] generateAttributesBytes() {
		return generateByteArray(new Writer() {
			@Override
			public void write(DataOutputStream dos) throws IOException {
				dos.writeShort(attributes.size());
				for (AttributeInfo attribute : attributes) {
					attribute.write(dos);
				}
			}
		});
	}

	private byte[] generateExceptionTableArray() {
		return generateByteArray(new Writer() {
			@Override
			public void write(DataOutputStream dos) throws IOException {
				dos.writeShort(exceptionTable.size());
				for (Code.Exception exception : exceptionTable) {
					exception.write(dos);
				}
			}
		});
	}

	private byte[] generateByteArray(Writer w) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			w.write(dos);
			dos.flush();
		} catch (IOException e) {
			throw new AssertionError("ByteArrayOutputStream can't throw IOException", e);
		}
		return baos.toByteArray();
	}

	public static AttributeInfo emptyConstructor(ConstantPool constantPool, String superclassName, ClassProxy classType) {
		short javaLangObjectClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8(superclassName)));
		short javaLangObjectConstructorIndex =
				constantPool.add(new NameAndTypeInfo(constantPool.utf8(MethodsGenerator.CONSTRUCTOR_NAME), constantPool.utf8("()V")));
		short methodRefToInit = constantPool.add(new MethodrefInfo(javaLangObjectClassInfoIndex, javaLangObjectConstructorIndex));

		CodeGenerator codeGenerator = new CodeGenerator(constantPool);
		codeGenerator.variables().addMethodVariable(classType, "this");

		codeGenerator.add(ByteCode.ALOAD_0); // load "this"
		codeGenerator.add(ByteCode.INVOKESPECIAL, methodRefToInit); // call to parent constructor
		codeGenerator.add(ByteCode.RETURN);

		return codeGenerator.generate();
	}

	public boolean hasOps() {
		return !ops.isEmpty();
	}

	public boolean lastOpIsReturn() {
		ByteCode byteCode = ops.get(ops.size() - 1).getByteCode();
		return byteCode == ByteCode.IRETURN || byteCode == ByteCode.ARETURN || byteCode == ByteCode.RETURN || byteCode == ByteCode.DRETURN || byteCode == ByteCode.FRETURN || byteCode == ByteCode.LRETURN;
	}

	public boolean lastOpIsThrow() {
		ByteCode byteCode = ops.get(ops.size() - 1).getByteCode();
		return byteCode == ByteCode.ATHROW;
	}

	private interface Op extends Writer {
		ByteCode getByteCode();

		byte getRequiredBytes();
	}

	@Value
	private static final class OnlyByteCode implements Op {
		private final ByteCode byteCode;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
		}

		@Override
		public byte getRequiredBytes() {
			return 1;
		}
	}

	@Value
	private static final class SingleOperandAsByte implements Op {
		private final ByteCode byteCode;
		private final byte operand;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
			dos.writeByte(operand);
		}

		@Override
		public byte getRequiredBytes() {
			return 2;
		}
	}

	@Value
	private static final class SingleOperandAsShort implements Op {
		private final ByteCode byteCode;
		private final short operand;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
			dos.writeShort(operand);
		}

		@Override
		public byte getRequiredBytes() {
			return 3;
		}
	}

	@Value
	private static final class ShortByteByteOperands implements Op {
		private final ByteCode byteCode;
		private final short operand1;
		private final byte operand2;
		private final byte operand3;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
			dos.writeShort(operand1);
			dos.writeByte(operand2);
			dos.writeByte(operand3);
		}

		@Override
		public byte getRequiredBytes() {
			return 5;
		}
	}

	@Value
	private static final class SinglePendingOperandAsShort implements Op {
		private final ByteCode byteCode;
		private final ShortJumpOffset operand;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
			dos.writeShort(operand.getValue());
		}

		@Override
		public byte getRequiredBytes() {
			return 3;
		}
	}

	@Value
	private static final class ByteByteOperands implements Op {
		private final ByteCode byteCode;
		private final byte operand1;
		private final byte operand2;

		@Override
		public void write(DataOutputStream dos) throws IOException {
			dos.writeByte(byteCode.getOpcode());
			dos.writeByte(operand1);
			dos.writeByte(operand2);
		}

		@Override
		public byte getRequiredBytes() {
			return 3;
		}
	}
}

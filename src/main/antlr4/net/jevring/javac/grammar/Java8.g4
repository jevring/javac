/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// https://docs.oracle.com/javase/specs/jls/se8/html/jls-19.html
// @author Markus Jevring <markus@jevring.net>
grammar Java8;

WhiteSpace              : [ \t\f\r\n]+ -> skip
						;
BlockComment            : '/*' .*? '*/' -> skip
						;
LineComment             : '//' ~[\r\n]* -> skip
						;						
// Lexer: https://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html
OpenParenthesis         : '(';
CloseParenthesis        : ')';
OpenBracket             : '[';
CloseBracket            : ']';
Slash                   : '/';
Star                    : '*';
Dot                     : '.';
SemiColon               : ';';
Ampersand               : '&';
Comma                   : ',';
OpenGullwing            : '{';
CloseGullwing           : '}';
LessThan                : '<';
GreaterThan             : '>';
At                      : '@';
Equals                  : '=';
Colon                   : ':';
Plus                    : '+';
Minus                   : '-';
Questionmark            : '?';
Pipe                    : '|';
Xor                     : '^';
Percent                 : '%';
Tilde                   : '~';
Not                     : '!';
PlusPlus                : '++';
MinusMinus              : '--';
DoubleColon             : '::';
And                     : '&&';
Or                      : '||';
Diamond                 : '<>';
Arrow                   : '->';
ReferenceEquals         : '==';
NotEquals               : '!=';
LessThanOrEqualTo       : '<=';
GreaterThanOrEqualTo    : '>=';
LeftShift               : '<<';
Elipsis                 : '...';
Synchronized            : 'synchronized';
Implements              : 'implements';
Instanceof              : 'instanceof';
Interface               : 'interface';
Protected               : 'protected';
Transient               : 'transient';
Volatile                : 'volatile';
Abstract                : 'abstract';
Strictfp                : 'strictfp';
Continue                : 'continue';
Finally                 : 'finally';
Boolean                 : 'boolean';
Package                 : 'package';
Extends                 : 'extends';
Default                 : 'default';
Private                 : 'private';
Native                  : 'native';
Import                  : 'import';
Static                  : 'static';
Switch                  : 'switch';
Assert                  : 'assert';
Public                  : 'public';
Throws                  : 'throws';
Return                  : 'return';
While                   : 'while';
Catch                   : 'catch';
Final                   : 'final';
Class                   : 'class';
Super                   : 'super';
Break                   : 'break';
Throw                   : 'throw';
Void                    : 'void';
Case                    : 'case';
This                    : 'this';
Enum                    : 'enum';
Else                    : 'else';
New                     : 'new';
For                     : 'for';
Try                     : 'try';
If                      : 'if';
Do                      : 'do';
fragment
UnicodeInputCharacter   : UnicodeEscape
						| RawInputCharacter
						;
fragment
UnicodeEscape           : '\\' UnicodeMarker HexDigit HexDigit HexDigit HexDigit
						;
fragment
UnicodeMarker           : 'u' 'u'*
						;
						// todo: surely there must be some better way of saying "all unicode characters"
//RawInputCharacter       : [\u0080-\ufffe]
fragment
RawInputCharacter       : [a-zA-Z_$]
						;
fragment
LineTerminator          : '\r'
						| '\n'
						| '\r\n'
						;
fragment
InputCharacter          : UnicodeInputCharacter
						;
fragment
Sub                     : '\u001A'
						;
// Productions from §3 (Lexical Structure) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html
NullLiteral             : 'null'
						;
BooleanLiteral          : 'true'
						| 'false'
						;
assignmentOperator      : Equals
                        | '*='
                        | '/=' 
                        | '%='  
                        | '+='  
                        | '-='  
                        | '<<='  
                        | '>>='
                        | '>>>='
                        | '&='
                        | '^='
                        | '|='
                        ;
fragment
JavaLetter              : UnicodeInputCharacter
						;
fragment
JavaLetterOrDigit   	: JavaLetter
						| Digit
						;
Identifier              : JavaLetter JavaLetterOrDigit* 
						;
literal                 : IntegerLiteral        #integerLiteral
						| FloatingPointLiteral  #floatingPointLiteral
						| BooleanLiteral        #booleanLiteral
						| CharacterLiteral      #characterLiteral
						| StringLiteral         #stringLiteral
						| NullLiteral           #nullLiteral
						;
IntegerLiteral          : DecimalIntegerLiteral
						| HexIntegerLiteral
						| OctalIntegerLiteral
						| BinaryIntegerLiteral
						;
fragment						
DecimalIntegerLiteral   : DecimalNumeral IntegerTypeSuffix?
						;
fragment
HexIntegerLiteral       : HexNumeral IntegerTypeSuffix?
						;
fragment
OctalIntegerLiteral     : OctalNumeral IntegerTypeSuffix?
						;
fragment
BinaryIntegerLiteral    : BinaryNumeral IntegerTypeSuffix?
						;
fragment
IntegerTypeSuffix       : 'l' 
						| 'L'
						;
fragment
DecimalNumeral          : '0'
						| NonZeroDigit Digits?
						| NonZeroDigit Underscores Digits
						;
fragment
NonZeroDigit            : [1-9]
						;
fragment
Digits                  : Digit
						| Digit DigitsAndUnderscores? Digit
						;
fragment
Digit                   : '0'
						| NonZeroDigit
						;
fragment
DigitsAndUnderscores    : DigitOrUnderscore DigitOrUnderscore*
						;
fragment
DigitOrUnderscore       : Digit
						| '_'
						;
fragment
Underscores             : '_' '_'*
						;
fragment
HexNumeral              : '0' 'x' HexDigits
						| '0' 'X' HexDigits
						;
fragment
HexDigits               : HexDigit
						| HexDigit HexDigitsAndUnderscores? HexDigit
						;
fragment
HexDigit                : [0-9a-fA-F]
						;
fragment
HexDigitsAndUnderscores : HexDigitOrUnderscore HexDigitOrUnderscore*
						;
fragment
HexDigitOrUnderscore    : HexDigit
						| '_'
						;
fragment
OctalNumeral            : '0' OctalDigits
						| '0' Underscores OctalDigits
						;
fragment
OctalDigits             : OctalDigit
						| OctalDigit OctalDigitsAndUnderscores? OctalDigit
						;
fragment
OctalDigit              : [0-7]
						;
fragment
OctalDigitsAndUnderscores   : OctalDigitOrUnderscore OctalDigitOrUnderscore*
							;
fragment
OctalDigitOrUnderscore  : OctalDigit
						| '_'
						;
fragment
BinaryNumeral           : '0' 'b' BinaryDigits
						| '0' 'B' BinaryDigits
						;
fragment
BinaryDigits            : BinaryDigit
						| BinaryDigit BinaryDigitsAndUnderscores? BinaryDigit
						;
fragment
BinaryDigit             : [0-1]
						;
fragment
BinaryDigitsAndUnderscores  : BinaryDigitOrUnderscore BinaryDigitOrUnderscore*
							;
fragment
BinaryDigitOrUnderscore : BinaryDigit
						| '_'
						;
						
FloatingPointLiteral    : DecimalFloatingPointLiteral
						| HexadecimalFloatingPointLiteral
						;
fragment
DecimalFloatingPointLiteral : Digits Dot Digits? ExponentPart? FloatTypeSuffix?
							| Dot Digits ExponentPart? FloatTypeSuffix?
							| Digits ExponentPart FloatTypeSuffix?
							| Digits ExponentPart? FloatTypeSuffix
							;
fragment							
ExponentPart            : ExponentIndicator SignedInteger
						;
fragment
ExponentIndicator       : 'e'
						| 'E'
						;
fragment						
SignedInteger           : Sign? Digits
						;
fragment
Sign                    : Plus
						| Minus
						;
fragment
FloatTypeSuffix         : 'f'
						| 'F'
						| 'd'
						| 'D'
						;
fragment
HexadecimalFloatingPointLiteral : HexSignificand BinaryExponent FloatTypeSuffix?
								;
fragment
HexSignificand          : HexNumeral Dot?
						| '0' 'x' HexDigits? . HexDigits
						| '0' 'X' HexDigits? . HexDigits
						;
fragment
BinaryExponent          : BinaryExponentIndicator SignedInteger
						;
fragment
BinaryExponentIndicator : 'p'
						| 'P'
						;
CharacterLiteral        : '\'' SingleCharacter '\''
						| '\'' EscapeSequence '\''
						;
fragment
SingleCharacter         : ~['\\\r\n] 
						;
StringLiteral           : '"' StringCharacter* '"'
						;
fragment
StringCharacter         : ~["\\\r\n] 
						| EscapeSequence
						;
fragment
EscapeSequence          : '\\' 'b' 
						| '\\' 't'
						| '\\' 'n'
						| '\\' 'f'
						| '\\' 'r'
						| '\\' '"'
						| '\\' '\''
						| '\\' '\\'
						| OctalEscape
						;
fragment
OctalEscape             : '\\' OctalDigit
						| '\\' OctalDigit OctalDigit
						| '\\' ZeroToThree OctalDigit OctalDigit
						;
fragment
ZeroToThree             : [0-3]
                        ;

// Productions from §4 (Types, Values, and Variables) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-4.html
primitiveType           : annotation* BooleanLiteral
						| annotation* numericType
						;
numericType             : integralType
						| floatingPointType
						;
integralType            : 'byte'    #primitiveByte
						| 'short'   #primitiveShort
						| 'int'     #primitiveInt
						| 'long'    #primitiveLong
						| 'char'    #primitiveChar
						;
floatingPointType       : 'float'   #primitiveFloat
						| 'double'  #primitiveDouble
						;
referenceType           : classOrInterfaceType
						| typeVariable
						| arrayType
						;
classOrInterfaceType    : annotation* Identifier typeArguments?
						| classOrInterfaceType Dot annotation* Identifier typeArguments?
						;
elementValuePairList    : elementValuePair (Comma elementValuePair)*
						;
elementValuePair        : key=Identifier Equals value=elementValue
						;
elementValue			: conditionalExpression
						| elementValueArrayInitializer
						| annotation
						;
elementValueArrayInitializer    : OpenGullwing elementValueList? Comma? CloseGullwing
 						        ;
elementValueList        : elementValue (Comma elementValue)*
 						;        
typeVariable            : annotation* Identifier
						;
arrayType               : primitiveType arrayDimensions
						| classOrInterfaceType arrayDimensions
						| typeVariable arrayDimensions
						;
arrayDimensions         : annotation* OpenBracket CloseBracket (annotation* OpenBracket CloseBracket)*
						;
typeParameter           : annotation* Identifier typeBound?
						;
typeBound               : Extends typeVariable
						| Extends classOrInterfaceType additionalBound*
						;
additionalBound         : Ampersand classOrInterfaceType
						;
typeArguments           : LessThan typeArgumentList GreaterThan
						;
typeArgumentList        : typeArgument (Comma typeArgument)*
						;
typeArgument            : referenceType
						| wildcard
						;
wildcard                : annotation* Questionmark wildcardBounds?
						;
wildcardBounds          : Extends referenceType
						| Super referenceType
						;

// Productions from §6 (Names) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-6.html
typeName                : Identifier
                        | typeName Dot Identifier
						;
expressionName          : Identifier 
						| expressionName Dot Identifier
						;
methodName              : Identifier
						;
packageName             : Identifier 
						| packageName Dot Identifier
						;

// Productions from §7 (Packages) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-7.html
compilationUnit         : packageDeclaration? importDeclaration* typeDeclaration* EOF
						;
packageDeclaration      : packageModifier* Package packageName SemiColon
						;
packageModifier         : annotation
						;
importDeclaration       : singleTypeImportDeclaration
						| typeImportOnDemandDeclaration
						| singleStaticImportDeclaration
						| staticImportOnDemandDeclaration
						;
singleTypeImportDeclaration     : Import typeName SemiColon
								;
typeImportOnDemandDeclaration   : Import typeName Dot Star SemiColon
								;
singleStaticImportDeclaration   : Import Static typeName Dot Identifier SemiColon
								;
staticImportOnDemandDeclaration : Import Static typeName Dot Star SemiColon
 								;
typeDeclaration         : classDeclaration
 						| interfaceDeclaration
 						;

// Productions from §8 (Classes) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-8.html
classDeclaration        : normalClassDeclaration
						| enumDeclaration
						;
normalClassDeclaration  : classModifier* Class Identifier typeParameters? superclass? superinterfaces? classBody
						;
classModifier           : annotation
						| Public
						| Protected
						| Private
						| Abstract
						| Static
						| Final
						| Strictfp
						;
typeParameters          : LessThan typeParameterList GreaterThan
						;
typeParameterList       : typeParameter (Comma typeParameter)*
						;
superclass              : Extends classOrInterfaceType
						;
superinterfaces         : Implements interfaceTypeList
						;
interfaceTypeList       : classOrInterfaceType (Comma classOrInterfaceType)*
						;
classBody               : OpenGullwing classBodyDeclaration* CloseGullwing
						;
classBodyDeclaration    : classMemberDeclaration
						| instanceInitializer
						| staticInitializer
						| constructorDeclaration
						;
classMemberDeclaration  : fieldDeclaration
						| methodDeclaration
						| classDeclaration
						| interfaceDeclaration
						;
fieldDeclaration        : fieldModifier* unannotatedType variableDeclaratorList SemiColon
						;
fieldModifier           : annotation
						| Public
						| Protected
						| Private
						| Static
						| Final
						| Transient
						| Volatile
						;
variableDeclaratorList  : variableDeclaration (Comma variableDeclaration)*
						;
variableDeclaration     : variableDeclaratorId (Equals variableInitializer)?
						;
variableDeclaratorId    : Identifier arrayDimensions?
						;
variableInitializer     : expression
						| arrayInitializer
						;
unannotatedType         : unannotatedPrimitiveType
						| unannotatedReferenceType
						;
unannotatedPrimitiveType: numericType   #primitiveNumeric
						| Boolean       #primitiveBoolean
						;
unannotatedReferenceType: unannotatedClassType
						| unannotatedTypeVariable
						| unannotatedArrayType
						;
unannotatedClassType    : Identifier typeArguments?
						| unannotatedClassType Dot annotation? Identifier typeArguments?
						;
unannotatedTypeVariable : Identifier
						;
unannotatedArrayType    : unannotatedPrimitiveType arrayDimensions
						| unannotatedClassType arrayDimensions
						| unannotatedTypeVariable arrayDimensions
						;
methodDeclaration       : methodModifier* methodHeader methodBody
						;
methodModifier          : annotation
						| Public
						| Protected
						| Private
						| Abstract
						| Static
						| Final
						| Synchronized
						| Native
						| Strictfp
						;
methodHeader            : result methodDeclarator xThrows?
						| typeParameters annotation* result methodDeclarator xThrows?
						;
result                  : unannotatedType
						| Void
						;
methodDeclarator        : Identifier OpenParenthesis formalParameterList? CloseParenthesis arrayDimensions?
						;
formalParameterList     : receiverParameter
						| formalParameters Comma lastFormalParameter
						| lastFormalParameter
						;
formalParameters        : formalParameter (Comma formalParameter)*
						| receiverParameter (Comma formalParameter)*
						;
formalParameter         : variableModifier* unannotatedType variableDeclaratorId
						;
variableModifier        : annotation
						| Final
						;
lastFormalParameter     : variableModifier* unannotatedType annotation* Elipsis variableDeclaratorId
						| formalParameter
						;
receiverParameter       : annotation* unannotatedType (Identifier Dot)? This
						;
// can't just use Throws as that's a reserved word						
xThrows                 : Throws exceptionTypeList
						;
exceptionTypeList       : exceptionType (Comma exceptionType)*
						;
exceptionType           : classOrInterfaceType
						| typeVariable
						;
methodBody              : block
						| SemiColon
						;
instanceInitializer     : block
						;
staticInitializer       : Static block
						;
constructorDeclaration  : constructorModifier* constructorDeclarator xThrows? constructorBody
						;
constructorModifier     : annotation
						| Public
						| Protected
						| Private
						;
constructorDeclarator   : typeParameters? simpleTypeName OpenParenthesis formalParameterList? CloseParenthesis
						;
simpleTypeName          : Identifier
						;
constructorBody         : OpenGullwing explicitConstructorInvocation? blockStatements? CloseGullwing
						;
explicitConstructorInvocation   : typeArguments? This OpenParenthesis argumentList? CloseParenthesis SemiColon
								| typeArguments? Super OpenParenthesis argumentList? CloseParenthesis SemiColon
								| expressionName Dot typeArguments? Super OpenParenthesis argumentList? CloseParenthesis SemiColon
								| primary Dot typeArguments? Super OpenParenthesis argumentList? CloseParenthesis SemiColon
								;
enumDeclaration         : classModifier* Enum Identifier superinterfaces? enumBody
						;
enumBody                : OpenGullwing enumConstantList? Comma? enumBodyDeclarations? CloseGullwing
						;
enumConstantList        : enumConstant (Comma enumConstant)*
						;
enumConstant            : enumConstantModifier* Identifier (OpenParenthesis argumentList? CloseParenthesis)? classBody?
						;
enumConstantModifier    : annotation
						;
enumBodyDeclarations    : SemiColon classBodyDeclaration*
						;
						
// Productions from §9 (Interfaces) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html
interfaceDeclaration    : normalInterfaceDeclaration
						| annotationTypeDeclaration
						;
normalInterfaceDeclaration      : interfaceModifier* Interface Identifier typeParameters? extendsInterfaces? interfaceBody
								;
interfaceModifier       : annotation
						| Public
						| Protected
						| Private
						| Abstract
						| Static
						| Strictfp
						;
extendsInterfaces       : Extends interfaceTypeList
						;
interfaceBody           : OpenGullwing interfaceMemberDeclaration* CloseGullwing
						;
interfaceMemberDeclaration      : constantDeclaration
								| interfaceMethodDeclaration
								| classDeclaration
								| interfaceDeclaration
								;
constantDeclaration     : constantModifier* unannotatedType variableDeclaratorList SemiColon
						;
constantModifier        : annotation
						| Public
						| Static
						| Final
						;
interfaceMethodDeclaration      : interfaceMethodModifier* methodHeader methodBody
								;
interfaceMethodModifier : annotation
						| Public
						| Abstract
						| Default
						| Static
						| Strictfp
						;
annotationTypeDeclaration       : interfaceModifier* At Interface Identifier annotationTypeBody
								;
annotationTypeBody              : OpenGullwing annotationTypeMemberDeclaration? CloseGullwing
								;
annotationTypeMemberDeclaration : annotationTypeElementDeclaration
								| constantDeclaration
								| classDeclaration
								| interfaceDeclaration
								;
annotationTypeElementDeclaration: annotationTypeElementModifier? unannotatedType Identifier OpenParenthesis CloseParenthesis arrayDimensions? defaultValue? SemiColon
                                ;
annotationTypeElementModifier   : annotation
								| Public
								| Abstract
								;
defaultValue            : Default elementValue
						;
annotation              : normalAnnotation
						| markerAnnotation
						| singleElementAnnotation
						;
normalAnnotation        : At typeName (OpenParenthesis elementValuePairList? CloseParenthesis)?
						;
markerAnnotation        : At typeName
						;
singleElementAnnotation : At typeName OpenParenthesis elementValue CloseParenthesis
						;

// Productions from §10 (Arrays) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-10.html
arrayInitializer        : OpenGullwing variableInitializerList? Comma? CloseGullwing
						;
variableInitializerList : variableInitializer (Comma variableInitializer)*
                        ;
// Productions from §14 (Blocks and Statements) -> https://docs.oracle.com/javase/specs/jls/se8/html/jls-14.html
block                   : OpenGullwing blockStatements? CloseGullwing
						;
blockStatements         : blockStatement blockStatement*
						;
blockStatement          : localVariableDeclarationStatement
						| classDeclaration
						| statement
						;
localVariableDeclarationStatement   : localVariableDeclaration SemiColon
									;
localVariableDeclaration            : variableModifier* unannotatedType variableDeclaratorList
									;
statement               : statementWithoutTrailingSubstatement
						| labeledStatement
						| ifThenStatement
						| ifThenElseStatement
						| whileStatement
						| forStatement
						;
statementNoShortIf      : statementWithoutTrailingSubstatement
						| labeledStatementNoShortIf
						| ifThenElseStatementNoShortIf
						| whileStatementNoShortIf
						| forStatementNoShortIf
						;
statementWithoutTrailingSubstatement    : block
										| emptyStatement
										| expressionStatement
										| assertStatement
										| switchStatement
										| doStatement
										| breakStatement
										| continueStatement
										| returnStatement
										| synchronizedStatement
										| throwStatement
										| tryStatement
										;
emptyStatement          : SemiColon
						;
labeledStatement        : Identifier Colon statement
						;
labeledStatementNoShortIf               : Identifier Colon statementNoShortIf
										;
expressionStatement     : statementExpression SemiColon
						;
statementExpression     : assignment
						| preIncrementExpression
						| preDecrementExpression
						| postIncrementExpression
						| postDecrementExpression
						| methodInvocation
						| pureClassInstanceCreationExpression
						;
ifThenStatement         : If OpenParenthesis expression CloseParenthesis statement
						;
ifThenElseStatement     : If OpenParenthesis expression CloseParenthesis statementNoShortIf Else statement
						;
ifThenElseStatementNoShortIf            : If OpenParenthesis expression CloseParenthesis ifPart=statementNoShortIf Else elsePart=statementNoShortIf
										;
assertStatement         : Assert expression SemiColon
						| Assert expression Colon expression SemiColon
						;
switchStatement         : Switch OpenParenthesis expression CloseParenthesis switchBlock
						;
switchBlock             : OpenGullwing switchBlockStatementGroup* switchLabel* CloseGullwing
						;
switchBlockStatementGroup   : switchLabels blockStatements
							;
switchLabels            : switchLabel switchLabel*
						;
switchLabel             : Case constantExpression Colon
						| Case enumConstantName Colon
						| Default Colon
						;
enumConstantName        : Identifier
						;
whileStatement          : While OpenParenthesis expression CloseParenthesis statement
						;
whileStatementNoShortIf : While OpenParenthesis expression CloseParenthesis statementNoShortIf
						;
doStatement             : Do statement While OpenParenthesis expression CloseParenthesis SemiColon
						;
forStatement            : basicForStatement
						| enhancedForStatement
						;
forStatementNoShortIf   : basicForStatementNoShortIf
						| enhancedForStatementNoShortIf
						;
basicForStatement       : For OpenParenthesis forInit? SemiColon expression? SemiColon forUpdate? CloseParenthesis statement
						;
basicForStatementNoShortIf      : For OpenParenthesis forInit? SemiColon expression? SemiColon forUpdate? CloseParenthesis statementNoShortIf
								;
forInit                 : statementExpressionList
						| localVariableDeclaration
						;
forUpdate               : statementExpressionList
						;
statementExpressionList : statementExpression (Comma statementExpression)*
						;
enhancedForStatement    : For OpenParenthesis variableModifier* unannotatedType variableDeclaratorId Colon expression CloseParenthesis statement
						;
enhancedForStatementNoShortIf   : For OpenParenthesis variableModifier* unannotatedType variableDeclaratorId Colon expression CloseParenthesis statementNoShortIf
								;
breakStatement          : Break Identifier? SemiColon
						;
continueStatement       : Continue Identifier? SemiColon
						;
returnStatement         : Return expression? SemiColon
						;
throwStatement          : Throw expression SemiColon
						;
synchronizedStatement   : Synchronized OpenParenthesis expression CloseParenthesis block
						;
tryStatement            : Try block catches
						| Try block catches? xFinally
						| tryWithResourcesStatement
						;
catches                 : catchClause catchClause*
						;
catchClause             : Catch OpenParenthesis catchFormalParameter CloseParenthesis block
						;
catchFormalParameter    : variableModifier* catchType variableDeclaratorId
						;
catchType               : unannotatedClassType (Pipe classOrInterfaceType)*
						;
xFinally                : Finally block
						;
tryWithResourcesStatement   : Try resourceSpecification block catches? xFinally?
							;
resourceSpecification   : OpenParenthesis resourceList SemiColon? CloseParenthesis
						;
resourceList            : resource (SemiColon resource)*
						;
resource                : variableModifier* unannotatedType variableDeclaratorId Equals expression
						;


// Productions from §15 (Expressions) -->
// we have to do a bunch of special hack here to deal with the left-first recursion.
primary                 : (purePrimaryNoNewArray | arrayCreationExpression) primaryNoNewArray*
						;
primaryNoNewArray       : classInstanceCreationExpression
                        | primaryFieldAccess
                        | primaryArrayAccess
                        | primaryMethodInvocation
                        | methodReference
						;
simplePrimaryNoNewArray : literal                                       #primaryLiteral
                        | classLiteral                                  #primaryClassLiteral
                        | This                                          #primaryThis
                        | typeName Dot This                             #primaryTypeDotThis
                        | OpenParenthesis expression CloseParenthesis   #primaryExpression
                        ;
purePrimaryNoNewArray   : simplePrimaryNoNewArray
						| pureClassInstanceCreationExpression
						| pureFieldAccess
						| pureArrayAccess
						| pureMethodInvocation
						| pureMethodReference
						;
classLiteral            : typeName (OpenBracket CloseBracket)* Dot Class
						| numericType (OpenBracket CloseBracket)* Dot Class
						| Boolean (OpenBracket CloseBracket)* Dot Class
						| Void Dot Class
						;
classInstanceCreationExpression : pureClassInstanceCreationExpression
								| primary Dot unqualifiedClassInstanceCreationExpression
								;
pureClassInstanceCreationExpression         : unqualifiedClassInstanceCreationExpression
                                            | expressionName Dot unqualifiedClassInstanceCreationExpression
                                            ;
unqualifiedClassInstanceCreationExpression  : New typeArguments? classOrInterfaceTypeToInstantiate OpenParenthesis argumentList? CloseParenthesis classBody?
											;
classOrInterfaceTypeToInstantiate           : annotation* Identifier (Dot annotation* Identifier)* typeArgumentsOrDiamond?
											;
typeArgumentsOrDiamond  : typeArguments
						| Diamond
						;
primaryFieldAccess      : Dot Identifier
						; 
fieldAccess             : primary Dot Identifier
						| pureFieldAccess
						;						
pureFieldAccess         : Super Dot Identifier
						| typeName Dot Super Dot Identifier
						;
// todo: write some tests that include array accesses, to see how much more we suck
primaryArrayAccess      : OpenBracket expression CloseBracket
						;						
arrayAccess             : purePrimaryNoNewArray OpenBracket expression CloseBracket 
						;
pureArrayAccess         : expressionName OpenBracket expression CloseBracket
						;
primaryMethodInvocation : dotMethodInvocation
						;						
methodInvocation        : primary dotMethodInvocation
						| pureMethodInvocation
						;
pureMethodInvocation    : methodName OpenParenthesis argumentList? CloseParenthesis #unclassifiedMethodInvocation
// todo: without this, it things that object.method() means "object.method" is a typeName, and then () is just plain wrong.
// hopefully we won't have to revisit this, but if we do, we'll have to do something about it
//						| typeName dotMethodInvocation 
						| expressionName dotMethodInvocation                        #expressionNameMethodInvocation // this one is automatically handled by expressionName and dotMethodInvocation already
						| Super dotMethodInvocation                                 #superMethodInvocation
						| typeName Dot Super dotMethodInvocation                    #typedSuperMethodInvocation
						;
dotMethodInvocation     : Dot typeArguments? Identifier OpenParenthesis argumentList? CloseParenthesis
						;
argumentList            : expression (Comma expression)*
						;
methodReference         : DoubleColon typeArguments? Identifier
						;
pureMethodReference     : expressionName DoubleColon typeArguments? Identifier
						| referenceType DoubleColon typeArguments? Identifier
						| Super DoubleColon typeArguments? Identifier
						| typeName Dot Super DoubleColon typeArguments? Identifier
						| classOrInterfaceType DoubleColon typeArguments? New
						| arrayType DoubleColon New
						;
arrayCreationExpression : New primitiveType dimExprs arrayDimensions?
						| New classOrInterfaceType dimExprs arrayDimensions?
						| New primitiveType arrayDimensions arrayInitializer
						| New classOrInterfaceType arrayDimensions arrayInitializer
						;
dimExprs                : dimExpr dimExpr*
						;
dimExpr                 : annotation* OpenBracket expression CloseBracket
						;
expression              : lambdaExpression
						| assignmentExpression
						;
lambdaExpression        : lambdaParameters Arrow lambdaBody
						;
lambdaParameters        : Identifier
						| OpenParenthesis formalParameterList? CloseParenthesis
						| OpenParenthesis inferredFormalParameterList CloseParenthesis
						;
inferredFormalParameterList     : Identifier (Comma Identifier)*
								;
lambdaBody              : expression
						| block
						;
assignmentExpression    : conditionalExpression
						| assignment
						;
assignment              : leftHandSide assignmentOperator expression
						;
leftHandSide            : expressionName
						| fieldAccess
						| arrayAccess
						;
conditionalExpression   : conditionalOrExpression                                                       #skip_ternary
						| conditionalOrExpression Questionmark expression Colon conditionalExpression   #ternary
						| conditionalOrExpression Questionmark expression Colon lambdaExpression        #ternaryLambda
						;
conditionalOrExpression : conditionalAndExpression
						| conditionalOrExpression Or conditionalAndExpression
						;
conditionalAndExpression: inclusiveOrExpression
						| conditionalAndExpression And inclusiveOrExpression
						;
inclusiveOrExpression   : exclusiveOrExpression
						| inclusiveOrExpression Pipe exclusiveOrExpression
						;
exclusiveOrExpression   : andExpression
						| exclusiveOrExpression Xor andExpression
						;
andExpression           : equalityExpression
						| andExpression Ampersand equalityExpression
						;
equalityExpression      : relationalExpression                                      #skip_relationalExpression
						| equalityExpression ReferenceEquals relationalExpression   #referenceEquals
						| equalityExpression NotEquals relationalExpression         #notEquals
						;
relationalExpression    : shiftExpression                                           #skip_shiftExpression
						| relationalExpression LessThan shiftExpression             #lessThan
						| relationalExpression GreaterThan shiftExpression          #greaterThan
						| relationalExpression LessThanOrEqualTo shiftExpression    #lessThanOrEqualTo
						| relationalExpression GreaterThanOrEqualTo shiftExpression #greaterThanOrEqualTo
						| relationalExpression Instanceof referenceType             #instanceOf
						;
shiftExpression         : additiveExpression
						| shiftExpression LeftShift additiveExpression
						| shiftExpression GreaterThan GreaterThan additiveExpression
						| shiftExpression GreaterThan GreaterThan GreaterThan additiveExpression
						;
additiveExpression      : multiplicativeExpression
						| additiveExpression Plus multiplicativeExpression
						| additiveExpression Minus multiplicativeExpression
						;
multiplicativeExpression: unaryExpression
						| multiplicativeExpression Star unaryExpression
						| multiplicativeExpression Slash unaryExpression
						| multiplicativeExpression Percent unaryExpression
						;
unaryExpression         : preIncrementExpression
						| preDecrementExpression
						| Plus unaryExpression
						| Minus unaryExpression
						| unaryExpressionNotPlusMinus
						;
preIncrementExpression  : PlusPlus unaryExpression
						;
preDecrementExpression  : MinusMinus unaryExpression
						;
unaryExpressionNotPlusMinus : postfixExpression
							| Tilde unaryExpression
							| Not unaryExpression
							| castExpression
							;
postfixExpression       : primary
						| expressionName
						| postfixExpression PlusPlus
						| postfixExpression MinusMinus
						;
postIncrementExpression : postfixExpression PlusPlus
						;
postDecrementExpression : postfixExpression MinusMinus
						;						
castExpression          : OpenParenthesis primitiveType CloseParenthesis unaryExpression
						| OpenParenthesis referenceType additionalBound* CloseParenthesis unaryExpressionNotPlusMinus
						| OpenParenthesis referenceType additionalBound* CloseParenthesis lambdaExpression
						;
constantExpression      : expression
						;

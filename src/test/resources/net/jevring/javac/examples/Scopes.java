/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

public class Scopes {

	public String declareOutsideBlock(boolean b) {
		String s1;
		if (b) {
			s1 = "true";
		} else {
			s1 = "false";
		}
		return s1;
	}

	public String declareOutsideBlockDoubleAssignment(boolean b) {
		String s2;
		if (b) {
			s2 = "nothing";
			s2 = "true";
		} else {
			s2 = "nothing";
			s2 = "false";
		}
		return s2;
	}

	public String declareAndAssignOutsideBlock(boolean b) {
		String s3 = "nothing so far";
		System.out.println("abc");
		if (b == true) {
			s3 = "true";
		} else {
			s3 = "false";
		}
		return s3;
	}

	public String declareAndAssignOutsideBlockDoubleAssignment(boolean b) {
		String s4 = "nothing so far";
		System.out.println("abc");
		if (b == true) {
			s4 = "nothing";
			s4 = "true";
		} else {
			s4 = "nothing";
			s4 = "false";
		}
		return s4;
	}

	public String sameNameInMultipleBlocks(boolean b) {
		String out1;
		if (b == true) {
			String s5 = "true";
			out1 = s5 + s5;
		} else {
			String s5 = "false";
			out1 = s5 + s5;
		}
		return out1;
	}

	public String multipleVariablesInBlock(boolean b) {
		String s6 = "3 -> ";
		if (b == true) {
			String ss1 = "one";
			String ss2 = "two";
			String ss3 = "three";
			return s6 + ss1 + ss2 + ss3;
		}
		return "nothing";
	}

	public String multipleNestings(boolean b1, boolean b2) {
		String out2;
		if (b1 == true) {
			String s7 = "true";
			if (b2 == true) {
				String inner = "true";
				out2 = s7 + "->" + inner;
			} else {
				String inner = "false";
				out2 = s7 + "->" + inner;
			}
		} else {
			String s7 = "false";
			if (b2 == true) {
				String inner = "true";
				out2 = s7 + "->" + inner;
			} else {
				String inner = "false";
				out2 = s7 + "->" + inner;
			}
		}
		return out2;
	}
	
	// todo. add tests where the variable in the scopes has the same name but is of a different type
}

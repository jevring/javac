/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

import net.jevring.javac.extras.A;

public class ExpressionNames {

	public String allGetters() {
		A a = new A();
		return a.getB().getC().getD().getHello();
	}

	public String allStaticGetters() {
		return A.getBStatic().getCStatic().getDStatic().getHelloStatic();
	}

	public String allStaticFields() {
		return A.b.c.d.hello;
	}

	public double allStaticFieldsDouble() {
		return A.b.c.d.myDouble;
	}

	public String staticGettersOnInstance() {
		A a = new A();
		return a.getBStatic().getCStatic().getDStatic().getHelloStatic();
	}

	// Interestingly, for the static getters, the normal java compiler will just basically walk the objects, pop then off of the stack, 
	// and at the end just return "hello". In one case it even adds a null check. Very weird.
	// though I suppose it'll resolve constants as often as it can

	public String allFields() {
		A a = new A();
		return a.nonStaticB.nonStaticC.nonStaticD.nonStaticHello;
	}

	public double allFieldsDouble() {
		A a = new A();
		return a.nonStaticB.nonStaticC.nonStaticD.nonStaticMyDouble;
	}

	public String staticFieldsOnInstance() {
		A a = new A();
		return a.b.c.d.hello;
	}

	public String interleavedStaticGettersAndStaticFields() {
		A a = new A();
		return a.b.getCStatic().d.hello;
	}

	public String interleavedGettersAndStaticFields() {
		A a = new A();
		return a.b.getC().d.hello;
	}

	public String interleavedGettersAndFields() {
		A a = new A();
		return a.nonStaticB.getC().nonStaticD.nonStaticHello;
	}

	public double staticFieldsOnInstanceDouble() {
		A a = new A();
		return a.b.c.d.myDouble;
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

// NOTE: some of these should retain their *, for testing purposes:
// import java.util.*;
// import static java.lang.Integer.*;
import net.jevring.javac.extras.IntContainer;
import java.util.*; 

import static java.lang.Integer.*;
import static java.time.Year.now;

// this does a bit more than instantiate things, but I may as well make sure the imports all work properly
public class Instantiation {
	private static final IntContainer CONST = new IntContainer(1, 1.5f, "bob", Locale.CHINESE);

	static {
		CONST.add(15);
	}

	public IntContainer getSet() {
		return CONST;
	}

	public int getDynamic() {
		IntContainer set = new IntContainer(2, 2.5f, "mike", Locale.ENGLISH);
		set.add(now().getValue());
		set.add(2);
		set.add(parseInt("3"));
		return set.size();
	}

	public int getConst() {
		return CONST.size();
	}
	
	public net.jevring.javac.extras.IntContainer newIntContainer() {
		return new net.jevring.javac.extras.IntContainer();
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

import net.jevring.javac.extras.A;
import net.jevring.javac.extras.AssignmentHelper;

public class VariableAssignment {

	public AssignmentHelper assignmentHelper = new AssignmentHelper();

	public VariableAssignment() {
	}

	public VariableAssignment(AssignmentHelper assignmentHelper) {
		this.assignmentHelper = assignmentHelper;
	}

	public VariableAssignment(String s) {
		// this does nothing
	}

	public void staticAssignment() {
		AssignmentHelper.myStaticInt = 666;
		AssignmentHelper.prefilledStaticInt = 777;
		AssignmentHelper.myStaticString = "goodbye";
		AssignmentHelper.prefilledStaticString = "pancakes!";
	}

	public void classVariableFieldAssignment() {
		assignmentHelper.myInt = 111;
		assignmentHelper.prefilledInt = 222;
		assignmentHelper.myString = "popsicles";
		assignmentHelper.prefilledString = "bananas";
	}

	public AssignmentHelper localVariableFieldAssignment() {
		AssignmentHelper assignmentHelper = new AssignmentHelper();
		assignmentHelper.myInt = 333;
		assignmentHelper.prefilledInt = 444;
		assignmentHelper.myString = "monkeys";
		assignmentHelper.prefilledString = "a glass of water";
		return assignmentHelper;
	}

	public A interleavedGettersAndStaticFields() {
		A a = new A();
		a.b.getC().d.writableString = "candy";
		return a;
	}

	public A allFields(String value) {
		A a = new A();
		a.nonStaticB.nonStaticC.nonStaticD.writableString = value;
		return a;
	}

	public void allStaticFields() {
		A.b.c.d.writableString = "candy is delicious";
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

public class Literals {
	// todo: use + and - as well
	public byte myByte = (byte) 127;
	public short myShort = (short) 32767;
	public int myHexInt = 0x7fffffff;
	public int myDecimalInt = 2147483647;
	public int myOctalInt = 017777777777;
	public int myBinaryInt = 0b1111111111111111111111111111111;
	public float myHexFloat = 0X1.fffffeP+127f;
	public float myFloat = 34028f;
	public float myScientificFloat = 3.40E1f;
	public double myHexDouble = 0x1.fffffffffffffP+1023;
	public double myDecimalDouble = 1.7976931348623157E308;
	public double myDouble = 99999999999999999999999999999999999.999999999999999999;
	public long myHexLong = 0x7fffffffffffffffL;
	public long myBinaryLong = 0B111111111111111111111111111111111111111111111111111111111111111L;
	public long myDecimalLong = 9223372036854775807L;
	public long myOctalLong = 0777777777777777777777L;
	public long myDelimiteredLong = 10_000_000_000_000_000L;
	public int myDelimiteredInt = 100_000_000;
	public int myDelimiteredBinaryInt = 0b101_11110101_11100001_00000000;


}

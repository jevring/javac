/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

import net.jevring.javac.extras.HashIntSet;
import net.jevring.javac.extras.IntSet;

public class ConcreteImplementationToInterface {
	private final IntSet set1 = new HashIntSet();
	private IntSet set2;

	public ConcreteImplementationToInterface(IntSet set2) {
		this.set2 = set2;
	}

	public IntSet assignField() {
		set2 = new HashIntSet();
		set2.add(1);
		set2.add(2);
		return set2;
	}

	public IntSet createNew() {
		IntSet set3 = new HashIntSet();
		set3.add(3);
		set3.add(4);
		return set3;
	}

	public IntSet addToField() {
		set2.add(5);
		set2.add(6);
		return set2;
	}


}

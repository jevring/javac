/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

import net.jevring.javac.extras.*;

import java.util.concurrent.locks.Lock;

public class ThrowsException {
	public ThrowsException() {
	}

	public ThrowsException(String message) throws Exception {
		throw new Exception(message);
		// todo: this generates an extra 'return' byte code at the end. it doesn't hurt, but it shouldn't be there.
	}

	public void throwException(String message) throws MyException {
		throw new MyException(message);
	}

	public void throwExceptionOfType(int exceptionClass, String message) throws MyException, MyOtherException {
		if (exceptionClass != 0) {
			if (exceptionClass == 1) {
				throw new MyException(message);
			} else {
				throw new MyOtherException(message);
			}
		}
		return; // todo: handle the implicit return here, then remove this explicit one
	}

	public void throwRuntimeException(String message) {
		throw new MyRuntimeException(message);
	}

	public void throwError(String message) {
		throw new MyError(message);
	}

	public StringList tryCatchFinallyNested(int exceptionClass, String message) {
		StringList events = new StringArrayList();
		try {
			events.add("before call");
			try {
				events.add("2. before call");
				throwExceptionOfType(exceptionClass, message);
				events.add("2. after call");
			} catch (Exception e) {
				events.add(exceptionEvent("2. caught exception: ", e));
				throw new MyOtherException(message);
			} finally {
				events.add("2. in finally");
			}
			events.add("after call");
		} catch (Exception e) {
			events.add(exceptionEvent("caught exception: ", e));
		} finally {
			events.add("in finally");
		}
		events.add("after everything");
		return events;
	}

	public String tryFinallyReturnException(Lock lock, boolean shouldThrow) {
		lock.lock();
		try {
			if (shouldThrow) {
				throw new RuntimeException();
			}
			return hello();
		} catch (RuntimeException e) {
			return "caught exception";
		} finally {
			lock.unlock();
		}
	}

	public int tryLockMultipleReturns(Lock lock, boolean b) {
		lock.lock();
		try {
			if (b) {
				return 666;
			} else {
				return 777;
			}
		} finally {
			lock.unlock();
		}
	}

	public String tryFinallyThrow(StringList events) throws MyException {
		try {
			throw new MyException("123");
		} finally {
			events.add("finally");
		}
	}

	public String throwFromFinally() throws MyException, MyOtherException {
		try {
			throw new MyException("123");
		} finally {
			// this is bad practice, but it should be supported none the less
			throw new MyOtherException("456");
		}
	}

	public StringList tryCatchNestedReturn() {
		StringList events = new StringArrayList();
		try {
			events.add("before call");
			try {
				events.add("2. before call");
				return events;
			} catch (Exception e) {
				events.add(exceptionEvent("2. caught exception: ", e));
				return events;
			} finally {
				events.add("2. in finally");
			}
		} catch (Exception e) {
			events.add(exceptionEvent("caught exception: ", e));
		} finally {
			events.add("in finally");
		}
		return events;
	}

	public String tryFinallyNestedReturn(StringList events, String message) {
		try {
			try {
				return message;
			} finally {
				events.add("2. in finally");
			}
		} finally {
			events.add("in finally");
		}
	}

	public String tryCatch(String message) {
		try {
			throwException(message);
			return "nope";
		} catch (MyException e) {
			return e.getMessage();
		}
	}

	public String tryCatchThrowable(String message) {
		try {
			throwException(message);
			return "nope";
		} catch (Throwable e) {
			return e.getMessage();
		}
	}

	public String tryLock(Lock lock) {
		lock.lock();
		try {
			return "Hello";
		} finally {
			lock.unlock();
		}
	}

	public StringList tryCatchContinue(int exceptionClass, String message) {
		StringList events = new StringArrayList();
		try {
			events.add("before call");
			throwExceptionOfType(exceptionClass, message);
			events.add("after call");
		} catch (Exception e) {
			events.add(exceptionEvent("caught exception: ", e));
		}
		events.add("after everything");
		System.out.println("events = " + events);
		return events;
	}

	public String tryCatchMultiple(int exceptionClass, String message) {
		try {
			throwExceptionOfType(exceptionClass, message);
			return "m-nothing-" + message;
		} catch (MyException e) {
			return "m-MyException-" + e.getMessage();
		} catch (MyOtherException e1) {
			return "m-MyOtherException-" + e1.getMessage();
		}
	}

	public String tryCatchPipeSeparated(int exceptionClass, String message) {
		try {
			throwExceptionOfType(exceptionClass, message);
			return "ps-nothing-" + message;
		} catch (MyException | MyOtherException e) {
			return "ps-" + e.getClass().getSimpleName() + "-" + e.getMessage();
		}
	}

	public StringList tryCatchFinally(int exceptionClass, String message) {
		StringList events = new StringArrayList();
		try {
			events.add("before call");
			throwExceptionOfType(exceptionClass, message);
			events.add("after call");
		} catch (Exception e) {
			events.add(exceptionEvent("caught exception: ", e));
		} finally {
			events.add("in finally");
		}
		return events;
	}

	public StringList tryCatchFinallyContinue(int exceptionClass, String message) {
		StringList events = new StringArrayList();
		try {
			events.add("before call");
			throwExceptionOfType(exceptionClass, message);
			events.add("after call");
		} catch (Exception e) {
			events.add(exceptionEvent("caught exception: ", e));
		} finally {
			events.add("in finally");
		}
		events.add("after everything");
		int i0 = exceptionClass;
		int i1 = i0;
		int i2 = i1;
		int i3 = i2;
		int i4 = i3;
		System.out.println("events = " + events);
		return events;
	}

	private String hello() {
		return "Hello";
	}

	private String exceptionEvent(String prefix, Exception e) {
		return prefix + e.getClass().getSimpleName() + "-" + e.getMessage();
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

public class IfStatements {
	private static boolean checkCondition() {
		return true;
	}

	public static boolean identity(boolean a) {
		return a;
	}

	public static boolean or(boolean a, boolean b) {
		return a || b;
	}

	public static String orIfStatement(boolean a, boolean b) {
		if (a || b) {
			return "yes";
		} else {
			return "no";
		}
	}

	public static boolean or3(boolean a, boolean b, boolean c) {
		return a || b || c;
	}

	public static boolean and(boolean a, boolean b) {
		return a && b;
	}

	public static String andIfStatement(boolean a, boolean b) {
		if (a && b) {
			return "yes";
		} else {
			return "no";
		}
	}

	public static boolean and3(boolean a, boolean b, boolean c) {
		return a && b && c;
	}

	public static boolean xor(boolean a, boolean b) {
		return a ^ b;
	}

	public static String xorIfStatement(boolean a, boolean b) {
		if (a ^ b) {
			return "yes";
		} else {
			return "no";
		}
	}

	/* todo: support this too	
		public static boolean notAAndB(boolean a, boolean b) {
			return !(a && b);
		}
		
		public static boolean notAOrNotB(boolean a, boolean b) {
			return !a || !b;
		}
	*/
	public static String outsideRangeAssignment(int start, int end, int value) {
		boolean b = value < start || value > end;
		if (b) {
			return "outside";
		} else {
			return "inside";
		}
	}

	public static String lessThanStartAndEnd(int start, int end, int value) {
		boolean b = value < start && value < end;
		if (b) {
			return "yes";
		} else {
			return "no";
		}
	}

	public static String lessThanStartXorEnd(int start, int end, int value) {
		boolean b = value < start ^ value < end;
		if (b) {
			return "yes";
		} else {
			return "no";
		}
	}

	public static String outsideRange(int start, int end, int value) {
		if (value < start || value > end) {
			return "outside";
		} else {
			return "inside";
		}
	}

	public static String outsideRangeViaMethodWithAssigmnent(int start, int end, int value) {
		boolean beforeStart = value < start;
		boolean afterEnd = value > end;
		if (or(beforeStart, afterEnd)) {
			return "outside";
		} else {
			return "inside";
		}
	}

	public static String outsideRangeViaIdentityMethod(int start, int end, int value) {
		if (identity(value < start || value > end)) {
			return "outside";
		} else {
			return "inside";
		}
	}

	public static String outsideRangeViaMethod(int start, int end, int value) {
		if (or(value < start, value > end)) {
			return "outside";
		} else {
			return "inside";
		}
	}

	public static String elseIf(int one, int two, int three) {
		if (one < 100) {
			return "one";
		} else if (two < 100) {
			return "two";
		} else if (three < 100) {
			return "three";
		} else {
			return "nothing";
		}
	}

	/*
		public static String outsideRange3(int start, int end, int value) {
			// todo: add unit test
			if (start < value && value < end && value < 100) {
				return "inside";
			} else {
				return "outside";
			}
		}
	  */
	public static String drivingAge(int ageInYears) {
		if (ageInYears < 18 || ageInYears > 70) {
			return "I don't think so";
		} else {
			return "you know it!";
		}
	}

	public static boolean wasNullReturn(Object o) {
		return o == null;
	}

	public static boolean wasNotNullReturn(Object o) {
		return o != null;
	}

	public static String wasNullIfStatement(Object o) {
		if (o == null) {
			return "was null";
		} else {
			return "was not null";
		}
	}

	public static String wasNotNullIfStatement(Object o) {
		if (o != null) {
			return "was not null";
		} else {
			return "was null";
		}
	}

	public static int wasNotNullTernary(Object o) {
		String nullity = o == null ? "yes" : "no";
		return nullity.length();
	}

	public static String wasB(boolean b) {
		if (b) {
			return "was b";
		} else {
			return "was not b";
		}
	}

	public static String viaCheckCondition() {
		if (checkCondition()) {
			return "condition was true";
		} else {
			return "condition was false";
		}
	}

	public static String alwaysTrue() {
		if (true) {
			return "always true";
		} else {
			throw new AssertionError("This will never happen");
		}
	}

	public static int max(int one, int two) {
		if (one > two) {
			return one;
		} else {
			return two;
		}
	}

	public static int maxIfStatementWithAssignment(int one, int two) {
		boolean b = one > two;
		if (b) {
			return one;
		} else {
			return two;
		}
	}

	public static int min(int one, int two) {
		if (one < two) {
			return one;
		} else {
			return two;
		}
	}

	public static int maxTernary(int one, int two) {
		return one > two ? one : two;
	}

	public static int maxTernaryWithAssignment(int one, int two) {
		int i;
		System.out.println("maxTernaryWithAssignment");
		i = one > two ? one : two;
		return i;
	}

	public static boolean isTwelve(int i) {
		if (i == 12) {
			System.out.println(i);
			return true;
		}
		return false;
	}

	public static boolean isZeroWithAssignment(int i) {
		boolean b = i == 0;
		System.out.println(b);
		return b;
	}

	public static boolean isOne(int i) {
		return i == 1;
	}

	public static boolean isTwo(int i) {
		boolean b = false;
		System.out.println(b);
		b = i == 2;
		return b;
	}

	public static boolean isThree(int i) {
		boolean b;
		System.out.println("three");
		b = i == 3;
		return b;
	}
}

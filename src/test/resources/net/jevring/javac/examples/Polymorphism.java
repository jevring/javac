/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

import net.jevring.javac.extras.*;

public class Polymorphism {
	private String stringify(Object o) {
		// this will test the ability to resolve external classes via Object
		return "object: " + String.valueOf(o);
	}

	private String stringify(StringList o) {
		return "string list: " + String.valueOf(o);
	}

	private String stringify(ParentA o) {
		return "A: " + String.valueOf(o);
	}

	private String stringify(ChildB o) {
		return "B: " + String.valueOf(o);
	}

	private String stringify(int o) {
		return "int: " + String.valueOf(o);
	}

	private String stringify(ChildB b, CharSequence o) {
		return "B + object: " + String.valueOf(b) + "+" + String.valueOf(o);
	}

	public String stringify() {
		// this will test the ability to resolve internal classes via Object
		return stringify("hello");
	}

	public String stringifyStringArrayList() {
		return stringify(StringArrayList.of("hello"));
	}

	public String stringifyD() {
		return stringify(new ChildD("t"));
	}

	public String stringifyDAndHello() {
		return stringify(new ChildD("t"), "hello");
	}

	public String stringifyInt() {
		return stringify(25);
	}
}

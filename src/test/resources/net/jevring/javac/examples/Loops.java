/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

public class Loops {
	public int plusPlusInvocations = 0;
	public int minusMinusInvocations = 0;

	public int whileIGT50() {
		int i = 100;
		while (i > 50) {
			i = i - 1;
		}
		return i;
	}

	public int doWhileIGT50() {
		int i = 100;
		do {
			i = i - 1;
		} while (i > 50);
		return i;
	}

	public int doWhileIGT50StartAt25() {
		int i = 25;
		int sum = 0;
		do {
			sum = sum + 1;
		} while (i != 25);
		return sum;
	}

	public int staticParameters() {
		int sum = 0;
		for (int i = 0; i < 100; i = i + 1) {
			sum = sum + i;
		}
		return sum;
	}

	public int plusPlus() {
		plusPlusInvocations++;
		int sum = 0;
		for (int i = 0; i < 100; i++) {
			sum--;
		}
		return sum;
	}

	public int minusMinus() {
		minusMinusInvocations--;
		int sum = 0;
		for (int i = 100; i > 0; i--) {
			// todo: if we do 'sum = sum++;', the sum++ doesn't get picked up...
			sum++;
		}
		return sum;
	}

	public int parametersFromMethods() {
		int sum = 0;
		for (int i = start(); i < end(); i = i + increment()) {
			sum = sum + i;
		}
		return sum;
	}

	private int start() {
		return 5;
	}

	private int end() {
		return 10;
	}

	private int increment() {
		return 2;
	}

	public int parametersFromParameters(int from, int to, int step) {
		int sum = 0;
		for (int i = from; i < to; i = i + step) {
			sum = sum + i;
		}
		return sum;
	}
}

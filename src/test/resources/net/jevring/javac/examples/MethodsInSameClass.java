/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.examples;

public class MethodsInSameClass {
	private final String firstField = "first";

	// declaration order matters here, so we can know we can reference things declared after the point of use as well
	private String identityFirst(String s) {
		return s;
	}

	public String identities(String first, String last) {
		return firstField + identityFirst(first) + lastField + identityLast(last);
	}

	public String deepChain(String s) {
		return one(s);
	}

	public String broadChain(String s) {
		return three(three(three(s)));
	}


	private String one(String s) {
		return "1" + two(s);
	}

	private String two(String s) {
		return "2" + three(s);
	}

	private String three(String s) {
		return "3" + s;
	}

	private String identityLast(String s) {
		return s;
	}

	private final String lastField = "last";
}

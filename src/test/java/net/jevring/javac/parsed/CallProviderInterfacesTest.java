/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.IntProvider;
import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class CallProviderInterfacesTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/CallProviderInterfaces.java"));
	}

	@Test
	public void test() throws Exception {
		IntProvider onesAndTwos = new IntProvider() {
			@Override
			public int provideInt(boolean useDefaultValue) {
				if (useDefaultValue) {
					return 111;
				} else {
					return 222;
				}
			}
		};

		IntProvider fivesAndSixes = new IntProvider() {
			@Override
			public int provideInt(boolean useDefaultValue) {
				if (useDefaultValue) {
					return 555;
				} else {
					return 666;
				}
			}
		};
		Object instance = parsed.getConstructor(IntProvider.class).newInstance(onesAndTwos);
		Method callField = parsed.getMethod("callField", boolean.class);

		assertEquals(222, callField.invoke(instance, false));
		assertEquals(111, callField.invoke(instance, true));

		Method callWithDefaultValue = parsed.getMethod("callWithDefaultValue", IntProvider.class);

		assertEquals(555, callWithDefaultValue.invoke(instance, fivesAndSixes));
		assertEquals(111, callWithDefaultValue.invoke(instance, onesAndTwos));

	}
}

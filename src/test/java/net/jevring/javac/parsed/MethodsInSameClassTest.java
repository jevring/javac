/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class MethodsInSameClassTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;


	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/MethodsInSameClass.java"));
		// trigger ONE error, rather than one per test
		parsed.getConstructor();
	}

	@Test
	public void testIdentities() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String identities = (String) parsed.getMethod("identities", String.class, String.class).invoke(instance, "hello", "goodbye");
		assertEquals("firsthellolastgoodbye", identities);
	}

	@Test
	public void testDeepChain() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String identities = (String) parsed.getMethod("deepChain", String.class).invoke(instance, "hello");
		assertEquals("123hello", identities);
	}

	@Test
	public void testBroadChain() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String identities = (String) parsed.getMethod("broadChain", String.class).invoke(instance, "hello");
		assertEquals("333hello", identities);
	}
}

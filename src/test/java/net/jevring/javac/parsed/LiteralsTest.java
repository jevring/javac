/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class LiteralsTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;
	private static Object instance;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/Literals.java"));
		instance = parsed.getConstructor().newInstance();
	}

	@Test
	public void testBytes() throws Exception {
		test("myByte", (byte) 127);
	}

	@Test
	public void testShorts() throws Exception {
		test("myShort", (short) 32767);
	}

	@Test
	public void testInts() throws Exception {
		test("myHexInt", 0x7fffffff);
		test("myDecimalInt", 2147483647);
		test("myOctalInt", 017777777777);
		test("myBinaryInt", 0b1111111111111111111111111111111);
		test("myDelimiteredInt", 100_000_000);
		test("myDelimiteredBinaryInt", 0b101_11110101_11100001_00000000);
	}

	@Test
	public void testLongs() throws Exception {
		test("myHexLong", 0x7fffffffffffffffL);
		test("myBinaryLong", 0B111111111111111111111111111111111111111111111111111111111111111L);
		test("myDecimalLong", 9223372036854775807L);
		test("myOctalLong", 0777777777777777777777L);
		test("myDelimiteredLong", 10_000_000_000_000_000L);
	}

	@Test
	public void testDoubles() throws Exception {
		test("myHexDouble", 0x1.fffffffffffffP+1023);
		test("myDecimalDouble", 1.7976931348623157E308);
		test("myDouble", 99999999999999999999999999999999999.999999999999999999);
	}

	@Test
	public void testFloats() throws Exception {
		test("myHexFloat", 0X1.fffffeP+127f);
		test("myFloat", 34028f);
		test("myScientificFloat", 3.40E1f);
	}

	private void test(String name, Object expectedValue) throws IllegalAccessException, NoSuchFieldException {
		Object myShort = parsed.getField(name).get(instance);
		assertEquals(expectedValue, myShort);
	}

}

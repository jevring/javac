/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class IfStatementsTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testIfStatement() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/IfStatements.java"));

		Method max = parsed.getMethod("max", int.class, int.class);
		assertEquals(2, max.invoke(null, 1, 2));
		assertEquals(3, max.invoke(null, 3, 2));

		Method maxIfStatementWithAssignment = parsed.getMethod("maxIfStatementWithAssignment", int.class, int.class);
		assertEquals(2, maxIfStatementWithAssignment.invoke(null, 1, 2));
		assertEquals(3, maxIfStatementWithAssignment.invoke(null, 3, 2));

		Method min = parsed.getMethod("min", int.class, int.class);
		assertEquals(1, min.invoke(null, 1, 2));
		assertEquals(2, min.invoke(null, 3, 2));

		Method maxTernary = parsed.getMethod("maxTernary", int.class, int.class);
		assertEquals(2, maxTernary.invoke(null, 1, 2));
		assertEquals(3, maxTernary.invoke(null, 3, 2));

		Method maxTernaryWithAssignment = parsed.getMethod("maxTernaryWithAssignment", int.class, int.class);
		assertEquals(2, maxTernaryWithAssignment.invoke(null, 1, 2));
		assertEquals(3, maxTernaryWithAssignment.invoke(null, 3, 2));

		Method isZero = parsed.getMethod("isTwelve", int.class);
		assertEquals(true, isZero.invoke(null, 12));
		assertEquals(false, isZero.invoke(null, 1));

		Method isZeroWithAssignment = parsed.getMethod("isZeroWithAssignment", int.class);
		assertEquals(true, isZeroWithAssignment.invoke(null, 0));
		assertEquals(false, isZeroWithAssignment.invoke(null, 1));

		Method isOne = parsed.getMethod("isOne", int.class);
		assertEquals(false, isOne.invoke(null, 0));
		assertEquals(true, isOne.invoke(null, 1));

		Method isTwo = parsed.getMethod("isTwo", int.class);
		assertEquals(false, isTwo.invoke(null, 0));
		assertEquals(true, isTwo.invoke(null, 2));

		Method isThree = parsed.getMethod("isThree", int.class);
		assertEquals(false, isThree.invoke(null, 0));
		assertEquals(true, isThree.invoke(null, 3));

		Method wasB = parsed.getMethod("wasB", boolean.class);
		assertEquals("was b", wasB.invoke(null, true));
		assertEquals("was not b", wasB.invoke(null, false));

		Method viaCheckCondition = parsed.getMethod("viaCheckCondition");
		assertEquals("condition was true", viaCheckCondition.invoke(null));

		Method alwaysTrue = parsed.getMethod("alwaysTrue");
		assertEquals("always true", alwaysTrue.invoke(null));

		Method wasNullReturn = parsed.getMethod("wasNullReturn", Object.class);
		assertEquals(true, wasNullReturn.invoke(null, new Object[]{null}));
		assertEquals(false, wasNullReturn.invoke(null, new Object()));

		Method wasNotNullReturn = parsed.getMethod("wasNotNullReturn", Object.class);
		assertEquals(false, wasNotNullReturn.invoke(null, new Object[]{null}));
		assertEquals(true, wasNotNullReturn.invoke(null, new Object()));

		Method wasNullIfStatement = parsed.getMethod("wasNullIfStatement", Object.class);
		assertEquals("was null", wasNullIfStatement.invoke(null, new Object[]{null}));
		assertEquals("was not null", wasNullIfStatement.invoke(null, new Object()));

		Method wasNotNullIfStatement = parsed.getMethod("wasNotNullIfStatement", Object.class);
		assertEquals("was null", wasNotNullIfStatement.invoke(null, new Object[]{null}));
		assertEquals("was not null", wasNotNullIfStatement.invoke(null, new Object()));

		Method wasNotNullTernary = parsed.getMethod("wasNotNullTernary", Object.class);
		assertEquals(3, wasNotNullTernary.invoke(null, new Object[]{null}));
		assertEquals(2, wasNotNullTernary.invoke(null, new Object()));

		Method or = parsed.getMethod("or", boolean.class, boolean.class);
		assertEquals(true, or.invoke(null, true, true));
		assertEquals(true, or.invoke(null, true, false));
		assertEquals(true, or.invoke(null, false, true));
		assertEquals(false, or.invoke(null, false, false));

		Method or3 = parsed.getMethod("or3", boolean.class, boolean.class, boolean.class);
		assertEquals(true, or3.invoke(null, true, true, true));
		assertEquals(true, or3.invoke(null, true, false, true));
		assertEquals(true, or3.invoke(null, false, true, true));
		assertEquals(true, or3.invoke(null, false, false, true));

		assertEquals(true, or3.invoke(null, true, true, false));
		assertEquals(true, or3.invoke(null, true, false, false));
		assertEquals(true, or3.invoke(null, false, true, false));
		assertEquals(false, or3.invoke(null, false, false, false));

		Method orIfStatement = parsed.getMethod("orIfStatement", boolean.class, boolean.class);
		assertEquals("yes", orIfStatement.invoke(null, true, true));
		assertEquals("yes", orIfStatement.invoke(null, true, false));
		assertEquals("yes", orIfStatement.invoke(null, false, true));
		assertEquals("no", orIfStatement.invoke(null, false, false));

		Method and = parsed.getMethod("and", boolean.class, boolean.class);
		assertEquals(true, and.invoke(null, true, true));
		assertEquals(false, and.invoke(null, true, false));
		assertEquals(false, and.invoke(null, false, true));
		assertEquals(false, and.invoke(null, false, false));

		Method and3 = parsed.getMethod("and3", boolean.class, boolean.class, boolean.class);
		assertEquals(true, and3.invoke(null, true, true, true));
		assertEquals(false, and3.invoke(null, true, false, true));
		assertEquals(false, and3.invoke(null, false, true, true));
		assertEquals(false, and3.invoke(null, false, false, true));

		assertEquals(false, and3.invoke(null, true, true, false));
		assertEquals(false, and3.invoke(null, true, false, false));
		assertEquals(false, and3.invoke(null, false, true, false));
		assertEquals(false, and3.invoke(null, false, false, false));

		Method andIfStatement = parsed.getMethod("andIfStatement", boolean.class, boolean.class);
		assertEquals("yes", andIfStatement.invoke(null, true, true));
		assertEquals("no", andIfStatement.invoke(null, true, false));
		assertEquals("no", andIfStatement.invoke(null, false, true));
		assertEquals("no", andIfStatement.invoke(null, false, false));

		Method xor = parsed.getMethod("xor", boolean.class, boolean.class);
		assertEquals(false, xor.invoke(null, true, true));
		assertEquals(true, xor.invoke(null, true, false));
		assertEquals(true, xor.invoke(null, false, true));
		assertEquals(false, xor.invoke(null, false, false));

		Method xorIfStatement = parsed.getMethod("xorIfStatement", boolean.class, boolean.class);
		assertEquals("no", xorIfStatement.invoke(null, true, true));
		assertEquals("yes", xorIfStatement.invoke(null, true, false));
		assertEquals("yes", xorIfStatement.invoke(null, false, true));
		assertEquals("no", xorIfStatement.invoke(null, false, false));

		Method outsideRangeAssignment = parsed.getMethod("outsideRangeAssignment", int.class, int.class, int.class);
		assertEquals("inside", outsideRangeAssignment.invoke(null, 10, 20, 15));
		assertEquals("outside", outsideRangeAssignment.invoke(null, 10, 20, 9));
		assertEquals("outside", outsideRangeAssignment.invoke(null, 10, 20, 21));

		Method lessThanStartAndEnd = parsed.getMethod("lessThanStartAndEnd", int.class, int.class, int.class);
		assertEquals("yes", lessThanStartAndEnd.invoke(null, 10, 20, 5));
		assertEquals("no", lessThanStartAndEnd.invoke(null, 10, 20, 15));
		assertEquals("no", lessThanStartAndEnd.invoke(null, 10, 20, 21));

		Method lessThanStartXorEnd = parsed.getMethod("lessThanStartXorEnd", int.class, int.class, int.class);
		assertEquals("no", lessThanStartXorEnd.invoke(null, 10, 20, 5));
		assertEquals("yes", lessThanStartXorEnd.invoke(null, 10, 20, 15));
		assertEquals("no", lessThanStartXorEnd.invoke(null, 10, 20, 21));

		Method outsideRange = parsed.getMethod("outsideRange", int.class, int.class, int.class);
		assertEquals("inside", outsideRange.invoke(null, 10, 20, 15));
		assertEquals("outside", outsideRange.invoke(null, 10, 20, 9));
		assertEquals("outside", outsideRange.invoke(null, 10, 20, 21));

		Method outsideRangeViaMethod = parsed.getMethod("outsideRangeViaMethod", int.class, int.class, int.class);
		assertEquals("inside", outsideRangeViaMethod.invoke(null, 10, 20, 15));
		assertEquals("outside", outsideRangeViaMethod.invoke(null, 10, 20, 9));
		assertEquals("outside", outsideRangeViaMethod.invoke(null, 10, 20, 21));

		Method outsideRangeViaIdentityMethod = parsed.getMethod("outsideRangeViaIdentityMethod", int.class, int.class, int.class);
		assertEquals("inside", outsideRangeViaIdentityMethod.invoke(null, 10, 20, 15));
		assertEquals("outside", outsideRangeViaIdentityMethod.invoke(null, 10, 20, 9));
		assertEquals("outside", outsideRangeViaIdentityMethod.invoke(null, 10, 20, 21));

		Method outsideRangeViaMethodWithAssigmnent = parsed.getMethod("outsideRangeViaMethodWithAssigmnent", int.class, int.class, int.class);
		assertEquals("inside", outsideRangeViaMethodWithAssigmnent.invoke(null, 10, 20, 15));
		assertEquals("outside", outsideRangeViaMethodWithAssigmnent.invoke(null, 10, 20, 9));
		assertEquals("outside", outsideRangeViaMethodWithAssigmnent.invoke(null, 10, 20, 21));

		Method drivingAge = parsed.getMethod("drivingAge", int.class);
		assertEquals("I don't think so", drivingAge.invoke(null, 12));
		assertEquals("I don't think so", drivingAge.invoke(null, 82));
		assertEquals("you know it!", drivingAge.invoke(null, 19));

		Method elseIf = parsed.getMethod("elseIf", int.class, int.class, int.class);
		assertEquals("one", elseIf.invoke(null, 99, 99, 99));
		assertEquals("two", elseIf.invoke(null, 100, 99, 99));
		assertEquals("three", elseIf.invoke(null, 100, 100, 99));
		assertEquals("nothing", elseIf.invoke(null, 100, 100, 100));
	}
}

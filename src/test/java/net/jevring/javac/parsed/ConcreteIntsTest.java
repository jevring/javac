/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.AbstractInts;
import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class ConcreteIntsTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/ConcreteInts.java"));
	}

	@Test
	public void testParameterizedConstructor() throws Exception {
		AbstractInts instance = (AbstractInts) parsed.getConstructor(int.class).newInstance(222);
		Method getTwoViaSuper = parsed.getMethod("getTwoViaSuper");

		assertEquals(222, getTwoViaSuper.invoke(instance));
		assertEquals(222, instance.getTwo());
		assertEquals(1, instance.getOne());

	}

	@Test
	public void testNoArgsConstructor() throws Exception {
		AbstractInts instance = (AbstractInts) parsed.getConstructor().newInstance();
		Method getTwoViaSuper = parsed.getMethod("getTwoViaSuper");

		assertEquals(666, getTwoViaSuper.invoke(instance));
		assertEquals(666, instance.getTwo());
		assertEquals(1, instance.getOne());

	}
}

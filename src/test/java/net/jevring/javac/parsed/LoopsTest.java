/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class LoopsTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;
	private static java.lang.Object instance;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/Loops.java"));
		instance = parsed.getConstructor().newInstance();
	}

	@Test
	public void testWhileIGT50() throws Exception {
		assertEquals(whileIGT50(), parsed.getMethod("whileIGT50").invoke(instance));
	}

	@Test
	public void testDoWhileIGT50() throws Exception {
		assertEquals(doWhileIGT50(), parsed.getMethod("doWhileIGT50").invoke(instance));
	}

	@Test
	public void testDoWhileIGT50StartAt25() throws Exception {
		assertEquals(doWhileIGT50StartAt25(), parsed.getMethod("doWhileIGT50StartAt25").invoke(instance));
	}

	@Test
	public void testStaticParameters() throws Exception {
		assertEquals(4950, parsed.getMethod("staticParameters").invoke(instance));
	}

	@Test
	public void testParametersFromMethods() throws Exception {
		assertEquals(21, parsed.getMethod("parametersFromMethods").invoke(instance));
	}

	@Test
	public void testParametersFromParameters() throws Exception {
		assertEquals(1225, parsed.getMethod("parametersFromParameters", int.class, int.class, int.class).invoke(instance, 0, 50, 1));
	}

	@Test
	public void testPlusPlus() throws Exception {
		assertEquals(-100, parsed.getMethod("plusPlus").invoke(instance));
		assertEquals(1, parsed.getField("plusPlusInvocations").get(instance));
	}

	@Test
	public void testMinusMinus() throws Exception {
		assertEquals(100, parsed.getMethod("minusMinus").invoke(instance));
		assertEquals(-1, parsed.getField("minusMinusInvocations").get(instance));
	}

	private static int whileIGT50() {
		int i = 100;
		while (i > 50) {
			i = i - 1;
		}
		return i;
	}

	private static int doWhileIGT50() {
		int i = 100;
		do {
			i = i - 1;
		} while (i > 50);
		return i;
	}

	private static int doWhileIGT50StartAt25() {
		int i = 25;
		int sum = 0;
		do {
			sum = sum + 1;
		} while (i != 25);
		return sum;
	}

}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class ScopesTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;


	@BeforeClass
	public static void setUp() throws Exception {
		// todo: why isn't the VerifyError thrown here, rather than when we try to access it via reflection
		// todo: make sure this test isn't IGNORED if the compiler throws a ClassFormatError
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/Scopes.java"));
		// trigger ONE error, rather than one per test
		parsed.getConstructor();
	}

	@Test
	public void testDeclareOutsideBlock() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String whenTrue = (String) parsed.getMethod("declareOutsideBlock", boolean.class).invoke(instance, true);
		assertEquals("true", whenTrue);

		String whenFalse = (String) parsed.getMethod("declareOutsideBlock", boolean.class).invoke(instance, false);
		assertEquals("false", whenFalse);
	}

	@Test
	public void testDeclareAndAssignOutsideBlock() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String whenTrue = (String) parsed.getMethod("declareAndAssignOutsideBlock", boolean.class).invoke(instance, true);
		assertEquals("true", whenTrue);

		String whenFalse = (String) parsed.getMethod("declareAndAssignOutsideBlock", boolean.class).invoke(instance, false);
		assertEquals("false", whenFalse);
	}

	@Test
	public void testSameNameInMultipleBlocks() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String whenTrue = (String) parsed.getMethod("sameNameInMultipleBlocks", boolean.class).invoke(instance, true);
		assertEquals("truetrue", whenTrue);

		String whenFalse = (String) parsed.getMethod("sameNameInMultipleBlocks", boolean.class).invoke(instance, false);
		assertEquals("falsefalse", whenFalse);
	}

	@Test
	public void testMultipleVariablesInBlock() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String whenTrue = (String) parsed.getMethod("multipleVariablesInBlock", boolean.class).invoke(instance, true);
		assertEquals("3 -> onetwothree", whenTrue);

		String whenFalse = (String) parsed.getMethod("multipleVariablesInBlock", boolean.class).invoke(instance, false);
		assertEquals("nothing", whenFalse);
	}

	@Test
	public void testMultipleNestings() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		String whenTrueTrue = (String) parsed.getMethod("multipleNestings", boolean.class, boolean.class).invoke(instance, true, true);
		assertEquals("true->true", whenTrueTrue);

		String whenTrueFalse = (String) parsed.getMethod("multipleNestings", boolean.class, boolean.class).invoke(instance, true, false);
		assertEquals("true->false", whenTrueFalse);

		String whenFalseTrue = (String) parsed.getMethod("multipleNestings", boolean.class, boolean.class).invoke(instance, false, true);
		assertEquals("false->true", whenFalseTrue);

		String whenFalseFalse = (String) parsed.getMethod("multipleNestings", boolean.class, boolean.class).invoke(instance, false, false);
		assertEquals("false->false", whenFalseFalse);

	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author markus@jevring.net
 */
public class PersonBeanTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testPersonBean() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/PersonBean.java"));
		Constructor<?> fullConstructor = parsed.getConstructor(String.class, int.class);
		Object fullInstance = fullConstructor.newInstance("Markus", 38);

		Field ageField = parsed.getDeclaredField("age");
		assertTrue(Modifier.isPrivate(ageField.getModifiers()));
		assertTrue(Modifier.isTransient(ageField.getModifiers()));
		ageField.setAccessible(true);
		Method ageGetter = parsed.getMethod("getAge");
		assertTrue(Modifier.isPublic(ageGetter.getModifiers()));
		assertTrue(Modifier.isSynchronized(ageGetter.getModifiers()));

		Method ageSetter = parsed.getMethod("setAge", int.class);

		Field nameField = parsed.getDeclaredField("name");
		assertTrue(Modifier.isPrivate(nameField.getModifiers()));
		assertTrue(Modifier.isVolatile(nameField.getModifiers()));
		nameField.setAccessible(true);
		Method nameGetter = parsed.getMethod("getName");
		assertTrue(Modifier.isPublic(nameGetter.getModifiers()));
		assertTrue(Modifier.isFinal(nameGetter.getModifiers()));

		Method nameSetter = parsed.getMethod("setName", String.class);

		assertEquals(38, ageField.get(fullInstance));
		assertEquals(38, ageGetter.invoke(fullInstance));

		assertEquals("Markus", nameField.get(fullInstance));
		assertEquals("Markus", nameGetter.invoke(fullInstance));

		ageSetter.invoke(fullInstance, 49);
		assertEquals(49, ageField.get(fullInstance));
		assertEquals(49, ageGetter.invoke(fullInstance));

		nameSetter.invoke(fullInstance, "Bob");
		assertEquals("Bob", nameField.get(fullInstance));
		assertEquals("Bob", nameGetter.invoke(fullInstance));
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Parses code for an empty class and writes it and loads it and verifies it.
 *
 * @author markus@jevring.net
 */
public class EmptyClassTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testEmptyClass() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/EmptyClass.java"));
		assertEquals(0, parsed.getDeclaredFields().length);
		assertEquals(0, parsed.getDeclaredMethods().length);
		Constructor<?>[] constructors = parsed.getConstructors();
		assertEquals(1, constructors.length);
		assertTrue(Modifier.isPublic(parsed.getModifiers()));
		assertTrue(Modifier.isFinal(parsed.getModifiers()));
		Object o2 = parsed.getDeclaredConstructor().newInstance();
		// since this class is empty, there's nothing we can really assert, so just creating it will have to be enough
	}
}

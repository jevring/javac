/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.Parser;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ParserTest {
	@Test
	public void shouldSuccessfullyParseAllTestFiles() throws Exception {
		testAllFiles(Paths.get("src/test"));
	}

	@Test
	public void shouldSuccessfullyParseAllMainFiles() throws Exception {
		testAllFiles(Paths.get("src/main"));
	}

	private void testAllFiles(Path start) throws IOException {
		// we turn it into a list, rather than call allMatch(), as we want to get all errors
		// out, and not just short-circuit on the first error
		List<Boolean> successes = Files.walk(start)
		                               .filter(path -> path.toFile().isFile())
		                               .filter(path -> path.toFile().getName().endsWith(".java"))
		                               .map(this::parseAndAssertDoesntThrow)
		                               .collect(Collectors.toList());
		boolean couldParseAllFiles = successes.stream().allMatch(v -> v);
		assertTrue(couldParseAllFiles);
	}

	private boolean parseAndAssertDoesntThrow(Path path) {
		try {
			Parser.parseFile(path);
			return true;
		} catch (IOException e) {
			System.out.println("Could not read " + path);
			return false;
		} catch (ParseCancellationException e) {
			//e.printStackTrace();
			System.out.println("Could not parse " + path);
			return false;
		}
	}

	@Test
	public void shouldThrowOnFailure() throws Exception {
		// pom.xml clearly isn't a java file, but it's one that we know will always be present.
		try {
			Parser.parseFile(Paths.get("pom.xml"));
			fail("Should have thrown an exception for an invalid input");
		} catch (Exception e) {
			assertTrue(e instanceof ParseCancellationException);
		}

	}
}

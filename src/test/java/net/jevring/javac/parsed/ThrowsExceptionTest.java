/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.*;
import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.*;

/**
 * @author markus@jevring.net
 */
public class ThrowsExceptionTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;
	private static Object instance;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/ThrowsException.java"));
		//parsed = Class.forName("net.jevring.javac.test.ThrowsExceptionXXX");
		instance = parsed.getConstructor().newInstance();
	}

	@Test
	public void testConstructorThatThrows() throws Exception {
		try {
			parsed.getConstructor(String.class).newInstance("oops");
			fail("Should have thrown an exception");
		} catch (InvocationTargetException e) {
			assertEquals("oops", e.getCause().getMessage());
			assertEquals(Exception.class, e.getCause().getClass());
		}
	}

	@Test
	public void testThrowException() throws Exception {
		Method throwException = parsed.getMethod("throwException", String.class);
		try {
			throwException.invoke(instance, "aaa");
			fail("Should have thrown an exception");
		} catch (InvocationTargetException e) {
			assertEquals("aaa", e.getCause().getMessage());
			assertEquals(MyException.class, e.getCause().getClass());
		}
	}

	@Test
	public void testThrowRuntimeException() throws Exception {
		Method throwException = parsed.getMethod("throwRuntimeException", String.class);
		try {
			throwException.invoke(instance, "bbb");
			fail("Should have thrown an exception");
		} catch (InvocationTargetException e) {
			assertEquals("bbb", e.getCause().getMessage());
			assertEquals(MyRuntimeException.class, e.getCause().getClass());
		}
	}

	@Test
	public void testThrowError() throws Exception {
		Method throwException = parsed.getMethod("throwError", String.class);
		try {
			throwException.invoke(instance, "ccc");
			fail("Should have thrown an exception");
		} catch (InvocationTargetException e) {
			assertEquals("ccc", e.getCause().getMessage());
			assertEquals(MyError.class, e.getCause().getClass());
		}
	}

	@Test
	public void testTryCatch() throws Exception {
		Method tryCatch = parsed.getMethod("tryCatch", String.class);
		assertEquals("hello", tryCatch.invoke(instance, "hello"));
	}

	@Test
	public void testTryCatchContinue() throws Exception {
		Method tryCatchContinue = parsed.getMethod("tryCatchContinue", int.class, String.class);
		assertEquals(StringArrayList.of("before call", "after call", "after everything"), tryCatchContinue.invoke(instance, 0, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyException-abc", "after everything"), tryCatchContinue.invoke(instance, 1, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyOtherException-abc", "after everything"),
		             tryCatchContinue.invoke(instance, 2, "abc"));
	}

	@Test
	public void testTryCatchThrowable() throws Exception {
		Method tryCatchThrowable = parsed.getMethod("tryCatchThrowable", String.class);
		assertEquals("kek", tryCatchThrowable.invoke(instance, "kek"));
	}

	@Test
	public void testTryCatchMultiple() throws Exception {
		Method tryCatchMultiple = parsed.getMethod("tryCatchMultiple", int.class, String.class);
		assertEquals("m-nothing-hiya", tryCatchMultiple.invoke(instance, 0, "hiya"));
		assertEquals("m-MyException-hiya", tryCatchMultiple.invoke(instance, 1, "hiya"));
		assertEquals("m-MyOtherException-hiya", tryCatchMultiple.invoke(instance, 2, "hiya"));
	}

	@Test
	public void testTryCatchPipeSeparated() throws Exception {
		Method tryCatchPipeSeparated = parsed.getMethod("tryCatchPipeSeparated", int.class, String.class);
		assertEquals("ps-nothing-lol", tryCatchPipeSeparated.invoke(instance, 0, "lol"));
		assertEquals("ps-MyException-lol", tryCatchPipeSeparated.invoke(instance, 1, "lol"));
		assertEquals("ps-MyOtherException-lol", tryCatchPipeSeparated.invoke(instance, 2, "lol"));
	}

	@Test
	public void testTryCatchFinally() throws Exception {
		Method tryCatchFinally = parsed.getMethod("tryCatchFinally", int.class, String.class);
		assertEquals(StringArrayList.of("before call", "after call", "in finally"), tryCatchFinally.invoke(instance, 0, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyException-abc", "in finally"), tryCatchFinally.invoke(instance, 1, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyOtherException-abc", "in finally"), tryCatchFinally.invoke(instance, 2, "abc"));
	}

	@Test
	public void testTryCatchFinallyContinue() throws Exception {
		Method tryCatchFinallyContinue = parsed.getMethod("tryCatchFinallyContinue", int.class, String.class);
		assertEquals(StringArrayList.of("before call", "after call", "in finally", "after everything"), tryCatchFinallyContinue.invoke(instance, 0, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyException-abc", "in finally", "after everything"),
		             tryCatchFinallyContinue.invoke(instance, 1, "abc"));
		assertEquals(StringArrayList.of("before call", "caught exception: MyOtherException-abc", "in finally", "after everything"),
		             tryCatchFinallyContinue.invoke(instance, 2, "abc"));
	}

	@Test
	public void testTryLock() throws Exception {
		ReentrantLock lock = new ReentrantLock();
		Method tryLock = parsed.getMethod("tryLock", Lock.class);
		assertEquals("Hello", tryLock.invoke(instance, lock));
		assertFalse("Lock was never unlocked in finally-clause", lock.isLocked());
	}

	@Test
	public void testTryFinallyReturnException() throws Exception {
		ReentrantLock lock = new ReentrantLock();
		Method method = parsed.getMethod("tryFinallyReturnException", Lock.class, boolean.class);
		assertEquals("Hello", method.invoke(instance, lock, false));
		assertEquals("caught exception", method.invoke(instance, lock, true));
		assertFalse("Lock was never unlocked in finally-clause", lock.isLocked());
	}

	@Test
	public void testTryLockMultipleReturns() throws Exception {
		ReentrantLock lock = new ReentrantLock();
		Method method = parsed.getMethod("tryLockMultipleReturns", Lock.class, boolean.class);
		assertEquals(777, method.invoke(instance, lock, false));
		assertEquals(666, method.invoke(instance, lock, true));
		assertFalse("Lock was never unlocked in finally-clause", lock.isLocked());
	}

	@Test
	public void testTryFinallyThrow() throws Exception {
		StringList events = new StringArrayList();
		Method method = parsed.getMethod("tryFinallyThrow", StringList.class);
		try {
			method.invoke(instance, events);
		} catch (InvocationTargetException e) {
			assertEquals("123", e.getCause().getMessage());
			assertTrue(e.getCause() instanceof MyException);
			assertEquals(StringArrayList.of("finally"), events);
		}
	}

	@Test
	public void tesTthrowFromFinally() throws Exception {
		Method method = parsed.getMethod("throwFromFinally");
		try {
			method.invoke(instance);
		} catch (InvocationTargetException e) {
			assertEquals("456", e.getCause().getMessage());
			assertTrue(e.getCause() instanceof MyOtherException);
		}
	}

	@Test
	public void testTryCatchFinallyNested() throws Exception {
		Method method = parsed.getMethod("tryCatchFinallyNested", int.class, String.class);
		assertEquals(StringArrayList.of("before call", "2. before call", "2. after call", "2. in finally", "after call", "in finally", "after everything"),
		             method.invoke(instance, 0, "abc"));
		assertEquals(StringArrayList.of("before call",
		                                "2. before call",
		                                "2. caught exception: MyException-abc",
		                                "2. in finally",
		                                "caught exception: MyOtherException-abc",
		                                "in finally",
		                                "after everything"), method.invoke(instance, 1, "abc"));
		assertEquals(StringArrayList.of("before call",
		                                "2. before call",
		                                "2. caught exception: MyOtherException-abc",
		                                "2. in finally",
		                                "caught exception: MyOtherException-abc",
		                                "in finally",
		                                "after everything"), method.invoke(instance, 2, "abc"));
	}

	@Test
	public void testTryCatchNestedReturn() throws Exception {
		Method method = parsed.getMethod("tryCatchNestedReturn");
		assertEquals(StringArrayList.of("before call", "2. before call", "2. after call", "2. in finally", "after call", "in finally", "after everything"),
		             method.invoke(instance));
	}

	@Test
	public void testTryFinallyNestedReturn() throws Exception {
		StringList events = new StringArrayList();
		Method method = parsed.getMethod("tryFinallyNestedReturn", StringList.class, String.class);
		assertEquals("hello", method.invoke(instance, events, "hello"));
		assertEquals(StringArrayList.of("2. in finally", "in finally"), events);
	}

}

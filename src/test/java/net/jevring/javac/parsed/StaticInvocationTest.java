/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class StaticInvocationTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testStaticInvocation() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/StaticInvocation.java"));

		Method identityString = parsed.getMethod("identity", String.class);
		assertEquals("Hello World", identityString.invoke(null, "Hello World"));

		Method identityInt = parsed.getMethod("identity", int.class);
		assertEquals(15, identityInt.invoke(null, 15));

		Method hello = parsed.getMethod("hello");
		assertEquals("hello", hello.invoke(null));

		Method one = parsed.getMethod("one");
		assertEquals(1, one.invoke(null));

		Method fqnOne = parsed.getMethod("fqnOne");
		assertEquals(1, fqnOne.invoke(null));

	}
}

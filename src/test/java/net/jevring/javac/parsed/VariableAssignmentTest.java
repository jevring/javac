/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.A;
import net.jevring.javac.extras.AssignmentHelper;
import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author markus@jevring.net
 */
public class VariableAssignmentTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/VariableAssignment.java"));
	}

	@Test
	public void testFieldInitializationEmptyConstructor() throws Exception {
		Object instance = parsed.getConstructor().newInstance();
		AssignmentHelper field = (AssignmentHelper) parsed.getField("assignmentHelper").get(instance);
		assertNotNull(field);
		assertEquals("hello", field.prefilledString);
	}

	@Test
	public void testFieldInitializationStringConstructor() throws Exception {
		Object instance = parsed.getConstructor(String.class).newInstance("hello");
		AssignmentHelper field = (AssignmentHelper) parsed.getField("assignmentHelper").get(instance);
		assertNotNull(field);
		assertEquals("hello", field.prefilledString);
	}

	@Test
	public void testFieldInitializationProvidedConstructor() throws Exception {
		AssignmentHelper helper = new AssignmentHelper();
		helper.myString = "are there hats?";
		Object instance = parsed.getConstructor(AssignmentHelper.class).newInstance(helper);
		AssignmentHelper field = (AssignmentHelper) parsed.getField("assignmentHelper").get(instance);
		assertNotNull(field);
		assertEquals("are there hats?", field.myString);
	}

	@Test
	public void testStaticInvocation() throws Exception {
		Object instance = parsed.getConstructor().newInstance();

		parsed.getMethod("staticAssignment").invoke(instance);
		assertEquals(666, AssignmentHelper.myStaticInt);
		assertEquals(777, AssignmentHelper.prefilledStaticInt);
		assertEquals("goodbye", AssignmentHelper.myStaticString);
		assertEquals("pancakes!", AssignmentHelper.prefilledStaticString);

		parsed.getMethod("classVariableFieldAssignment").invoke(instance);
		AssignmentHelper classVariableFieldAssignment = (AssignmentHelper) parsed.getField("assignmentHelper").get(instance);
		assertEquals(111, classVariableFieldAssignment.myInt);
		assertEquals(222, classVariableFieldAssignment.prefilledInt);
		assertEquals("popsicles", classVariableFieldAssignment.myString);
		assertEquals("bananas", classVariableFieldAssignment.prefilledString);

		AssignmentHelper localVariableFieldAssignment = (AssignmentHelper) parsed.getMethod("localVariableFieldAssignment").invoke(instance);
		assertEquals(333, localVariableFieldAssignment.myInt);
		assertEquals(444, localVariableFieldAssignment.prefilledInt);
		assertEquals("monkeys", localVariableFieldAssignment.myString);
		assertEquals("a glass of water", localVariableFieldAssignment.prefilledString);

		A a = (A) parsed.getMethod("interleavedGettersAndStaticFields").invoke(instance);
		assertEquals("candy", a.b.getC().d.writableString);

		A allFieldsA = (A) parsed.getMethod("allFields", String.class).invoke(instance, "some string");
		assertEquals("some string", allFieldsA.nonStaticB.nonStaticC.nonStaticD.writableString);

		parsed.getMethod("allStaticFields").invoke(instance);
		assertEquals("candy is delicious", A.b.c.d.writableString);
	}

}

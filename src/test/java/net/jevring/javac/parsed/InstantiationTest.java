/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.IntContainer;
import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author markus@jevring.net
 */
public class InstantiationTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testInstantiation() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/Instantiation.java"));
		Constructor<?> constructor = parsed.getConstructor();
		Object instance = constructor.newInstance();

		Method getDynamic = parsed.getMethod("getDynamic");
		assertTrue(Modifier.isPublic(getDynamic.getModifiers()));
		assertEquals(3, getDynamic.invoke(instance));

		Method getConst = parsed.getMethod("getConst");
		assertTrue(Modifier.isPublic(getConst.getModifiers()));
		assertEquals(1, getConst.invoke(instance));

		Method getSet = parsed.getMethod("getSet");
		assertTrue(Modifier.isPublic(getSet.getModifiers()));
		IntContainer set = (IntContainer) getSet.invoke(instance);
		assertTrue(set.contains(15));


		Method newIntContainer = parsed.getMethod("newIntContainer");
		assertTrue(Modifier.isPublic(newIntContainer.getModifiers()));
		IntContainer nic = (IntContainer) newIntContainer.invoke(instance);
		assertEquals(0, nic.size());
	}
}

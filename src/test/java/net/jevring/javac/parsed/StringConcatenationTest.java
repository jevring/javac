/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.input.ClassPath;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class StringConcatenationTest {
	private final net.jevring.javac.input.Compiler compiler = new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());

	@Test
	public void testStringConcatenation() throws Exception {
		Class<?> parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/StringConcatenation.java"));

		Method stringAndString = parsed.getMethod("add", String.class, String.class);
		assertEquals("Hello World", stringAndString.invoke(null, "Hello", " World"));

		Method objectAndString = parsed.getMethod("add", Object.class, String.class);
		assertEquals("Hello World", objectAndString.invoke(null, "Hello", " World"));
		assertEquals("[] World", objectAndString.invoke(null, new ArrayList(), " World"));

		Method stringAndObject = parsed.getMethod("add", String.class, Object.class);
		assertEquals("Hello World", stringAndObject.invoke(null, "Hello", " World"));
		assertEquals("Hello[]", stringAndObject.invoke(null, "Hello", new ArrayList()));

		Method stringAndInt = parsed.getMethod("add", String.class, int.class);
		assertEquals("Hello5", stringAndInt.invoke(null, "Hello", 5));

		Method doubleAndString = parsed.getMethod("add", Object.class, String.class);
		assertEquals("1.2 World", doubleAndString.invoke(null, 1.2, " World"));

		Method stringAndStringAndString = parsed.getMethod("add", String.class, String.class, String.class);
		assertEquals("Hello World!", stringAndStringAndString.invoke(null, "Hello", " World", "!"));

		Method addInline = parsed.getMethod("addInline", String.class, String.class);
		assertEquals("Hello World", addInline.invoke(null, "Hello", " World"));

		Method length = parsed.getMethod("length", String.class, String.class);
		assertEquals(29, length.invoke(null, "Hello", " World"));

		Method nestedLength = parsed.getMethod("nestedLength", String.class, String.class);
		assertEquals(29, nestedLength.invoke(null, "Hello", " World"));
	}
}

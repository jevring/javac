/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.parsed;

import net.jevring.javac.extras.HashIntSet;
import net.jevring.javac.extras.IntSet;
import net.jevring.javac.input.ClassPath;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author markus@jevring.net
 */
public class ConcreteImplementationToInterfaceTest {
	private static final net.jevring.javac.input.Compiler compiler =
			new net.jevring.javac.input.Compiler(Paths.get("target/generated-classes"), new ClassPath());
	private static Class<?> parsed;

	@BeforeClass
	public static void setUp() throws Exception {
		parsed = compiler.compile(Paths.get("src/test/resources/net/jevring/javac/examples/ConcreteImplementationToInterface.java"));
		parsed.getConstructors(); // trigger verification
	}

	@Test
	public void testProvidedIntSet() throws Exception {
		IntSet set = new HashIntSet();
		Object instance = parsed.getConstructor(IntSet.class).newInstance(set);
		Method addToField = parsed.getMethod("addToField");

		IntSet expectedIntSet = new HashIntSet();
		expectedIntSet.add(5);
		expectedIntSet.add(6);
		assertEquals(expectedIntSet, addToField.invoke(instance));
	}

	@Test
	public void testNullIntSet() throws Exception {
		Object instance = parsed.getConstructor(IntSet.class).newInstance(new Object[]{null});
		Method assignField = parsed.getMethod("assignField");

		IntSet expectedIntSet = new HashIntSet();
		expectedIntSet.add(1);
		expectedIntSet.add(2);
		assertEquals(expectedIntSet, assignField.invoke(instance));

		Method createNew = parsed.getMethod("createNew");

		IntSet expectedSet2 = new HashIntSet();
		expectedSet2.add(3);
		expectedSet2.add(4);
		assertEquals(expectedSet2, createNew.invoke(instance));

	}
}

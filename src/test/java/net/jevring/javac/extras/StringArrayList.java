/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.extras;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This class, just like IntSet etc is only until I implement generics and polymorphism.
 * As a result, it looks bad, but since it's temporary, I don't care.
 *
 * @author markus@jevring.net
 */
public class StringArrayList implements StringList {
	private final List<String> list = new ArrayList<>();

	@Override
	public void add(String s) {
		list.add(s);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof StringArrayList)) {
			return false;
		}
		StringArrayList that = (StringArrayList) o;
		return Objects.equals(list, that.list);
	}

	@Override
	public int hashCode() {
		return Objects.hash(list);
	}

	public static StringArrayList of(String... strings) {
		StringArrayList stringArrayList = new StringArrayList();
		Collections.addAll(stringArrayList.list, strings);
		return stringArrayList;
	}

	public static StringArrayList of(String string) {
		StringArrayList stringArrayList = new StringArrayList();
		Collections.addAll(stringArrayList.list, string);
		return stringArrayList;
	}

	@Override
	public String toString() {
		return "StringArrayList{" + "list=" + list + '}';
	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ClassWithOnlyFieldsTest {
	private static final AccessFlags PUBLIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_FINAL);
	private static final AccessFlags PRIVATE = new AccessFlags(Flag.ACC_PRIVATE);
	private static final AccessFlags VOLATILE = new AccessFlags(Flag.ACC_VOLATILE);
	private static final AccessFlags TRANSIENT = new AccessFlags(Flag.ACC_TRANSIENT);


	@Test
	public void testClassGeneration() throws Exception {
		Map<Class<?>, String> primitives =
				Map.of(int.class, "I", float.class, "F", short.class, "S", double.class, "D", long.class, "J", byte.class, "B", boolean.class, "Z");

		Map<Class<?>, String> objects = Map.of(String.class,
		                                       "Ljava/lang/String;",
		                                       Short.class,
		                                       "Ljava/lang/Short;",
		                                       Float.class,
		                                       "Ljava/lang/Float;",
		                                       Double.class,
		                                       "Ljava/lang/Double;",
		                                       Long.class,
		                                       "Ljava/lang/Long;",
		                                       Byte.class,
		                                       "Ljava/lang/Byte;",
		                                       Boolean.class,
		                                       "Ljava/lang/Boolean;");


		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream("target/generated-classes/direct/net/jevring/javac/Fields.class")) {
			ConstantPool constantPool = new ConstantPool();
			FieldsGenerator fieldsGenerator = new FieldsGenerator(constantPool);

			short thisClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Fields")));
			short superClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));


			for (Map.Entry<Class<?>, String> entry : primitives.entrySet()) {
				createFields(fieldsGenerator, entry);
			}
			for (Map.Entry<Class<?>, String> entry : objects.entrySet()) {
				createFields(fieldsGenerator, entry);
			}


			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_FINAL)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(Collections.emptyList())
			                               .fields(fieldsGenerator.generate())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoConstantPoolIndex)
			                               .superClass(superClassClassInfoConstantPoolIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> clazz = urlClassLoader.loadClass("net.jevring.javac.Fields");
		for (Map.Entry<Class<?>, String> entry : primitives.entrySet()) {
			assertCorrectEntry(clazz, entry);
		}
		for (Map.Entry<Class<?>, String> entry : objects.entrySet()) {
			assertCorrectEntry(clazz, entry);
		}
		Field[] fields = clazz.getFields();
		assertEquals(14, fields.length);

		Field[] declaredFields = clazz.getDeclaredFields();
		assertEquals(56, declaredFields.length);
	}

	private void createFields(FieldsGenerator fieldsGenerator, Map.Entry<Class<?>, String> entry) {
		String simpleName = entry.getKey().getSimpleName();
		String descriptor = entry.getValue();
		fieldsGenerator.addField(PUBLIC_FINAL, descriptor, "my" + simpleName);
		fieldsGenerator.addField(PRIVATE, descriptor, "myPrivate" + simpleName);
		fieldsGenerator.addField(VOLATILE, descriptor, "myVolatile" + simpleName);
		fieldsGenerator.addField(TRANSIENT, descriptor, "myTransient" + simpleName);
	}

	private void assertCorrectEntry(Class<?> clazz, Map.Entry<Class<?>, String> entry) throws NoSuchFieldException {
		String simpleName = entry.getKey().getSimpleName();

		Field publicField = clazz.getField("my" + simpleName);
		assertEquals(entry.getKey(), publicField.getType());
		assertTrue(Modifier.isPublic(publicField.getModifiers()));
		assertTrue(Modifier.isFinal(publicField.getModifiers()));

		Field privateField = clazz.getDeclaredField("myPrivate" + simpleName);
		assertEquals(entry.getKey(), privateField.getType());
		assertTrue(Modifier.isPrivate(privateField.getModifiers()));

		Field volatileField = clazz.getDeclaredField("myVolatile" + simpleName);
		assertEquals(entry.getKey(), volatileField.getType());
		assertTrue(Modifier.isVolatile(volatileField.getModifiers()));

		Field transientField = clazz.getDeclaredField("myTransient" + simpleName);
		assertEquals(entry.getKey(), transientField.getType());
		assertTrue(Modifier.isTransient(transientField.getModifiers()));


	}
}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class InterfaceTest {
	private static final AccessFlags PUBLIC_INTERFACE = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_INTERFACE, Flag.ACC_ABSTRACT);
	private static final AccessFlags PUBLIC = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_ABSTRACT);

	@Test
	public void testClassGeneration() throws Exception {
		Map<Class<?>, String> primitives =
				Map.of(int.class, "I", float.class, "F", short.class, "S", double.class, "D", long.class, "J", byte.class, "B", boolean.class, "Z");

		Map<Class<?>, String> objects = Map.of(String.class,
		                                       "Ljava/lang/String;",
		                                       Short.class,
		                                       "Ljava/lang/Short;",
		                                       Float.class,
		                                       "Ljava/lang/Float;",
		                                       Double.class,
		                                       "Ljava/lang/Double;",
		                                       Long.class,
		                                       "Ljava/lang/Long;",
		                                       Byte.class,
		                                       "Ljava/lang/Byte;",
		                                       Boolean.class,
		                                       "Ljava/lang/Boolean;");

		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream("target/generated-classes/direct/net/jevring/javac/PublicInterface.class")) {
			ConstantPool constantPool = new ConstantPool();
			ClassFileGenerator classFileGenerator = new ClassFileGenerator(constantPool, null);
			classFileGenerator.setFullyQualifiedName("net.jevring.javac.PublicInterface");
			MethodsGenerator methodsGenerator = classFileGenerator.getMethodsGenerator();


			short classNameInfoConstantPoolIndex = constantPool.utf8("net/jevring/javac/PublicInterface");
			short thisClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(classNameInfoConstantPoolIndex));

			short javaLangObjectNameIndex = constantPool.utf8("java/lang/Object");
			short superClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(javaLangObjectNameIndex));

			for (Map.Entry<Class<?>, String> entry : primitives.entrySet()) {
				createMethod(methodsGenerator, entry);
			}
			for (Map.Entry<Class<?>, String> entry : objects.entrySet()) {
				createMethod(methodsGenerator, entry);
			}

			methodsGenerator.addMethod(PUBLIC, MethodsGenerator.EMPTY_CONSTRUCTOR_SIGNATURE, "myVoid");

			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_INTERFACE)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(methodsGenerator.generate())
			                               .fields(Collections.emptyList())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoConstantPoolIndex)
			                               .superClass(superClassClassInfoConstantPoolIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> publicInterface = urlClassLoader.loadClass("net.jevring.javac.PublicInterface");

		for (Map.Entry<Class<?>, String> entry : primitives.entrySet()) {
			assertCorrectEntry(publicInterface, entry);
		}
		for (Map.Entry<Class<?>, String> entry : objects.entrySet()) {
			assertCorrectEntry(publicInterface, entry);
		}

		Method method = publicInterface.getMethod("myVoid");
		assertTrue(Modifier.isPublic(method.getModifiers()));
		assertEquals(void.class, method.getReturnType());

		Method[] methods = publicInterface.getMethods();
		assertEquals(15, methods.length);
	}

	private void assertCorrectEntry(Class<?> publicInterface, Map.Entry<Class<?>, String> entry) throws NoSuchMethodException {
		Method method = publicInterface.getMethod("my" + entry.getKey().getSimpleName(), entry.getKey());
		assertTrue(Modifier.isPublic(method.getModifiers()));
		assertEquals(entry.getKey(), method.getReturnType());
		assertEquals(1, method.getParameterTypes().length);
		assertEquals(entry.getKey(), method.getParameterTypes()[0]);

	}

	private void createMethod(MethodsGenerator methodsGenerator, Map.Entry<Class<?>, String> entry) {
		String type = entry.getValue();
		methodsGenerator.addMethod(PUBLIC, String.format("(%s)%s", type, type), "my" + entry.getKey().getSimpleName());
	}
}

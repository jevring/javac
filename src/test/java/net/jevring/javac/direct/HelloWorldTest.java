/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.*;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;

import static org.junit.Assert.assertTrue;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class HelloWorldTest {
	private static final AccessFlags PUBLIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_FINAL);
	private static final AccessFlags PUBLIC_STATIC = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_STATIC);

	@Test
	public void testClassGeneration() throws Exception {
		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream("target/generated-classes/direct/net/jevring/javac/HelloWorld.class")) {
			ConstantPool constantPool = new ConstantPool();
			ClassFileGenerator classFileGenerator = new ClassFileGenerator(constantPool, null);
			classFileGenerator.setFullyQualifiedName("net.jevring.javac.HelloWorld");
			FieldsGenerator fieldsGenerator = classFileGenerator.getFieldsGenerator();
			MethodsGenerator methodsGenerator = classFileGenerator.getMethodsGenerator();

			// todo: maybe in the future we can streamline this, if we create methods in the ConstantPool for each one of these
			short outUtf8Index = constantPool.utf8("out");
			short printStreamReferenceIndex = constantPool.utf8("Ljava/io/PrintStream;");
			short systemOutNameAndTypeIndex = constantPool.add(new NameAndTypeInfo(outUtf8Index, printStreamReferenceIndex));
			short javaLangSystemClassIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/System")));
			short fieldRefToSystemOut = constantPool.add(new FieldrefInfo(javaLangSystemClassIndex, systemOutNameAndTypeIndex));

			short thisClassClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/HelloWorld")));
			short superClassClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));
			classFileGenerator.setSuperClassIndex(superClassClassInfoIndex);
			classFileGenerator.setThisClassIndex(thisClassClassInfoIndex);

			short helloWorldStringIndex = constantPool.add(new StringInfo(constantPool.utf8("Hello world!")));


			short javaIoPrintStreamClassIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/io/PrintStream")));
			short printlnNameIndex = constantPool.utf8("println");
			short printlnSignatureIndex = constantPool.utf8("(Ljava/lang/String;)V");
			short printlnNameAndSignatureIndex = constantPool.add(new NameAndTypeInfo(printlnNameIndex, printlnSignatureIndex));
			short methodRefToPrintStreamPrintln = constantPool.add(new MethodrefInfo(javaIoPrintStreamClassIndex, printlnNameAndSignatureIndex));

			CodeGenerator codeGenerator = new CodeGenerator(constantPool);
			codeGenerator.add(ByteCode.GETSTATIC, fieldRefToSystemOut);
			codeGenerator.add(ByteCode.LDC, (byte) helloWorldStringIndex);
			codeGenerator.add(ByteCode.INVOKEVIRTUAL, methodRefToPrintStreamPrintln);
			codeGenerator.add(ByteCode.RETURN);

			codeGenerator.variables().addMethodVariable(ClassProxy.STRING_ARRAY, "args");
			methodsGenerator.addMethod(PUBLIC_STATIC, "([Ljava/lang/String;)V", "main").addAttribute(codeGenerator.generate());

			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_FINAL)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(methodsGenerator.generate())
			                               .fields(fieldsGenerator.generate())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoIndex)
			                               .superClass(superClassClassInfoIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> helloWorld = urlClassLoader.loadClass("net.jevring.javac.HelloWorld");
		//System.out.println("helloWorld = " + helloWorld);
		// blerg, had to cast this via Object for the invocation to work...
		helloWorld.getMethod("main", String[].class).invoke(null, (Object) new String[0]);

		//$ java -cp target/generated-classes net.jevring.javac.HelloWorld

		// todo: what can we even assert on here?
	}
}

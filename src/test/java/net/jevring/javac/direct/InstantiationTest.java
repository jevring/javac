/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariable;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.*;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class InstantiationTest {
	private static final AccessFlags PRIVATE_STATIC_FINAL = new AccessFlags(Flag.ACC_PRIVATE, Flag.ACC_STATIC, Flag.ACC_FINAL);
	private static final AccessFlags PUBLIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_FINAL);
	private static final AccessFlags PUBLIC = new AccessFlags(Flag.ACC_PUBLIC);
	private static final AccessFlags STATIC = new AccessFlags(Flag.ACC_STATIC);


	@Test
	public void testClassGeneration() throws Exception {
		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream(new File(outputDirectory, "Instantiation.class"))) {
			ConstantPool constantPool = new ConstantPool();
			ClassFileGenerator classFileGenerator = new ClassFileGenerator(constantPool, null);
			classFileGenerator.setFullyQualifiedName("net.jevring.javac.Instantiation");
			FieldsGenerator fieldsGenerator = classFileGenerator.getFieldsGenerator();
			MethodsGenerator methodsGenerator = classFileGenerator.getMethodsGenerator();


			short thisClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Instantiation")));
			short superClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));
			short simpleValueClassReference = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/direct/SimpleValue")));

			// todo: reference this, and then perhaps initialize it in a static block?
			fieldsGenerator.addField(PRIVATE_STATIC_FINAL, "Lnet/jevring/javac/direct/SimpleValue;", "CONST", Collections.emptyList());

			// unless otherwise defined, all classes need an empty public constructor by default. apparently we have to add this
			methodsGenerator.addPublicEmptyConstructor("java/lang/Object", ClassProxy.forClass(classFileGenerator));
			methodsGenerator.addMethod(PUBLIC, "()Ljava/lang/String;", "getNew").addAttribute(getNewCode(constantPool, simpleValueClassReference,
			                                                                                             classFileGenerator));
			short constFieldIndex = constantPool.add(new FieldrefInfo(thisClassClassInfoConstantPoolIndex,
			                                                          constantPool.add(new NameAndTypeInfo(constantPool.utf8("CONST"),
			                                                                                               constantPool.utf8(
					                                                                                               "Lnet/jevring/javac/direct/SimpleValue;")))));
			methodsGenerator.addMethod(PUBLIC, "()Ljava/lang/String;", "getConst")
			                .addAttribute(getConstCode(constantPool, simpleValueClassReference, constFieldIndex, classFileGenerator));
			methodsGenerator.addMethod(STATIC, MethodsGenerator.EMPTY_CONSTRUCTOR_SIGNATURE, MethodsGenerator.CLASS_INITIALIZER_NAME)
			                .addAttribute(staticInitializer(constantPool, simpleValueClassReference, constFieldIndex));

			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_FINAL)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(methodsGenerator.generate())
			                               .fields(fieldsGenerator.generate())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoConstantPoolIndex)
			                               .superClass(superClassClassInfoConstantPoolIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> clazz = urlClassLoader.loadClass("net.jevring.javac.Instantiation");
		Constructor<?> constructor = clazz.getConstructor();
		Object instance = constructor.newInstance();

		Method getNew = clazz.getMethod("getNew");
		Object getNewResult = getNew.invoke(instance);
		assertEquals("Mike", getNewResult);
		Method getConst = clazz.getMethod("getConst");
		Object getConstResult = getConst.invoke(instance);
		assertEquals("Bob", getConstResult);
	}

	private AttributeInfo staticInitializer(ConstantPool constantPool, short simpleValueClassReference, short constFieldIndex) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);

		newSimpleValue(constantPool, simpleValueClassReference, codeGenerator, "Bob", 20);

		codeGenerator.add(ByteCode.PUTSTATIC, constFieldIndex);
		codeGenerator.add(ByteCode.RETURN);


		return codeGenerator.generate();
	}

	private AttributeInfo getConstCode(ConstantPool constantPool, short simpleValueClassReference, short constField, ClassFileGenerator classFileGenerator) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);

		codeGenerator.variables().addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");

		//return CONST.getName();
		codeGenerator.add(ByteCode.GETSTATIC, constField);
		codeGenerator.add(ByteCode.INVOKEVIRTUAL,
		                  constantPool.add(new MethodrefInfo(simpleValueClassReference,
		                                                     constantPool.add(new NameAndTypeInfo(constantPool.utf8("getName"),
		                                                                                          constantPool.utf8("()Ljava/lang/String;"))))));
		codeGenerator.add(ByteCode.ARETURN);

		return codeGenerator.generate();
	}

	private AttributeInfo getNewCode(ConstantPool constantPool, short simpleValueClassReference, ClassFileGenerator classFileGenerator) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);

		codeGenerator.variables().addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");
		newSimpleValue(constantPool, simpleValueClassReference, codeGenerator, "Mike", 25);


		LocalVariable simpleValueVariableIndex = codeGenerator.variables().addLocalVariable(ClassProxy.forClass(SimpleValue.class), "simpleValue");
		codeGenerator.add(ByteCode.ASTORE_1); // we know that the variable index is 1. This is an optimization. We could have done an ALOAD for the index, had it been over one of the predefined ones.

		//return simpleValue.getName();
		short simpleValueLoadIndex = codeGenerator.add(ByteCode.ALOAD_1);
		simpleValueVariableIndex.startScope(simpleValueLoadIndex);
		codeGenerator.add(ByteCode.INVOKEVIRTUAL,
		                  constantPool.add(new MethodrefInfo(simpleValueClassReference,
		                                                     constantPool.add(new NameAndTypeInfo(constantPool.utf8("getName"),
		                                                                                          constantPool.utf8("()Ljava/lang/String;"))))));
		codeGenerator.add(ByteCode.ARETURN);

		return codeGenerator.generate();
	}

	private void newSimpleValue(ConstantPool constantPool, short simpleValueClassReference, CodeGenerator codeGenerator, String name, int age) {
		//new SimpleValue("Mike", 25);
		codeGenerator.add(ByteCode.NEW, simpleValueClassReference);
		codeGenerator.add(ByteCode.DUP);  // we need to duplicate the value here, since the invocation of the init is void, and won't put the object back on the stack 
		codeGenerator.add(ByteCode.LDC, (byte) constantPool.add(new StringInfo(constantPool.utf8(name))));
		codeGenerator.add(ByteCode.LDC, (byte) constantPool.add(new IntegerInfo(age))); // normal javac uses bipush here
		codeGenerator.add(ByteCode.INVOKESPECIAL,
		                  constantPool.add(new MethodrefInfo(simpleValueClassReference,
		                                                     constantPool.add(new NameAndTypeInfo(constantPool.utf8(MethodsGenerator.CONSTRUCTOR_NAME),
		                                                                                          constantPool.utf8("(Ljava/lang/String;I)V"))))));
	}


}

/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.helpers.Descriptor;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import net.jevring.javac.output.structure.constantpool.FieldrefInfo;
import net.jevring.javac.output.structure.constantpool.MethodrefInfo;
import net.jevring.javac.output.structure.constantpool.NameAndTypeInfo;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests a class with an empty constructor, a "full" constructor, and public getters and setters for the private fields
 *
 * @author markus@jevring.net
 */
public class BeanTest {
	private static final AccessFlags PUBLIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_FINAL);
	private static final AccessFlags PRIVATE = new AccessFlags(Flag.ACC_PRIVATE);
	private static final AccessFlags PUBLIC = new AccessFlags(Flag.ACC_PUBLIC);


	@Test
	public void testClassGeneration() throws Exception {
		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream("target/generated-classes/direct/net/jevring/javac/Bean.class")) {
			ConstantPool constantPool = new ConstantPool();
			ClassFileGenerator classFileGenerator = new ClassFileGenerator(constantPool, null);
			classFileGenerator.setFullyQualifiedName("net.jevring.javac.Bean");

			FieldsGenerator fieldsGenerator = classFileGenerator.getFieldsGenerator();
			MethodsGenerator methodsGenerator = classFileGenerator.getMethodsGenerator();


			short thisClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Bean")));
			short superClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));
			classFileGenerator.setSuperClassIndex(superClassClassInfoConstantPoolIndex);
			classFileGenerator.setThisClassIndex(thisClassClassInfoConstantPoolIndex);

			fieldsGenerator.addField(PRIVATE, "I", "age");
			fieldsGenerator.addField(PRIVATE, "Ljava/lang/String;", "name");

			methodsGenerator.addMethod(PUBLIC, MethodsGenerator.EMPTY_CONSTRUCTOR_SIGNATURE, MethodsGenerator.CONSTRUCTOR_NAME)
			                .addAttribute(ctor(constantPool, false, classFileGenerator));

			methodsGenerator.addMethod(PUBLIC, "(ILjava/lang/String;)V", MethodsGenerator.CONSTRUCTOR_NAME)
			                .addAttribute(ctor(constantPool, true, classFileGenerator));

			methodsGenerator.addMethod(PUBLIC, "(I)V", "setAge").addAttribute(setter(constantPool, "age", ClassProxy.INT, classFileGenerator));
			methodsGenerator.addMethod(PUBLIC, "(Ljava/lang/String;)V", "setName")
			                .addAttribute(setter(constantPool, "name", ClassProxy.STRING, classFileGenerator));

			methodsGenerator.addMethod(PUBLIC, "()I", "getAge").addAttribute(getter(constantPool, "age", "I", classFileGenerator));
			methodsGenerator.addMethod(PUBLIC, "()Ljava/lang/String;", "getName")
			                .addAttribute(getter(constantPool, "name", "Ljava/lang/String;", classFileGenerator));


			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_FINAL)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(methodsGenerator.generate())
			                               .fields(fieldsGenerator.generate())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoConstantPoolIndex)
			                               .superClass(superClassClassInfoConstantPoolIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> clazz = urlClassLoader.loadClass("net.jevring.javac.Bean");
		Constructor<?> fullConstructor = clazz.getConstructor(int.class, String.class);
		Object fullInstance = fullConstructor.newInstance(38, "Markus");

		Field ageField = clazz.getDeclaredField("age");
		assertTrue(Modifier.isPrivate(ageField.getModifiers()));
		ageField.setAccessible(true);
		Method ageGetter = clazz.getMethod("getAge");
		Method ageSetter = clazz.getMethod("setAge", int.class);

		Field nameField = clazz.getDeclaredField("name");
		assertTrue(Modifier.isPrivate(nameField.getModifiers()));
		nameField.setAccessible(true);
		Method nameGetter = clazz.getMethod("getName");
		Method nameSetter = clazz.getMethod("setName", String.class);

		assertEquals(38, ageField.get(fullInstance));
		assertEquals(38, ageGetter.invoke(fullInstance));

		assertEquals("Markus", nameField.get(fullInstance));
		assertEquals("Markus", nameGetter.invoke(fullInstance));

		ageSetter.invoke(fullInstance, 49);
		assertEquals(49, ageField.get(fullInstance));
		assertEquals(49, ageGetter.invoke(fullInstance));

		nameSetter.invoke(fullInstance, "Bob");
		assertEquals("Bob", nameField.get(fullInstance));
		assertEquals("Bob", nameGetter.invoke(fullInstance));
	}

	private AttributeInfo setter(ConstantPool constantPool, String field, ClassProxy type, ClassFileGenerator classFileGenerator) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);

		short beanClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Bean")));
		short fieldrefIndex = constantPool.add(new FieldrefInfo(beanClassInfoIndex,
		                                                        constantPool.add(new NameAndTypeInfo(constantPool.utf8(field),
		                                                                                             constantPool.utf8(Descriptor.of(type))))));
		codeGenerator.variables().addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");
		codeGenerator.variables().addMethodVariable(type, field);

		codeGenerator.add(ByteCode.ALOAD_0); // load "this"
		if (ClassProxy.INT.equals(type)) {
			codeGenerator.add(ByteCode.ILOAD_1); // load the parameter
		} else {
			codeGenerator.add(ByteCode.ALOAD_1); // load the parameter
		}
		codeGenerator.add(ByteCode.PUTFIELD, fieldrefIndex); // push the variable into the field
		codeGenerator.add(ByteCode.RETURN);

		return codeGenerator.generate();
	}

	private AttributeInfo getter(ConstantPool constantPool, String field, String descriptor, ClassFileGenerator classFileGenerator) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);
		codeGenerator.variables().addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");

		short beanClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Bean")));
		short fieldrefIndex = constantPool.add(new FieldrefInfo(beanClassInfoIndex,
		                                                        constantPool.add(new NameAndTypeInfo(constantPool.utf8(field),
		                                                                                             constantPool.utf8(descriptor)))));
		codeGenerator.add(ByteCode.ALOAD_0); // load "this"
		codeGenerator.add(ByteCode.GETFIELD, fieldrefIndex); // push the variable into the field
		if ("I".equals(descriptor)) {
			codeGenerator.add(ByteCode.IRETURN);
		} else {
			codeGenerator.add(ByteCode.ARETURN);
		}
		return codeGenerator.generate();
	}


	private AttributeInfo ctor(ConstantPool constantPool, boolean initializeFields, ClassFileGenerator classFileGenerator) {
		short javaLangObjectClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));
		short javaLangObjectConstructorIndex = constantPool.add(new NameAndTypeInfo(constantPool.utf8("<init>"), constantPool.utf8("()V")));
		short methodRefToInit = constantPool.add(new MethodrefInfo(javaLangObjectClassInfoIndex, javaLangObjectConstructorIndex));

		CodeGenerator codeGenerator = new CodeGenerator(constantPool);
		LocalVariableTableGenerator localVariables = codeGenerator.variables();
		localVariables.addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");

		codeGenerator.add(ByteCode.ALOAD_0); // load "this"
		codeGenerator.add(ByteCode.INVOKESPECIAL, methodRefToInit); // call to parent constructor
		if (initializeFields) {
			localVariables.addMethodVariable(ClassProxy.INT, "age");
			localVariables.addMethodVariable(ClassProxy.STRING, "name");

			short beanClassInfoIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Bean")));
			short ageFieldrefIndex = constantPool.add(new FieldrefInfo(beanClassInfoIndex,
			                                                           constantPool.add(new NameAndTypeInfo(constantPool.utf8("age"),
			                                                                                                constantPool.utf8("I")))));
			short nameFieldrefIndex = constantPool.add(new FieldrefInfo(beanClassInfoIndex,
			                                                            constantPool.add(new NameAndTypeInfo(constantPool.utf8("name"),
			                                                                                                 constantPool.utf8("Ljava/lang/String;")))));


			codeGenerator.add(ByteCode.ALOAD_0); // load "this" as the object reference onto which to push this object 
			codeGenerator.add(ByteCode.ILOAD_1);
			codeGenerator.add(ByteCode.PUTFIELD, ageFieldrefIndex);

			codeGenerator.add(ByteCode.ALOAD_0); // load "this" as the object reference onto which to push this object
			codeGenerator.add(ByteCode.ALOAD_2);
			codeGenerator.add(ByteCode.PUTFIELD, nameFieldrefIndex);
		}
		codeGenerator.add(ByteCode.RETURN);

		return codeGenerator.generate();
	}
}

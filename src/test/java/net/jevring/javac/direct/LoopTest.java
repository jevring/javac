/*
 * Copyright 2017 Markus Jevring
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.jevring.javac.direct;

import net.jevring.javac.generators.StackMapTableGenerator;
import net.jevring.javac.generators.code.CodeGenerator;
import net.jevring.javac.generators.code.PendingShortJumpOffset;
import net.jevring.javac.generators.fields.FieldsGenerator;
import net.jevring.javac.generators.methods.MethodsGenerator;
import net.jevring.javac.generators.variables.LocalVariable;
import net.jevring.javac.generators.variables.LocalVariableTableGenerator;
import net.jevring.javac.input.intermediate.ClassFileGenerator;
import net.jevring.javac.input.proxies.ClassProxy;
import net.jevring.javac.output.ClassFileWriter;
import net.jevring.javac.output.structure.AttributeInfo;
import net.jevring.javac.output.structure.ByteCode;
import net.jevring.javac.output.structure.ClassFile;
import net.jevring.javac.output.structure.ConstantPool;
import net.jevring.javac.output.structure.accessflags.AccessFlags;
import net.jevring.javac.output.structure.accessflags.Flag;
import net.jevring.javac.output.structure.attributes.ConstantValue;
import net.jevring.javac.output.structure.attributes.stackmapframe.ChopFrame;
import net.jevring.javac.output.structure.attributes.stackmapframe.FullFrame;
import net.jevring.javac.output.structure.attributes.stackmapframe.variableinfo.DoubleVariableInfo;
import net.jevring.javac.output.structure.attributes.stackmapframe.variableinfo.IntegerVariableInfo;
import net.jevring.javac.output.structure.attributes.stackmapframe.variableinfo.ObjectVariableInfo;
import net.jevring.javac.output.structure.attributes.stackmapframe.variableinfo.VerificationTypeInfo;
import net.jevring.javac.output.structure.constantpool.ClassInfo;
import net.jevring.javac.output.structure.constantpool.DoubleInfo;
import net.jevring.javac.output.structure.constantpool.IntegerInfo;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests a class with an empty constructor, a "full" constructor, and public getters and setters for the private fields
 *
 * @author markus@jevring.net
 */
public class LoopTest {
	private static final AccessFlags PUBLIC_STATIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_STATIC, Flag.ACC_FINAL);
	private static final AccessFlags PUBLIC_FINAL = new AccessFlags(Flag.ACC_PUBLIC, Flag.ACC_FINAL);
	private static final AccessFlags PUBLIC = new AccessFlags(Flag.ACC_PUBLIC);


	@Test
	public void testClassGeneration() throws Exception {
		File outputDirectory = new File("target/generated-classes/direct/net/jevring/javac/");
		if (!outputDirectory.exists()) {
			assertTrue(outputDirectory.mkdirs());
		}
		try (OutputStream out = new FileOutputStream(new File(outputDirectory, "Looper.class"))) {
			ConstantPool constantPool = new ConstantPool();
			ClassFileGenerator classFileGenerator = new ClassFileGenerator(constantPool, null);
			classFileGenerator.setFullyQualifiedName("net.jevring.javac.Looper");
			FieldsGenerator fieldsGenerator = classFileGenerator.getFieldsGenerator();
			MethodsGenerator methodsGenerator = classFileGenerator.getMethodsGenerator();


			short thisClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Looper")));
			short superClassClassInfoConstantPoolIndex = constantPool.add(new ClassInfo(constantPool.utf8("java/lang/Object")));

			fieldsGenerator.addField(PUBLIC_STATIC_FINAL,
			                         "I",
			                         "CONST",
			                         Collections.singletonList(new AttributeInfo(constantPool.utf8("ConstantValue"),
			                                                                     new ConstantValue(constantPool.add(new IntegerInfo(99))))));

			fieldsGenerator.addField(PUBLIC_STATIC_FINAL,
			                         "D",
			                         "DCONST",
			                         Collections.singletonList(new AttributeInfo(constantPool.utf8("ConstantValue"),
			                                                                     new ConstantValue(constantPool.add(new DoubleInfo(99.9))))));

			// unless otherwise defined, all classes need an empty public constructor by default. apparently we have to add this
			methodsGenerator.addPublicEmptyConstructor("java/lang/Object", ClassProxy.forClass(classFileGenerator));
			methodsGenerator.addMethod(PUBLIC, "(III)D", "loop").addAttribute(loopCode(constantPool, classFileGenerator));

			ClassFile classFile = ClassFile.builder()
			                               .accessFlags(PUBLIC_FINAL)
			                               .version(ClassFile.Version.JAVA_8)
			                               .methods(methodsGenerator.generate())
			                               .fields(fieldsGenerator.generate())
			                               .attributes(Collections.emptyList())
			                               .constantPool(constantPool.generate())
			                               .interfaces(Collections.emptyList())
			                               .thisClass(thisClassClassInfoConstantPoolIndex)
			                               .superClass(superClassClassInfoConstantPoolIndex)
			                               .build();
			ClassFileWriter.createFor(classFile).write(out);
		}

		URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new File("target/generated-classes/direct").toURI().toURL()});
		Class<?> clazz = urlClassLoader.loadClass("net.jevring.javac.Looper");
		Constructor<?> constructor = clazz.getConstructor();
		Object instance = constructor.newInstance();

		Method loop = clazz.getMethod("loop", int.class, int.class, int.class);
		Object result = loop.invoke(instance, 1, 2, 3);
		assertEquals(Double.class, result.getClass());
		assertEquals(260.4, (double) result, 0);
	}

	@Test
	public void testLooper() throws Exception {
		LooperSample looper = new LooperSample();
		double loop = looper.loop(1, 2, 3);
		assertEquals(260.4, loop, 0);
	}

	private AttributeInfo loopCode(ConstantPool constantPool, ClassFileGenerator classFileGenerator) {
		CodeGenerator codeGenerator = new CodeGenerator(constantPool);

		StackMapTableGenerator stackMapTable = new StackMapTableGenerator(constantPool);

		LocalVariableTableGenerator localVariables = codeGenerator.variables();
		localVariables.addMethodVariable(ClassProxy.forClass(classFileGenerator), "this");
		localVariables.addMethodVariable(ClassProxy.INT, "from");
		localVariables.addMethodVariable(ClassProxy.INT, "to");
		localVariables.addMethodVariable(ClassProxy.INT, "step");

		//double v1 = CONST + to;
		// todo: why does the normal javac use bipush here? likely because it's one fewer levels of indirection
		codeGenerator.add(ByteCode.LDC, (byte) constantPool.add(new IntegerInfo(99)));
		codeGenerator.add(ByteCode.ILOAD_2);
		codeGenerator.add(ByteCode.IADD);
		codeGenerator.add(ByteCode.I2D); // convert int to double, as we've been doing int math, but are now storing in a double

		// todo change one of the locals to a long


		LocalVariable v1 = localVariables.addLocalVariable(ClassProxy.DOUBLE, "v1");
		codeGenerator.add(ByteCode.DSTORE, v1.getSlot());
		v1.startScope(codeGenerator.getNextByteCodeArrayIndex());

		//double v2 = v1 / 2;
		codeGenerator.add(ByteCode.DLOAD, v1.getSlot());
		codeGenerator.add(ByteCode.LDC2_W, constantPool.add(new DoubleInfo(2)));
		codeGenerator.add(ByteCode.DDIV);
		LocalVariable v2 = localVariables.addLocalVariable(ClassProxy.DOUBLE, "v2");
		codeGenerator.add(ByteCode.DSTORE, v2.getSlot());
		v2.startScope(codeGenerator.getNextByteCodeArrayIndex());

		//double v3 = 3;
		codeGenerator.add(ByteCode.LDC2_W, constantPool.add(new DoubleInfo(3)));
		LocalVariable v3 = localVariables.addLocalVariable(ClassProxy.DOUBLE, "v3");
		codeGenerator.add(ByteCode.DSTORE, v3.getSlot());
		v3.startScope(codeGenerator.getNextByteCodeArrayIndex());

		//int v4 = from + to + step;
		codeGenerator.add(ByteCode.ILOAD_1);
		codeGenerator.add(ByteCode.ILOAD_2);
		codeGenerator.add(ByteCode.IADD);
		codeGenerator.add(ByteCode.ILOAD_3);
		codeGenerator.add(ByteCode.IADD);
		LocalVariable v4 = localVariables.addLocalVariable(ClassProxy.INT, "v4");
		codeGenerator.add(ByteCode.ISTORE, v4.getSlot());
		v4.startScope(codeGenerator.getNextByteCodeArrayIndex());

		//double sum = 0;
		codeGenerator.add(ByteCode.DCONST_0);
		LocalVariable sum = localVariables.addLocalVariable(ClassProxy.DOUBLE, "sum");
		codeGenerator.add(ByteCode.DSTORE, sum.getSlot());
		sum.startScope(codeGenerator.getNextByteCodeArrayIndex());

		//for (int i = from; i <= to; i += step) {
		// -- int i = from;
		codeGenerator.add(ByteCode.ILOAD_1);
		LocalVariable i = localVariables.addLocalVariable(ClassProxy.INT, "i");
		codeGenerator.add(ByteCode.ISTORE, i.getSlot());
		i.startScope(codeGenerator.getNextByteCodeArrayIndex());
		// -- i <= to;
		short loopStartIndex = codeGenerator.add(ByteCode.ILOAD, i.getSlot());

		// todo: understand what the StackMapTable does!
		// it's apparently needed for verification, but I'm not quite sure how that even works
		List<VerificationTypeInfo> loopLocals = new ArrayList<>();
		// todo: we should be able to build this first frame from the LocalVariableTableGenerator, but what about the subsequent frames?
		loopLocals.add(new ObjectVariableInfo(constantPool.add(new ClassInfo(constantPool.utf8("net/jevring/javac/Looper")))));
		loopLocals.add(new IntegerVariableInfo());
		loopLocals.add(new IntegerVariableInfo());
		loopLocals.add(new IntegerVariableInfo());
		loopLocals.add(new DoubleVariableInfo());
		loopLocals.add(new DoubleVariableInfo());
		loopLocals.add(new DoubleVariableInfo());
		loopLocals.add(new IntegerVariableInfo());
		loopLocals.add(new DoubleVariableInfo());
		loopLocals.add(new IntegerVariableInfo());
		List<VerificationTypeInfo> loopStack = Collections.emptyList();
		stackMapTable.add(new FullFrame(loopStartIndex, loopLocals, loopStack));

		codeGenerator.add(ByteCode.ILOAD_2);
		PendingShortJumpOffset endOfLoopReference = new PendingShortJumpOffset();
		short branchIndex = codeGenerator.add(ByteCode.IF_ICMPGT, endOfLoopReference); // this is an inverse check. It says "jump to end of loop if i > from"

		//	sum = sum + v1 + v2 + v3 + v4;
		codeGenerator.add(ByteCode.DLOAD, sum.getSlot());
		codeGenerator.add(ByteCode.DLOAD, v1.getSlot());
		codeGenerator.add(ByteCode.DADD);
		codeGenerator.add(ByteCode.DLOAD, v2.getSlot());
		codeGenerator.add(ByteCode.DADD);
		codeGenerator.add(ByteCode.DLOAD, v3.getSlot());
		codeGenerator.add(ByteCode.DADD);
		codeGenerator.add(ByteCode.ILOAD, v4.getSlot());
		codeGenerator.add(ByteCode.I2D);
		codeGenerator.add(ByteCode.DADD);
		codeGenerator.add(ByteCode.DSTORE, sum.getSlot());

		// -- i += step
		codeGenerator.add(ByteCode.ILOAD, i.getSlot());
		codeGenerator.add(ByteCode.ILOAD_3);
		codeGenerator.add(ByteCode.IADD);
		codeGenerator.add(ByteCode.ISTORE, i.getSlot());

		//}
		// -- loop back to start of loop
		// We need to jump BACK to where the loop starts. 
		// Thus, we take where we want to jump To (loopStartIndex), minus where currently ARE (gotoIndex)
		PendingShortJumpOffset startOfLoopReverseOffsetReference = new PendingShortJumpOffset();
		short gotoIndex = codeGenerator.add(ByteCode.GOTO, startOfLoopReverseOffsetReference); // NOTE: This is an *offset*, not an index!
		startOfLoopReverseOffsetReference.setValue((short) (loopStartIndex - gotoIndex));

		//return sum + DCONST;
		short firstOperationAfterLoopIndex = codeGenerator.add(ByteCode.DLOAD, sum.getSlot());
		// todo: what are the semantics of the inputs to the chop frame?
		// it's supposed to reference the previous frame, but with something missing. Likely the loop variable missing
		byte frameType = (byte) (251 - 1); // todo: where does this 1 come from?
		// todo: this math is supposed to be off-by-one, why?
		stackMapTable.add(new ChopFrame(frameType, (short) (firstOperationAfterLoopIndex - loopStartIndex - 1)));

		endOfLoopReference.setValue((short) (firstOperationAfterLoopIndex - branchIndex)); // NOTE: This is an *offset*, not an index!
		localVariables.exitBlock(firstOperationAfterLoopIndex);
		codeGenerator.add(ByteCode.LDC2_W, constantPool.add(new DoubleInfo(99.9)));
		codeGenerator.add(ByteCode.DADD);
		codeGenerator.add(ByteCode.DRETURN);

		localVariables.endScopeForRemainingVariables(codeGenerator.getNextByteCodeArrayIndex());

		// todo: calculate max stack on the fly!
		codeGenerator.add(stackMapTable.generate());

		return codeGenerator.generate();
	}


}

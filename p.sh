#!/bin/bash
class=ThrowsException
echo "Running for $class"
javac -source 8 -target 8 -cp target/classes:target/test-classes -g -d . src/test/resources/net/jevring/javac/examples/$class.java
javap -l -v -c -cp . net.jevring.javac.examples.$class > src/test/resources/net/jevring/javac/examples/javap-$class.txt
rm -rf net
javap -l -v -c -cp target/generated-classes net.jevring.javac.examples.$class > src/test/resources/net/jevring/javac/examples/our-$class.txt
